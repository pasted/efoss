class GenericRecord

	#reporter model
	attr_accessor :reporter_fname, :reporter_lname, :reporter_email
	
	#outbreak model
	attr_accessor :outbreak_id, :outbreak_current_state, :outbreak_onset_first, :outbreak_onset_last, :outbreak_user_reference, :outbreak_gezi_reference, :outbreak_transmission_mode,
			:outbreak_total_affected, :outbreak_at_risk, :outbreak_lab_confirmed, :outbreak_admitted, :outbreak_died, :outbreak_aetiology, :outbreak_outbreak_type, :outbreak_comment,
			:outbreak_point_source, :outbreak_point_source_date, :outbreak_point_source_detail
	
	#incident model		
	attr_accessor :incident_category, :incident_subcategory, :incident_subtype, :incident_cuisine, :incident_detail, :incident_evidence_category ,:incident_evidence_name
	
	#location model
	attr_accessor :location_placename, :location_address_1, :location_address_2, :location_address_3, :location_town,
			:location_region, :location_hpu, :location_local_authority, :location_postcode, :location_lon, :location_lat
	
	
	#agent_1 model
	attr_accessor :agent_1_name, :agent_1_serotype, :agent_1_phagetype, :agent_1_other_typing, :agent_1_concentration, :agent_1_confirmed, :agent_1_comment
	
	#agent_2 model
	attr_accessor :agent_2_name, :agent_2_serotype, :agent_2_phagetype, :agent_2_other_typing, :agent_2_concentration, :agent_2_confirmed, :agent_2_comment
	
	#agent_3 model
	attr_accessor :agent_3_name, :agent_3_serotype, :agent_3_phagetype, :agent_3_other_typing, :agent_3_concentration, :agent_3_confirmed, :agent_3_comment
	
	#risk_1 model
	attr_accessor :risk_1_factor_name, :risk_1_detail
	
	#risk_2 model
	attr_accessor :risk_2_factor_name, :risk_2_detail
	
	#risk_3 model
	attr_accessor :risk_3_factor_name, :risk_3_detail
	
	#food_1 model
	attr_accessor :food_1_category, :food_1_subcategory, :food_1_description, :food_1_contains_rse, :food_1_country, :food_1_evidence_category, :food_1_evidence_name
	
	#food_2 model
	attr_accessor :food_2_category, :food_2_subcategory, :food_2_description, :food_2_contains_rse, :food_2_country, :food_2_evidence_category, :food_2_evidence_name
	
	#food_3 model
	attr_accessor :food_3_category, :food_3_subcategory, :food_3_description, :food_3_contains_rse, :food_3_country, :food_3_evidence_category, :food_3_evidence_name
	
	#response model
	attr_accessor :response_premise_status, :response_prosecution_status, :response_notice_served, :response_inspection_rating, :response_comment
	
	#measure_1 model
	attr_accessor :measure_1_description
	
	#measure_2 model
	attr_accessor :measure_2_description
	
	#measure_3 model
	attr_accessor :measure_3_description
	
end
