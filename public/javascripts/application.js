// Place your application-specific JavaScript functions and classes here
// This file is automatically included by javascript_include_tag :defaults


//https://github.com/enterprise-rails/invoice_nested_attributes_ajax/blob/master/public/javascripts/invoice.js
function max_html_seq_id(object_name, collection_name, attribute_name, field_to_count) {
    if ( field_to_count === undefined ) {
        field_to_count = 'input[type=text]';       
    }
    
    search_string = field_to_count + '[id^=' + object_name + '_' + collection_name + '_attributes]';
    
    bp_fields = $$(search_string);
    
    max_seq_id = -1;

    
    bp_fields.each(function(bp_field) {
       
        html_id = bp_field.id;       
        reg_exp_str = '^' + object_name + '_' + collection_name + '_attributes_(\\d+)_' + attribute_name;  
        
        reg_exp = new RegExp(reg_exp_str); 
        try{
         match_ret = reg_exp.exec(html_id);
       	 seq_id_str = match_ret[1];
       	 seq_id = parseInt(seq_id_str);
       	 
        }catch(err){
         seq_id = 0;	
        }
        if (seq_id > max_seq_id) {
            max_seq_id = seq_id;
        }
    });

    return max_seq_id;
}

function this_html_seq_id(html_id, object_name, collection_name, attribute_name){
	reg_exp_str = '^' + object_name + '_' + collection_name + '_attributes_(\\d+)_' + attribute_name;       
        reg_exp = new RegExp(reg_exp_str); 
        try{
         match_ret = reg_exp.exec(html_id);
         seq_id_str = match_ret[1];
       	 seq_id = parseInt(seq_id_str);
        }catch(err){
         seq_id = 0; 
        }
        
        return seq_id;
}

function next_html_seq_id(object_name, collection_name, attribute_name, field_to_count) {
	return (max_html_seq_id(object_name, collection_name, attribute_name, field_to_count) + 1);
}


function params_to_new_risk() {
	return 'next_child_index=' + next_html_seq_id('outbreak', 'risks', 'detail', 'textarea');    
}

function params_to_new_agent() {
	return 'next_ba_index=' + next_html_seq_id('outbreak', 'bacterial_agents', 'confirmed', 'select') + '&next_va_index=' + next_html_seq_id('outbreak', 'viral_agents', 'confirmed', 'select') + '&next_pa_index=' + next_html_seq_id('outbreak', 'protozoal_agents', 'confirmed', 'select') + '&next_ta_index=' + next_html_seq_id('outbreak', 'toxic_agents', 'confirmed', 'select');	
}

function params_to_new_food() {
	return 'next_food_index=' + next_html_seq_id('outbreak', 'foods', 'description', 'textarea');	
}

function params_to_new_control_measure() {
	return 'next_child_index=' + next_html_seq_id('outbreak_response_attributes', 'measures', 'description', 'textarea');
}

function params_to_update_food_select(this_id) {
	return 'this_child_index=' + this_html_seq_id(this_id, 'outbreak', 'foods', 'description');	
}

function params_to_new_investigation(agent){
	if(agent == "bacterium"){
		return 'this_index=' + max_html_seq_id('outbreak', 'bacterial_agents', 'confirmed', 'select');
	}else if(agent == "virus"){
		return 'this_index=' + max_html_seq_id('outbreak', 'viral_agents', 'confirmed', 'select');
	}else if(agent == "protozoan"){
		return 'this_index=' + max_html_seq_id('outbreak', 'protozoal_agents', 'confirmed', 'select');
	}else if(agent == "toxin"){
		return 'this_index=' + max_html_seq_id('outbreak', 'toxic_agents', 'confirmed', 'select');
	}
}
