require 'test_helper'

class OutbreakTest < ActiveSupport::TestCase
  test "should not save without validations" do
      outbreak = Outbreak.new
      assert !outbreak.save, "Saved outbreak without validations passing"
  end
end
