require 'test_helper'

class LocalAuthoritiesControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:local_authorities)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create local_authority" do
    assert_difference('LocalAuthority.count') do
      post :create, :local_authority => { }
    end

    assert_redirected_to local_authority_path(assigns(:local_authority))
  end

  test "should show local_authority" do
    get :show, :id => local_authorities(:one).to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => local_authorities(:one).to_param
    assert_response :success
  end

  test "should update local_authority" do
    put :update, :id => local_authorities(:one).to_param, :local_authority => { }
    assert_redirected_to local_authority_path(assigns(:local_authority))
  end

  test "should destroy local_authority" do
    assert_difference('LocalAuthority.count', -1) do
      delete :destroy, :id => local_authorities(:one).to_param
    end

    assert_redirected_to local_authorities_path
  end
end
