require 'test_helper'

class VirusesControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:viruses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create virus" do
    assert_difference('Virus.count') do
      post :create, :virus => { }
    end

    assert_redirected_to virus_path(assigns(:virus))
  end

  test "should show virus" do
    get :show, :id => viruses(:one).to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => viruses(:one).to_param
    assert_response :success
  end

  test "should update virus" do
    put :update, :id => viruses(:one).to_param, :virus => { }
    assert_redirected_to virus_path(assigns(:virus))
  end

  test "should destroy virus" do
    assert_difference('Virus.count', -1) do
      delete :destroy, :id => viruses(:one).to_param
    end

    assert_redirected_to viruses_path
  end
end
