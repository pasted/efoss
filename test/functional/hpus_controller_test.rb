require 'test_helper'

class HpusControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:hpus)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create hpu" do
    assert_difference('Hpu.count') do
      post :create, :hpu => { }
    end

    assert_redirected_to hpu_path(assigns(:hpu))
  end

  test "should show hpu" do
    get :show, :id => hpus(:one).to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => hpus(:one).to_param
    assert_response :success
  end

  test "should update hpu" do
    put :update, :id => hpus(:one).to_param, :hpu => { }
    assert_redirected_to hpu_path(assigns(:hpu))
  end

  test "should destroy hpu" do
    assert_difference('Hpu.count', -1) do
      delete :destroy, :id => hpus(:one).to_param
    end

    assert_redirected_to hpus_path
  end
end
