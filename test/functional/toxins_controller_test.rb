require 'test_helper'

class ToxinsControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:toxins)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create toxin" do
    assert_difference('Toxin.count') do
      post :create, :toxin => { }
    end

    assert_redirected_to toxin_path(assigns(:toxin))
  end

  test "should show toxin" do
    get :show, :id => toxins(:one).to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => toxins(:one).to_param
    assert_response :success
  end

  test "should update toxin" do
    put :update, :id => toxins(:one).to_param, :toxin => { }
    assert_redirected_to toxin_path(assigns(:toxin))
  end

  test "should destroy toxin" do
    assert_difference('Toxin.count', -1) do
      delete :destroy, :id => toxins(:one).to_param
    end

    assert_redirected_to toxins_path
  end
end
