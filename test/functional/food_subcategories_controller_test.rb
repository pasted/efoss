require 'test_helper'

class FoodSubcategoriesControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:food_subcategories)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create food_subcategory" do
    assert_difference('FoodSubcategory.count') do
      post :create, :food_subcategory => { }
    end

    assert_redirected_to food_subcategory_path(assigns(:food_subcategory))
  end

  test "should show food_subcategory" do
    get :show, :id => food_subcategories(:one).to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => food_subcategories(:one).to_param
    assert_response :success
  end

  test "should update food_subcategory" do
    put :update, :id => food_subcategories(:one).to_param, :food_subcategory => { }
    assert_redirected_to food_subcategory_path(assigns(:food_subcategory))
  end

  test "should destroy food_subcategory" do
    assert_difference('FoodSubcategory.count', -1) do
      delete :destroy, :id => food_subcategories(:one).to_param
    end

    assert_redirected_to food_subcategories_path
  end
end
