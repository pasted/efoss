require 'test_helper'

class EvidencesControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:evidences)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create evidence" do
    assert_difference('Evidence.count') do
      post :create, :evidence => { }
    end

    assert_redirected_to evidence_path(assigns(:evidence))
  end

  test "should show evidence" do
    get :show, :id => evidences(:one).to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => evidences(:one).to_param
    assert_response :success
  end

  test "should update evidence" do
    put :update, :id => evidences(:one).to_param, :evidence => { }
    assert_redirected_to evidence_path(assigns(:evidence))
  end

  test "should destroy evidence" do
    assert_difference('Evidence.count', -1) do
      delete :destroy, :id => evidences(:one).to_param
    end

    assert_redirected_to evidences_path
  end
end
