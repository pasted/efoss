require 'test_helper'

class ProtozoasControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:protozoas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create protozoa" do
    assert_difference('Protozoa.count') do
      post :create, :protozoa => { }
    end

    assert_redirected_to protozoa_path(assigns(:protozoa))
  end

  test "should show protozoa" do
    get :show, :id => protozoas(:one).to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => protozoas(:one).to_param
    assert_response :success
  end

  test "should update protozoa" do
    put :update, :id => protozoas(:one).to_param, :protozoa => { }
    assert_redirected_to protozoa_path(assigns(:protozoa))
  end

  test "should destroy protozoa" do
    assert_difference('Protozoa.count', -1) do
      delete :destroy, :id => protozoas(:one).to_param
    end

    assert_redirected_to protozoas_path
  end
end
