require 'test_helper'

class EvidenceCategoriesControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:evidence_categories)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create evidence_category" do
    assert_difference('EvidenceCategory.count') do
      post :create, :evidence_category => { }
    end

    assert_redirected_to evidence_category_path(assigns(:evidence_category))
  end

  test "should show evidence_category" do
    get :show, :id => evidence_categories(:one).to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => evidence_categories(:one).to_param
    assert_response :success
  end

  test "should update evidence_category" do
    put :update, :id => evidence_categories(:one).to_param, :evidence_category => { }
    assert_redirected_to evidence_category_path(assigns(:evidence_category))
  end

  test "should destroy evidence_category" do
    assert_difference('EvidenceCategory.count', -1) do
      delete :destroy, :id => evidence_categories(:one).to_param
    end

    assert_redirected_to evidence_categories_path
  end
end
