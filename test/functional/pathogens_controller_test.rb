require 'test_helper'

class PathogensControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:pathogens)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create pathogen" do
    assert_difference('Pathogen.count') do
      post :create, :pathogen => { }
    end

    assert_redirected_to pathogen_path(assigns(:pathogen))
  end

  test "should show pathogen" do
    get :show, :id => pathogens(:one).to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => pathogens(:one).to_param
    assert_response :success
  end

  test "should update pathogen" do
    put :update, :id => pathogens(:one).to_param, :pathogen => { }
    assert_redirected_to pathogen_path(assigns(:pathogen))
  end

  test "should destroy pathogen" do
    assert_difference('Pathogen.count', -1) do
      delete :destroy, :id => pathogens(:one).to_param
    end

    assert_redirected_to pathogens_path
  end
end
