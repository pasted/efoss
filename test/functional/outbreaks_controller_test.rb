require 'test_helper'

class OutbreaksControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:outbreaks)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create outbreak" do
    assert_difference('Outbreak.count') do
      post :create, :outbreak => { }
    end

    assert_redirected_to outbreak_path(assigns(:outbreak))
  end

  test "should show outbreak" do
    get :show, :id => outbreaks(:one).to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => outbreaks(:one).to_param
    assert_response :success
  end

  test "should update outbreak" do
    put :update, :id => outbreaks(:one).to_param, :outbreak => { }
    assert_redirected_to outbreak_path(assigns(:outbreak))
  end

  test "should destroy outbreak" do
    assert_difference('Outbreak.count', -1) do
      delete :destroy, :id => outbreaks(:one).to_param
    end

    assert_redirected_to outbreaks_path
  end
end
