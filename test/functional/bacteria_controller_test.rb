require 'test_helper'

class BacteriaControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:bacteria)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create bacterium" do
    assert_difference('Bacterium.count') do
      post :create, :bacterium => { }
    end

    assert_redirected_to bacterium_path(assigns(:bacterium))
  end

  test "should show bacterium" do
    get :show, :id => bacteria(:one).to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => bacteria(:one).to_param
    assert_response :success
  end

  test "should update bacterium" do
    put :update, :id => bacteria(:one).to_param, :bacterium => { }
    assert_redirected_to bacterium_path(assigns(:bacterium))
  end

  test "should destroy bacterium" do
    assert_difference('Bacterium.count', -1) do
      delete :destroy, :id => bacteria(:one).to_param
    end

    assert_redirected_to bacteria_path
  end
end
