# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_efoss_session',
  :secret      => '2a2f1d44c0cd5becdc53ef90800ea5ae6220e72989802b5377776637ebafdf08a7aedc08c3109ba54c0129ac445af8f9bef678e70dc09ef4ff2edc6a2deeb1ec'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
