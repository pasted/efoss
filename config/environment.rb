# Be sure to restart your server when you modify this file

# Specifies gem version of Rails to use when vendor/rails is not present
RAILS_GEM_VERSION = '2.3.9' unless defined? RAILS_GEM_VERSION
START_DATE = '01/01/1992'
ENV['RAILS_ENV'] = 'development'
#ENV['RAILS_ENV'] ||= 'production'
# Bootstrap the Rails environment, frameworks, and default configuration
require File.join(File.dirname(__FILE__), 'boot')

Rails::Initializer.run do |config|
  # Settings in config/environments/* take precedence over those specified here.
  # Application configuration should go into files in config/initializers
  # -- all .rb files in that directory are automatically loaded.

  # Add additional load paths for your own custom dirs
  # config.load_paths += %W( #{RAILS_ROOT}/extras )

  # Specify gems that this application depends on and have them installed with rake gems:install
  # config.gem "bj"
  # config.gem "hpricot", :version => '0.6', :source => "http://code.whytheluckystiff.net"
  # config.gem "sqlite3-ruby", :lib => "sqlite3"
  # config.gem "aws-s3", :lib => "aws/s3"

  # Only load the plugins named here, in the order given (default is alphabetical).
  # :all can be used as a placeholder for all plugins not explicitly named
  # config.plugins = [ :exception_notification, :ssl_requirement, :all ]

  # Skip frameworks you're not going to use. To use Rails without a database,
  # you must remove the Active Record framework.
  # config.frameworks -= [ :active_record, :active_resource, :action_mailer ]

  # Activate observers that should always be running
  # config.active_record.observers = :cacher, :garbage_collector, :forum_observer

  # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
  # Run "rake -D time" for a list of tasks for finding time zone names.
  config.time_zone = 'UTC'
  
  #Devise config.gem -> warden is a dependency
  config.gem 'warden'
  config.gem 'devise', :version => '1.0.10'
  #CanCan handles the User permissions
  config.gem 'cancan'
  
  #will_paginate
  config.gem 'will_paginate'
  
  #config.gem 'mislav-will_paginate', 
  #  :lib => 'will_paginate', 
  #  :source => 'http://gems.github.com'
  
  #acts_as_state_machine
  config.gem 'aasm'  
  
  #searchlogic - adds scopes based on method missing
  config.gem "searchlogic"
  
  #paperclip - file upload and storage handler
  config.gem 'paperclip', :source => 'http://rubygems.org', :version => '2.3.1.1'
  
  #encryption - defaults to AES256::OpenSSL
  config.gem 'attr_encrypted'
  
  #required to use HAML for the views
  config.gem "haml"
  
  
  
  # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
  # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}')]
  # config.i18n.default_locale = :de
end
  
