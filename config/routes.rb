ActionController::Routing::Routes.draw do |map|
  map.devise_for :users


  map.resources :roles

  map.resources :evidence_categories

  map.resources :evidences

  map.resources :foods
  map.update_food_subcategory 'foods/update_food_subcategory', :controller => 'foods', :action => 'update_food_subcategory'
  
  map.resources :investigations
  map.new_investigation_agent 'investigations/add_investigation_agent', :controller => 'investigations', :action => 'add_investigation_agent'
 
  map.resources :reporters

  map.resources :origins

  map.resources :food_subcategories

  map.resources :food_categories

  map.add_investigation 'outbreaks/add_investigation', :controller => 'outbreaks', :action => 'add_investigation'

  map.update_select_menus 'outbreaks/update_select_menus', :controller => 'outbreaks', :action => 'update_select_menus'
  
  map.new_control_measure 'outbreaks/new_control_measure', :controller => 'outbreaks', :action => 'new_control_measure'
  map.update_reporter 'outbreaks/update_reporter', :controller => 'outbreaks', :action => 'update_reporter'
  map.new_food 'outbreaks/new_food', :controller => 'outbreaks', :action => 'new_food'

  map.update_subcategory 'outbreaks/update_subcategory', :controller => 'outbreaks', :action => 'update_subcategory'
  map.update_subtype 'outbreaks/update_subtype', :controller => 'outbreaks', :action => 'update_subtype'
  map.add_risk 'outbreaks/add_risk', :controller => 'outbreaks', :action => 'add_risk'
  map.get_pathogen 'outbreaks/get_pathogen', :controller => 'outbreaks', :action => 'get_pathogen'
  map.find_lonlat 'locations/find_lonlat', :controller => 'locations', :action => 'find_lonlat'
  map.activate_state 'outbreaks/activate_state', :controller => 'outbreaks', :action => 'activate_state'
  map.review 'outbreaks/review', :controller => 'outbreaks', :action => 'review'
  map.inactive_list 'outbreaks/inactive_list', :controller => 'outbreaks', :action => 'inactive_list'
  map.download_report 'outbreaks/download_report', :controller => 'outbreaks', :action => 'download_report'
  
  map.full_search 'outbreaks/search', :controller  => 'outbreaks', :action => 'search'
  map.add_associated_agent 'outbreaks/add_associated_agent', :controller => 'outbreaks', :action => 'add_associated_agent'
  
  map.chart 'outbreaks/chart', :controller => 'outbreaks', :action => 'chart'
  map.total_outbreaks_by_month_year 'charts/total_outbreaks_by_month_year', :controller => 'charts', :action => 'total_outbreaks_by_month_year'
  map.foodborne_non_foodborne_by_year 'charts/foodborne_non_foodborne_by_year', :controller => 'charts', :action => 'foodborne_non_foodborne_by_year'
  map.pathogens_by_year 'charts/pathogens_by_year', :controller => 'charts', :action => 'pathogens_by_year'
  map.foodtypes_by_year 'charts/foodtypes_by_year', :controller => 'charts', :action => 'foodtypes_by_year'
  map.causative_pathogen_by_transmission 'charts/causative_pathogen_by_transmission', :controller => 'charts', :action => 'causative_pathogen_by_transmission'
  map.non_foodborne_transmission_by_year 'charts/non_foodborne_transmission_by_year', :controller => 'charts', :action => 'non_foodborne_transmission_by_year'
  map.major_foodborne_pathogen_by_year 'charts/major_foodborne_pathogen_by_year', :controller => 'charts', :action => 'major_foodborne_pathogen_by_year'
  map.foodborne_pathogens_total 'charts/foodborne_pathogens_total', :controller => 'charts', :action => 'foodborne_pathogens_total'
  
  map.listing 'exports/listing', :controller => 'exports', :action => 'listing'
  map.export_checker 'exports/export_checker', :controller => 'exports', :action => 'export_checker'
  map.export_ready 'exports/export_ready', :controller => 'exports', :action => 'export_ready'
  map.send_export 'exports/send_export', :controller => 'exports', :action => 'send_export'
  
  
  map.resources :factors

  map.resources :toxins

  map.resources :protozoas

  map.resources :bacteria

  map.resources :viruses
  
  map.resources :bacterial_agents, :has_one => :investigation
  
  map.resources :viral_agents, :has_one => :investigation
  
  map.resources :protozoal_agents, :has_one => :investigation
  
  map.resources :toxic_agents, :has_one => :investigation

  map.resources :pathogens

  map.resources :cuisines

  map.resources :subtypes

  map.resources :subcategories

  map.resources :categories

  map.resources :locations
  
  map.resources :incidents

  map.resources :properties

  map.resources :local_authorities

  map.resources :hpus

  map.resources :regions
  
  map.resources :outbreaks

  map.devise_for :users
  map.new_user_registration '/users/registrations/new', :controller => 'users/registrations', :action => 'new', :conditions => {:method => :get}
  map.create_user_registration '/users/registrations/create', :controller => 'users/registrations', :action => 'create', :conditions => {:method => :post}
  map.user_account 'account', :controller => 'users', :action => 'account'
  
  map.check_email 'users/check_email', :controller => 'users', :action => 'check_email'

  map.resources :users

  
  map.resources :exposures

  map.resources :details
  
  map.resources :risks
  
  map.national_map 'maps/national_map', :controller => 'maps', :action => 'national_map'
 
  map.admin 'pages/admin', :controller => 'pages', :action => 'admin_section'
  
  map.namespace :admin do |admin|
  	  admin.root :action => 'panel'
  end

  map.root :controller => 'pages', :action => 'show', :name => 'welcome'
  
  # The priority is based upon order of creation: first created -> highest priority.

  # Sample of regular route:
  #   map.connect 'products/:id', :controller => 'catalog', :action => 'view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   map.purchase 'products/:id/purchase', :controller => 'catalog', :action => 'purchase'
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   map.resources :products

  # Sample resource route with options:
  #   map.resources :products, :member => { :short => :get, :toggle => :post }, :collection => { :sold => :get }

  # Sample resource route with sub-resources:
  #   map.resources :products, :has_many => [ :comments, :sales ], :has_one => :seller
  
  # Sample resource route with more complex sub-resources
  #   map.resources :products do |products|
  #     products.resources :comments
  #     products.resources :sales, :collection => { :recent => :get }
  #   end

  # Sample resource route within a namespace:
  #   map.namespace :admin do |admin|
  #     # Directs /admin/products/* to Admin::ProductsController (app/controllers/admin/products_controller.rb)
  #     admin.resources :products
  #   end

  # You can have the root of your site routed with map.root -- just remember to delete public/index.html.
  # map.root :controller => "welcome"

  

  # See how all your routes lay out with "rake routes"

  # Install the default routes as the lowest priority.
  # Note: These default routes make all actions in every controller accessible via GET requests. You should
  # consider removing or commenting them out if you're using named routes and resources.
  #map.connect ':controller/:action/:id'
  #map.connect ':controller/:action/:id.:format'
  
end
