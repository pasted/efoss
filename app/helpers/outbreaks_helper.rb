module OutbreaksHelper
	
	def add_risk_link(name)
		link_to_function name do |page|
			page.insert_html :bottom, :risk_list, :partial => 'risk', :object => Risk.new	
		end	
	end
end
