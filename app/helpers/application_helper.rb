# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper
	
	#FormBuilder's indexing of nested fields uses private methods
	#use form_tag_id to return the dynamically generated field index
	def sanitized_object_name(object_name)
	  object_name.gsub(/\]\[|[^-a-zA-Z0-9:.]/,"_").sub(/_$/,"")
	end
	 
	def sanitized_method_name(method_name)
	  method_name.sub(/\?$/, "")
	end
	 
	def form_tag_id(object_name, method_name)
	  "#{sanitized_object_name(object_name.to_s)}_#{sanitized_method_name(method_name.to_s)}"
	end
	
	def top_nav_formatter
		page = request.path_parameters['controller'] + ":" + request.path_parameters['action']
		if page == "pages:show"
			@home_klass = "class='active'"
			@admin_klass = ""
			@account_klass = "" 
			@signout_klass = "class='last'"
		elsif page == "pages:admin_section"
			@home_klass = "class='preactive'"
			@admin_klass = "class='active'"
			@account_klass = ""
			@signout_klass = "class='last'"
		elsif page == "users:account"
			if current_user.role?("admin")
			 	@home_klass = ""
				@admin_klass = "class='preactive'"
			else 
				@home_klass = "class='preactive'" 
				@admin_klass = "" 
			end 
			@account_klass = "class='active'"
			@signout_klass = "class='last'"
	 	else
			@home_klass = "class='active'" 
			@admin_klass = ""
		        @account_klass = "" 
		 	@signout_klass = "class='last'"
		end
			
	end
	
	def left_nav_formatter
		page = request.path_parameters['controller'] + ":" + request.path_parameters['action']
		if page == "outbreaks:new"
			@new_outbreak_klass = "class='active'"	
			@list_outbreak_klass = ""
			@map_klass = ""
			@chart_general_monthly_klass = ""
			@chart_general_type_klass = ""
			@chart_general_pathogens_klass = ""
			@chart_foodborne_pathogens_klass = ""
			@chart_foodborne_transmission_klass = ""
			@chart_nonfoodborne_foodtype_klass = ""
			@chart_nonfoodborne_pathogens_year_klass = ""
			@chart_nonfoodborne_pathogens_overall_klass = ""
		elsif page == "outbreaks:index"
			@new_outbreak_klass = ""	
			@list_outbreak_klass = "class='active'"
			@map_klass = ""
			@chart_general_monthly_klass = ""
			@chart_general_type_klass = ""
			@chart_general_pathogens_klass = ""
			@chart_foodborne_pathogens_klass = ""
			@chart_foodborne_transmission_klass = ""
			@chart_nonfoodborne_foodtype_klass = ""
			@chart_nonfoodborne_pathogens_year_klass = ""
			@chart_nonfoodborne_pathogens_overall_klass = ""
			
		elsif page == "maps:national_map"
			@new_outbreak_klass = ""	
			@list_outbreak_klass = ""
			@map_klass = "class='active'"
			@chart_general_monthly_klass = ""
			@chart_general_type_klass = ""
			@chart_general_pathogens_klass = ""
			@chart_foodborne_pathogens_klass = ""
			@chart_foodborne_transmission_klass = ""
			@chart_nonfoodborne_foodtype_klass = ""
			@chart_nonfoodborne_pathogens_year_klass = ""
			@chart_nonfoodborne_pathogens_overall_klass = ""
			
		elsif page == "charts:total_outbreaks_by_month_year"
			@new_outbreak_klass = ""	
			@list_outbreak_klass = ""
			@map_klass = ""
			@chart_general_monthly_klass = "class='active'"
			@chart_general_type_klass = ""
			@chart_general_pathogens_klass = ""
			@chart_foodborne_pathogens_klass = ""
			@chart_foodborne_transmission_klass = ""
			@chart_nonfoodborne_foodtype_klass = ""
			@chart_nonfoodborne_pathogens_year_klass = ""
			@chart_nonfoodborne_pathogens_overall_klass = ""
			
		elsif page == "charts:foodborne_non_foodborne_by_year"
			@new_outbreak_klass = ""	
			@list_outbreak_klass = ""
			@map_klass = ""
			@chart_general_monthly_klass = ""
			@chart_general_type_klass = "class='active'"
			@chart_general_pathogens_klass = ""
			@chart_foodborne_pathogens_klass = ""
			@chart_foodborne_transmission_klass = ""
			@chart_nonfoodborne_foodtype_klass = ""
			@chart_nonfoodborne_pathogens_year_klass = ""
			@chart_nonfoodborne_pathogens_overall_klass = ""
			
		elsif page == "charts:pathogens_by_year"
			@new_outbreak_klass = ""	
			@list_outbreak_klass = ""
			@map_klass = ""
			@chart_general_monthly_klass = ""
			@chart_general_type_klass = ""
			@chart_general_pathogens_klass = "class='active'"
			@chart_foodborne_pathogens_klass = ""
			@chart_foodborne_transmission_klass = ""
			@chart_nonfoodborne_foodtype_klass = ""
			@chart_nonfoodborne_pathogens_year_klass = ""
			@chart_nonfoodborne_pathogens_overall_klass = ""
			
		elsif page == "charts:causative_pathogen_by_transmission"
			@new_outbreak_klass = ""	
			@list_outbreak_klass = ""
			@map_klass = ""
			@chart_general_monthly_klass = ""
			@chart_general_type_klass = ""
			@chart_general_pathogens_klass = ""
			@chart_foodborne_pathogens_klass = "class='active'"
			@chart_foodborne_transmission_klass = ""
			@chart_nonfoodborne_foodtype_klass = ""
			@chart_nonfoodborne_pathogens_year_klass = ""
			@chart_nonfoodborne_pathogens_overall_klass = ""
			
		elsif page == "charts:non_foodborne_transmission_by_year"
			@new_outbreak_klass = ""	
			@list_outbreak_klass = ""
			@map_klass = ""
			@chart_general_monthly_klass = ""
			@chart_general_type_klass = ""
			@chart_general_pathogens_klass = ""
			@chart_foodborne_pathogens_klass = ""
			@chart_foodborne_transmission_klass = "class='active'"
			@chart_nonfoodborne_foodtype_klass = ""
			@chart_nonfoodborne_pathogens_year_klass = ""
			@chart_nonfoodborne_pathogens_overall_klass = ""
			
		elsif page == "charts:foodtypes_by_year"
			@new_outbreak_klass = ""	
			@list_outbreak_klass = ""
			@map_klass = ""
			@chart_general_monthly_klass = ""
			@chart_general_type_klass = ""
			@chart_general_pathogens_klass = ""
			@chart_foodborne_pathogens_klass = ""
			@chart_foodborne_transmission_klass = ""
			@chart_nonfoodborne_foodtype_klass = "class='active'"
			@chart_nonfoodborne_pathogens_year_klass = ""
			@chart_nonfoodborne_pathogens_overall_klass = ""
			
		elsif page == "charts:major_foodborne_pathogen_by_year"
			@new_outbreak_klass = ""	
			@list_outbreak_klass = ""
			@map_klass = ""
			@chart_general_monthly_klass = ""
			@chart_general_type_klass = ""
			@chart_general_pathogens_klass = ""
			@chart_foodborne_pathogens_klass = ""
			@chart_foodborne_transmission_klass = ""
			@chart_nonfoodborne_foodtype_klass = ""
			@chart_nonfoodborne_pathogens_year_klass = "class='active'"
			@chart_nonfoodborne_pathogens_overall_klass = ""
			
		elsif page == "charts:foodborne_pathogens_total"
			@new_outbreak_klass = ""	
			@list_outbreak_klass = ""
			@map_klass = ""
			@chart_general_monthly_klass = ""
			@chart_general_type_klass = ""
			@chart_general_pathogens_klass = ""
			@chart_foodborne_pathogens_klass = ""
			@chart_foodborne_transmission_klass = ""
			@chart_nonfoodborne_foodtype_klass = ""
			@chart_nonfoodborne_pathogens_year_klass = ""
			@chart_nonfoodborne_pathogens_overall_klass = "class='active'"
			
		end
			
	end
	
	
end
