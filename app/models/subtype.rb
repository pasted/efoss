class Subtype < ActiveRecord::Base
	has_many :incidents
	belongs_to :subcategory
end
