class Toxin < ActiveRecord::Base
	
	has_many :toxic_agents
	has_many :outbreaks, :through => :toxic_agents
end
