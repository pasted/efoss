class Outbreak < ActiveRecord::Base
	include AASM
	
	aasm_column :current_state
	aasm_initial_state :draft
	 
	aasm_state :draft
	aasm_state :active
	aasm_state :inactive
	 
	aasm_event :activate do
		transitions :to => :active, :from => [:draft]	
	end
	 
	aasm_event :inactivate do
		transitions :to => :inactive, :from => [:active, :draft]
	end
	 
	aasm_event :draft do
		transitions :to => :draft, :from => [:active, :inactive]
	end
	
	
	
	has_many :incidents, :dependent => :destroy
	has_many :locations, :through => :incidents	
	
	has_many :risks, :dependent => :destroy
	has_many :factors, :through => :risks
	
	has_many :bacterial_agents, :dependent => :destroy
	has_many :bacteria, :through => :bacterial_agents
	
	
	has_many :viral_agents, :dependent => :destroy
	has_many :viruses, :through => :viral_agents
	
	
	has_many :protozoal_agents, :dependent => :destroy
	has_many :protozoas, :through => :protozoal_agents
		
	has_many :toxic_agents, :dependent => :destroy
	has_many :toxins, :through => :toxic_agents
	
	has_many :foods, :dependent => :destroy
	
	belongs_to :origin
	belongs_to :user
	has_one :reporter
	has_one :response
	
	has_many :measures, :through => :response
	
	has_attached_file :report, :path => "../../../../home/f0/html/efoss/uploads/:attachment/:id/:style/:filename", :url => "../../../../home/f0/html/efoss/uploads/:attachment/:id/:style/:filename"
	 
	accepts_nested_attributes_for :incidents, :allow_destroy => true
	#accepts_nested_attributes_for :locations, :allow_destroy => true, :reject_if => proc { |attrs| attrs.all? { |k, v| v.blank? } }
	accepts_nested_attributes_for :risks, :allow_destroy => true
	accepts_nested_attributes_for :bacterial_agents, :allow_destroy => true
	accepts_nested_attributes_for :viral_agents, :allow_destroy => true
	accepts_nested_attributes_for :protozoal_agents, :allow_destroy => true
	accepts_nested_attributes_for :toxic_agents, :allow_destroy => true
	accepts_nested_attributes_for :reporter, :allow_destroy => true
	accepts_nested_attributes_for :foods, :allow_destroy => true
	accepts_nested_attributes_for :response, :allow_destroy => true
	accepts_nested_attributes_for :measures, :allow_destroy => true
	
	
	validates_associated :incidents
	validates_associated :reporter
	
	validates_numericality_of :year, :only_integer => true, :message => "%{value} is not a number"
	validates_numericality_of :total_affected, :only_integer => true, :allow_nil => true, :message => "%{value} is not a number"
	validates_numericality_of :at_risk, :only_integer => true, :allow_nil => true, :message => "%{value} is not a number"
	validates_numericality_of :admitted, :only_integer => true, :allow_nil => true, :message => "%{value} is not a number"
	validates_numericality_of :died, :only_integer => true, :allow_nil => true, :message => "%{value} is not a number"
	validates_presence_of :outbreak_type, :transmission_mode, :user_reference
	
	named_scope :distinct, :select => "distinct outbreaks.*"
	named_scope :onset_first_year, lambda {|*args| {:conditions => ["onset_first BETWEEN ? AND ?", Date.parse(args[0]), Date.parse(args[1]) ]} }
	named_scope :onset_last_year, lambda {|*args| {:conditions => ["onset_last BETWEEN ? AND ?", Date.parse(args[0]), Date.parse(args[1]) ]} }
	named_scope :point_source_year, lambda {|*args| {:conditions => ["point_source_date BETWEEN ? AND ?", Date.parse(args[0]), Date.parse(args[1]) ]} }
	
	def agent_name
		if self.aetiology == "BACTERIUM"
			ba = self.bacterial_agents.first
			name = ba ? ba.bacterium.name : "UNKNOWN BACTERIUM"
			return name
		elsif self.aetiology == "VIRUS"
			va = self.viral_agents.first
			name = va ? va.virus.name : "UNKNOWN VIRUS"
			return name
		elsif self.aetiology == "PROTOZOA"
			pa = self.protozoal_agents.first
			name = pa ? pa.protozoa.name : "UNKNOWN PROTOZOA"
			return name
		elsif self.aetiology == "TOXIN"
			ta = self.toxic_agents.first
			name = ta ? ta.toxin.name : "UNKNOWN TOXIN"
			return name
		else
			return "UNKNOWN"	
		end
	end
	
	#new_or_existing risk calls
	def new_risk_attributes=(risk_attributes)
	    risk_attributes.each do |attributes|
	      risks.build(attributes)
	    end
	end
	
	def existing_risk_attributes=(risk_attributes)
	    risks.reject(&:new_record?).each do |risk|
		      attributes = risk_attributes[risk.id.to_s]
		      if attributes
		      	      if attributes['_destroy'] == '1'
		      	      	      risks.delete(risk)
		      	      else
		      	      	      risk.attributes = attributes
		      	      end
		      else
		      	      risk.delete(risk)
		      end
	    end
    	end
	
	def save_risks
	    risks.each do |risk|
	      risk.save(false)
	    end
    	end
    	
    	#new_or_existing food calls
    	def new_food_attributes=(food_attributes)
    		food_attributes.each do |attributes|
    			foods.build(attributes)
	    end
	end
	
	def existing_food_attributes=(food_attributes)
		foods.reject(&:new_record?).each do |food|
			attributes = food_attributes[food.id.to_s]
		      if attributes
		      	      if attributes['_destroy'] == '1'
		      	      	      foods.delete(food)
		      	      else
		      	      	      food.attributes = attributes
		      	      end
		      else
		      	      food.delete(food)
		      end
	    end
    	end
	
	def save_foods
	    foods.each do |food|
			food.save(false)
	    end
    	end
    	
    	#new_or_existing bacterial_agent calls
    	def new_bacterial_agent_attributes=(bacterial_agent_attributes)
    	    bacterial_agent_attributes.each do |attributes|
    	    	    bacterial_agents.build(attributes)
	    end
	end
	
	def existing_bacterial_agent_attributes=(bacterial_agent_attributes)
	    bacterial_agents.reject(&:new_record?).each do |bacterial_agent|
			attributes = bacterial_agent_attributes[bacterial_agent.id.to_s]
		      
			if attributes
		      	      if attributes['_destroy'] == '1'
		      	      	      bacterial_agents.delete(bacterial_agent)
		      	      else
		      	      	      bacterial_agent.attributes = attributes
		      	      end
		      	else
		      		bacterial_agent.delete(bacterial_agent)
		      	end
	    end
    	end
	
	def save_bacterial_agents
	    bacterial_agents.each do |bacterial_agent|
			bacterial_agent.save(false)
	    end
    	end
    	
    	#new_or_existing viral_agent calls
    	def new_viral_agent_attributes=(viral_agent_attributes)
    	    viral_agent_attributes.each do |attributes|
    	    	    viral_agents.build(attributes)
	    end
	end
	
	def existing_viral_agent_attributes=(viral_agent_attributes)
	    viral_agents.reject(&:new_record?).each do |viral_agent|
			attributes = viral_agent_attributes[viral_agent.id.to_s]
		      if attributes
		      	      if attributes['_destroy'] == '1'
		      	      	      viral_agents.delete(viral_agent)
		      	      else
		      	      	      viral_agent.attributes = attributes
		      	      end
		      else
		      		viral_agent.delete(viral_agent)
		      end
			
	    end
    	end
	
	def save_viral_agents
	    viral_agents.each do |viral_agent|
			viral_agent.save(false)
	    end
    	end
    	
    	
    	#new_or_existing protozoal_agent calls
    	def new_protozoal_agent_attributes=(protozoal_agent_attributes)
    	    protozoal_agent_attributes.each do |attributes|
    	    	    protozoal_agents.build(attributes)
	    end
	end
	
	def existing_protozoal_agent_attributes=(protozoal_agent_attributes)
	    protozoal_agents.reject(&:new_record?).each do |protozoal_agent|
			attributes = protozoal_agent_attributes[protozoal_agent.id.to_s]
		      if attributes
		      	      if attributes['_destroy'] == '1'
		      	      	      protozoal_agents.delete(protozoal_agent)
		      	      else
		      	      	      protozoal_agent.attributes = attributes
		      	      end
		      else
		      		protozoal_agent.delete(protozoal_agent)
		      end
			
	    end
    	end
	
	def save_protozoal_agents
	    protozoal_agents.each do |protozoal_agent|
			protozoal_agent.save(false)
	    end
    	end
    	
    	#new_or_existing toxic_agent calls
    	def new_toxic_agent_attributes=(toxic_agent_attributes)
    	    toxic_agent_attributes.each do |attributes|
    	    	    toxic_agents.build(attributes)
	    end
	end
	
	def existing_toxic_agent_attributes=(toxic_agent_attributes)
	    toxic_agents.reject(&:new_record?).each do |toxic_agent|
			attributes = toxic_agent_attributes[toxic_agent.id.to_s]
		      if attributes
		      	      if attributes['_destroy'] == '1'
		      	      	      toxic_agents.delete(toxic_agent)
		      	      else
		      	      	      toxic_agent.attributes = attributes
		      	      end
		      else
		      		toxic_agent.delete(toxic_agent)
		      end
			
	    end
    	end
	
	def save_toxic_agents
	    toxic_agents.each do |toxic_agent|
			toxic_agent.save(false)
	    end
    	end
    	

	
	# def combined_agent_query(params)
		# ba_outbreaks = Array.new
		# vi_outbreaks = Array.new
		# pr_outberaks = Array.new
		# to_outbreaks = Array.new
		# if params[:bacterial_agents_bacterium_name_like_any]
			# ba_outbreaks = Outbreak.bacterial_agents_bacterium_name_like_any(params[:bacterial_agents_bacterium_name_like_any])	
		# end
		# if params[:viral_agents_virus_name_like_any]
			# vi_outbreaks = Outbreak.viral_agents_virus_name_like_any(params[:viral_agents_virus_name_like_any])
		# end
		# if params[:protozoal_agents_protozoa_name_like_any]
			# pr_outbreaks = Outbreak.protozoal_agents_protozoa_name_like_any(params[:protozoal_agents_protozoa_name_like_any])
		# end
		# if params[:toxic_agents_toxin_name_like_any]
			# to_outbreaks = Outbreak.toxic_agents_toxin_name_like_any(params[:toxic_agents_toxin_name_like_any])
		# end
		# outbreak_selection = ba_outbreaks + vi_outbreaks + pr_outbreaks + to_outbreaks
		# outbreak_selection.uniq!
		# return outbreak_selection
		# 
	# end
	
end
