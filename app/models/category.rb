class Category < ActiveRecord::Base
	has_many :incidents
	has_many :subcategories
	has_many :subtypes, :through => :subcategories
end
