class Protozoa < ActiveRecord::Base
	has_many :protozoal_agents
	has_many :outbreaks, :through => :protozoal_agents
end
