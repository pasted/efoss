class Response < ActiveRecord::Base
	belongs_to :outbreak
	has_many :measures
	
	accepts_nested_attributes_for :measures, :allow_destroy => true
	
	#new_or_existing measure calls
    	def new_measure_attributes=(measure_attributes)
    	    measure_attributes.each do |attributes|
    		measures.build(attributes)
	    end
	end
	
	def existing_measure_attributes=(measure_attributes)
	   measures.reject(&:new_record?).each do |measure|
			attributes = measure_attributes[measure.id.to_s]
		      if attributes
		      	      if attributes['_destroy'] == '1'
		      	      	      measures.delete(measure)
		      	      else
		      	      	      measure.attributes = attributes
		      	      end
		      else
		      	      measure.delete(measure)
		      end
	    end
    	end
	
	def save_measures
	    measures.each do |measure|
			measure.save(false)
	    end
    	end
end
