class Pathogen < ActiveRecord::Base
	
	has_many :outbreaks, :through => :infections
	belongs_to :bacteria
	belongs_to :virus
	belongs_to :protozoa

end
