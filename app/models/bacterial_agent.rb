class BacterialAgent < ActiveRecord::Base
	
	belongs_to :outbreak
	belongs_to :bacterium
	has_one :investigation, :as => :investigatable, :dependent => :destroy
	
	accepts_nested_attributes_for :investigation, :allow_destroy => true
	
	attr_accessor :_destroy
end
