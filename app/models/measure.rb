class Measure < ActiveRecord::Base
	belongs_to :response
	has_one :outbreak, :through => :response
	
	attr_accessor :_destroy
end
