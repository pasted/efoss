class Food < ActiveRecord::Base
	belongs_to :outbreak
	belongs_to :food_category
	belongs_to :food_subcategory
	has_one :evidence, :as => :evident, :dependent => :destroy
	
	accepts_nested_attributes_for :evidence, :allow_destroy => true
	
	attr_accessor :_destroy
end
