class LocalAuthority < ActiveRecord::Base
	belongs_to :hpu
	belongs_to :region
	has_many :locations
	
	attr_accessible :name, :region_id, :hpu_id
end
