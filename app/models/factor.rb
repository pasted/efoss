class Factor < ActiveRecord::Base
	has_many :risks
	has_many :outbreaks, :through => :risks
end
