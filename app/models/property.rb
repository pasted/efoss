class Property < ActiveRecord::Base
	
		
  # LG named scopes for convenience methods to return attribute in groups.
  # named_scope :mab, :conditions => {:field => :mab}
  # named_scope :serogroup, :conditions => {:field => :serogroup}
  # named_scope :context, :conditions => {:field => :context}
  # named_scope :source, :conditions => {:field => :source}
  # named_scope :title, :conditions => {:field => :title}
  # named_scope :gender, :conditions => {:field => :gender}
  
  
  # checks to see if a named_scope is defined on this field key
  after_save :check_scopes
  
  class << self 
  	# LG Actually could do something like this to create a named scope for every distinct field name.
  	def load_scopes
  		find(:all, :select => "DISTINCT field").each do |attribute|
  	    load_scope(attribute.field)
  	  end
  	end
  	
  	def load_scope(key)
  	  named_scope(key, :conditions => { :field => key })
  	end
  	
  	def has_scope?(key)
  	  scopes.detect{|scope| scope.first.to_s==key.to_s}.present?
  	end	
  end
  
  # This must be called here after the function is defined above.
  load_scopes	
  
  protected
  
  def check_scopes
  	self.class.load_scope(field) unless self.class.has_scope?(field)
  end
end
