class Risk < ActiveRecord::Base
	belongs_to :outbreak
	belongs_to :factor
	
	attr_accessible :detail, :factor_id, :outbreak_id
	attr_accessor :_destroy
end
