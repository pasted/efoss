class Virus < ActiveRecord::Base
	
	has_many :viral_agents
	has_many :outbreaks, :through => :viral_agents
end
