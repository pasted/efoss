class Subcategory < ActiveRecord::Base
	has_many :incidents
	has_many :subtypes
	belongs_to :category
end
