class Reporter < ActiveRecord::Base
	belongs_to :outbreak
	has_one :location
	
	validates_presence_of :fname, :lname, :email
end
