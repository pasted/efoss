class Evidence < ActiveRecord::Base
	belongs_to :evident, :polymorphic => true
	belongs_to :evidence_category
end
