class Incident < ActiveRecord::Base
	belongs_to :outbreak
	belongs_to :location
	belongs_to :category
	belongs_to :subcategory
	belongs_to :subtype
	belongs_to :cuisine
	has_one :evidence, :as => :evident
	
	accepts_nested_attributes_for :location, :allow_destroy => true
	accepts_nested_attributes_for :evidence, :allow_destroy => true
	
	validates_presence_of :category_id, :subcategory_id, :subtype_id 
	validates_associated :location
end
