class Hpu < ActiveRecord::Base
	has_many :locations
	has_many :local_authorities
	belongs_to :region
end
