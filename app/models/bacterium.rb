class Bacterium < ActiveRecord::Base
	has_many :bacterial_agents
	has_many :outbreaks, :through => :bacterial_agents
end
