class ProtozoalAgent < ActiveRecord::Base
	
	belongs_to :outbreak
	belongs_to :protozoa
	has_one :investigation, :as => :investigatable
	
	accepts_nested_attributes_for :investigation, :allow_destroy => true
	
	attr_accessor :_destroy
	
end
