class FoodSubcategory < ActiveRecord::Base
	has_many :foods
	belongs_to :food_category
end
