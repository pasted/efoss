class Location < ActiveRecord::Base
	
	attr_encrypted :placename, :key => LOCK
	attr_encrypted :category, :key => LOCK
	attr_encrypted :address_1, :key => LOCK
	attr_encrypted :address_2, :key => LOCK
	attr_encrypted :address_3, :key => LOCK
	attr_encrypted :town, :key => LOCK
	attr_encrypted :postcode, :key => LOCK
	#lon / lat are currently type : double precision, would probably have to be change to character varying (255)
	#attr_encrypted :lon, :key => LOCK
	#attr_encrypted :lat, :key => LOCK
	
	has_many :incidents
	has_many :outbreaks, :through => :incidents
	has_many :categories, :through => :incidents
	has_many :subcategories, :through => :incidents
	has_many :subtypes, :through => :incidents
	
	has_many :contacts
	has_many :users, :through => :contacts
      
	belongs_to :reporter
	belongs_to :hpu
	belongs_to :region
	belongs_to :local_authority
	
	validates_presence_of :hpu_id, :region_id, :local_authority_id
	
	before_save :check_postcode
	#set postcode to NATIONAL if nil
	def check_postcode
		self.postcode = 'NATIONAL' unless self.postcode
		self.find_lonlat(self.postcode)
		self.encrypted_postcode
	end	
	
	def find_lonlat(postcode)
		
		if postcode
			
			postcode.strip!
			postcode.upcase!
			if postcode == "NATIONAL"
				this_postcode = Postcode.find_by_postcode(postcode)
				
				self.lon = this_postcode.lon.to_f
				self.lat = this_postcode.lat.to_f
				self.town = this_postcode.ward
				self.encrypted_town
			
				begin
					@region = Region.find_by_name(this_postcode.region.upcase)
					self.region_id = @region.id
				rescue
						
				end
					
				begin
					@hpu = Hpu.find_by_name(this_postcode.hpu.upcase)
					self.hpu_id = @hpu.id
				rescue
						
				end
					
				begin
					@local_authority = LocalAuthority.find_by_name(this_postcode.local.upcase)
					self.local_authority_id = @local_authority.id
				rescue
						
				end
				@reply = 'postcode_found'
			elsif postcode.include?(' ')
				
				begin
					this_postcode = Postcode.find_by_postcode(postcode)
					
					self.lon = this_postcode.lon.to_f
					self.lat = this_postcode.lat.to_f
					self.town = this_postcode.ward
					self.encrypted_town
					begin
					        @region = Region.find_by_name(this_postcode.region.upcase)
						self.region_id = @region.id
					rescue StandardError => e
						puts this_postcode.inspect
						raise e.inspect
					end
					
					begin
						@hpu = Hpu.find_by_name(this_postcode.hpu.upcase)
						self.hpu_id = @hpu.id
					rescue StandardError => e
						puts this_postcode.inspect
						raise e.inspect
					end
					
					begin
						@local_authority = LocalAuthority.find_by_name(this_postcode.local.upcase)
						self.local_authority_id = @local_authority.id
					rescue StandardError => e
						puts this_postcode.insect
						raise e.inspect
					end
					
					if this_postcode
						
						@reply = 'postcode_found'
					else
						if postcode =~ /^((([A-PR-UWYZ][0-9])|([A-PR-UWYZ][0-9][0-9])|([A-PR-UWYZ][A-HK-Y][0-9])|([A-PR-UWYZ][A-HK-Y][0-9][0-9])|([A-PR-UWYZ][0-9][A-HJKSTUW])|([A-PR-UWYZ][A-HK-Y][0-9][ABEHMNPRVWXY]))\s?([0-9][ABD-HJLNP-UW-Z]{2})|(GIR)\s?(0AA))$/ 
							@reply = 'regex_pass_postcode_not_found'
						else
							@reply = 'regex_fail_postcode_not_found'
						end
					end
				rescue StandardError => e
					
				       puts e.inspect
				       @reply = 'query_error'	
				end
			else
				@reply = 'no_spaces_error'	
			end
			
		else
			@reply = 'param_error'
		end
                
	end
end
