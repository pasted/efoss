class Infection < ActiveRecord::Base
	belongs_to :pathogen
	belongs_to :outbreak
end
