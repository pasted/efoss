class User < ActiveRecord::Base
  
  has_many :outbreaks
  has_and_belongs_to_many :roles
  
  # Include default devise modules. Others available are:
  # :http_authenticatable, :token_authenticatable,  :confirmable, :lockable, :timeoutable and :activatable
  # devise :registerable, :database_authenticatable, :recoverable,
  #      :rememberable, :trackable, :validatable
  devise :database_authenticatable, :confirmable, :recoverable, :lockable, :timeoutable, :trackable, :rememberable, :validatable, :registerable
  
  
  attr_accessible :email, :password, :password_confirmation, :fname, :lname, :location_id, :confirmed_at, :remember_me, :role_ids
  #accepts_nested_attributes_for :roles, :allow_destroy => true
  
  before_save :lowercase_email
  
  def role?(role)
  	  return !!self.roles.find_by_name(role.to_s.downcase)
  end
  
  # new function to set the password without knowing the current password used in our confirmation controller. 
  def attempt_set_password(params)
  	  p = {}
  	  p[:password] = params[:password]
  	  p[:password_confirmation] = params[:password_confirmation]
  	  update_attributes(p)
  end

  # new function to return whether a password has been set
  def has_no_password?
  	  self.encrypted_password.blank?
  end

  # new function to provide access to protected method unless_confirmed
  def only_if_unconfirmed
  	  unless_confirmed {yield}
  end
 
  def lowercase_email
  	  self.email.downcase!	  
  end
  
end
