class PathogensController < ApplicationController


  def get_pathogen
  	  pathogen = params[:pathogen_category].strip
  	  case pathogen
  	  when "VIRUS"
  	  	  @viruses = Virus.all
  	  	  render :partial => "viruses/dropdown_select", :locals => { :viruses => @viruses }  
  	  when "BACTERIUM"
  	  	  @bacteria = Bacterium.all
  	  	  render :partial => "bacteria/dropdown_select", :locals => { :bacteria => @bacteria }  
  	  when "PROTOZOA"
  	  	  @protozoas = Protozoa.all
  	  	  render :partial => "protozoas/dropdown_select", :locals => { :protozoas => @protozoas }
  	  when "TOXIN"
  	  	  @toxin = Toxin.new
  	  	  render :partial => "toxins/dropdown_select", :locals => { :toxin => @toxin }
  	  end
  end	
  
	
  # GET /pathogens
  # GET /pathogens.xml
  def index
    @pathogens = Pathogen.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @pathogens }
    end
  end

  # GET /pathogens/1
  # GET /pathogens/1.xml
  def show
    @pathogen = Pathogen.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @pathogen }
    end
  end

  # GET /pathogens/new
  # GET /pathogens/new.xml
  def new
    @pathogen = Pathogen.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @pathogen }
    end
  end

  # GET /pathogens/1/edit
  def edit
    @pathogen = Pathogen.find(params[:id])
  end

  # POST /pathogens
  # POST /pathogens.xml
  def create
    @pathogen = Pathogen.new(params[:pathogen])

    respond_to do |format|
      if @pathogen.save
        flash[:notice] = 'Pathogen was successfully created.'
        format.html { redirect_to(@pathogen) }
        format.xml  { render :xml => @pathogen, :status => :created, :location => @pathogen }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @pathogen.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /pathogens/1
  # PUT /pathogens/1.xml
  def update
    @pathogen = Pathogen.find(params[:id])

    respond_to do |format|
      if @pathogen.update_attributes(params[:pathogen])
        flash[:notice] = 'Pathogen was successfully updated.'
        format.html { redirect_to(@pathogen) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @pathogen.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /pathogens/1
  # DELETE /pathogens/1.xml
  def destroy
    @pathogen = Pathogen.find(params[:id])
    @pathogen.destroy

    respond_to do |format|
      format.html { redirect_to(pathogens_url) }
      format.xml  { head :ok }
    end
  end
end
