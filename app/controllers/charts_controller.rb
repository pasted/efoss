class ChartsController < ApplicationController
		def total_outbreaks_by_month_year
			 
			  
			  #find earliest onset date
			  #@earliest_outbreak = Outbreak.first(:order => :onset_first)
			  
			  #start_date = @earliest_outbreak.onset_first.at_beginning_of_year
			 
			  #hardcoded start date
			  start_date = Date.parse(START_DATE)
			  
			  @start_year = start_date.year
			  @start_month = start_date.month
			  @start_day = start_date.day
			  end_date = Date.today.at_end_of_year
			  @dates = Hash.new
			  until start_date > end_date
				  end_of_month = start_date.at_end_of_month
				  monthly_collection = Outbreak.find(:all, :conditions => ["(onset_first BETWEEN ? AND ?)", start_date, end_of_month]) 	  	  
				  
				  if @dates.key?(start_date.year)
					  existing_month_hash = @dates[start_date.year]
					  existing_month_hash[start_date.month] = monthly_collection
					  @dates[start_date.year] = existing_month_hash
				  else
					  month_hash = Hash.new
					  month_hash[start_date.month] = monthly_collection
					  @dates[start_date.year] = month_hash
				  end
				  
				  start_date = start_date + 1.month
			  end
			  @category_series = ""
			  @outbreak_series = ""
			  @ordered_series = Hash.new
			  @years = Array.new
			  #raise @dates.inspect
			  @dates.each_pair do |this_year, this_date|
				  temp_data = "name: #{this_year},"
				  temp_data += " data: ["
				  
				  temp_array = Array.new
				  #raise this_date.inspect
				  this_date.each_pair do |this_month, outbreaks|
					 temp_array.push(outbreaks.length)
				  end
				  
				 temp_data << temp_array.join(",")
				 temp_data << "]"
				 
				 @ordered_series[this_year] = temp_data
				 @years.push(this_year)
			  end
			  @years.sort! { |a,b| a <=> b}
			  
		  end
		  
		  def foodborne_non_foodborne_by_year
		  	  start_year = Date.parse(START_DATE).year
		  	  end_year = Date.today.year
		  	  year_range = Range.new(start_year,end_year)
		  	  @years = Array.new
		  	  @foodborne_store = Hash.new
		  	  @nonfoodborne_store = Hash.new
		  	  @total_store = Hash.new
		  	  year_range.each do |this_year|
		  	  	  
		  	  	  foodborne_search = Outbreak.search({:outbreak_type => 'FOODBORNE', :year => this_year})
		  	  	  foodborne_outbreaks = foodborne_search.find(:all)
		  	  	  @foodborne_store[this_year] = foodborne_outbreaks.length
		  	  	  
		  	  	  nonfoodborne_search = Outbreak.search({:outbreak_type => 'NON-FOODBORNE', :year => this_year})
		  	  	  nonfoodborne_outbreaks = nonfoodborne_search.find(:all)
		  	  	  @nonfoodborne_store[this_year] = nonfoodborne_outbreaks.length
		  	  	  @total_store[this_year] = foodborne_outbreaks.length + nonfoodborne_outbreaks.length
		  	  	  @years.push(this_year)
		  	  end
		  	  @years.sort! { |a,b| a <=> b}
		  
	  	  end
		  
	  	  def pathogens_by_year
	  	  	  start_year = Date.parse(START_DATE).year
	  	  	  end_year = Date.today.year
	  	  	  year_range = Range.new(start_year, end_year)
	  	  	  @years = Array.new
	  	  	  @bacterial_agent_store = Hash.new
	  	  	  @viral_agent_store = Hash.new
	  	  	  @protozoal_agent_store = Hash.new
	  	  	  @toxic_agent_store = Hash.new

	  	  	  year_range.each do |this_year|
	  	  	  	  
	  	  	  	  bacterial_agent_search = Outbreak.search({:aetiology => 'BACTERIUM', :year => this_year})
	  	  	  	  bacterial_agents = bacterial_agent_search.find(:all)
	  	  	  	  @bacterial_agent_store[this_year] = bacterial_agents.length

	  	  	  	  viral_agent_search = Outbreak.search({:aetiology => 'VIRUS', :year => this_year})
	  	  	  	  viral_agents = viral_agent_search.find(:all)
	  	  	  	  @viral_agent_store[this_year] = viral_agents.length
	  	  	  	  
	  	  	  	  protozoal_agent_search = Outbreak.search({:aetiology => 'PROTOZOA', :year => this_year})
	  	  	  	  protozoal_agents = protozoal_agent_search.find(:all)
	  	  	  	  @protozoal_agent_store[this_year] = protozoal_agents.length
	  	  	  	  
	  	  	  	  toxic_agent_search = Outbreak.search({:aetiology => 'TOXIN', :year => this_year})
	  	  	  	  toxic_agents = toxic_agent_search.find(:all)
	  	  	  	  @toxic_agent_store[this_year] = toxic_agents.length
	  	  	  	  @years.push(this_year)
	  	  	  end
	  	  	  
	  	  	  @years.sort! { |a,b| a <=> b}
	  	  	  
	  	  end
	  	  
	  	  def foodtypes_by_year
	  	  	  start_year = Date.parse(START_DATE).year
	  	  	  end_year = Date.today.year
	  	  	  year_range = Range.new(start_year, end_year)
	  	  	  @years = Array.new
	  	  	  @poultry_store = Hash.new
	  	  	  @red_meat_store = Hash.new
	  	  	  @finfish_store = Hash.new
	  	  	  @potable_water_store = Hash.new
	  	  	  @rice_store = Hash.new
	  	  	  @other_foods_store = Hash.new
	  	  	  @unknown_store = Hash.new
	  	  	  @mixed_meats_store = Hash.new
	  	  	  @crustacea_shellfish_store = Hash.new
	  	  	  @mixed_finfish_shellfish_store = Hash.new
	  	  	  @veg_fruit_store = Hash.new
	  	  	  @sauces_store = Hash.new
	  	  	  @desserts_confectionery_store = Hash.new
	  	  	  @eggs_store = Hash.new
	  	  	  @milk_store = Hash.new
	  	  	  @mixed_foods_store = Hash.new

	  	  	  

	  	  	  year_range.each do |this_year|
	  	  	  	  
	  	  	  	  poultry_search = Outbreak.search({:foods_food_category_name_like => 'POULTRY MEAT', :year => this_year})
	  	  	  	  poultry = poultry_search.find(:all)
	  	  	  	  @poultry_store[this_year] = poultry.length

	  	  	  	  red_meat_search = Outbreak.search({:foods_food_category_name_like => 'RED MEAT', :year => this_year})
	  	  	  	  red_meat = red_meat_search.find(:all)
	  	  	  	  @red_meat_store[this_year] = red_meat.length
	  	  	  	  
	  	  	  	  finfish_search = Outbreak.search({:foods_food_category_name_like => 'FINFISH', :year => this_year})
	  	  	  	  finfish = finfish_search.find(:all)
	  	  	  	  @finfish_store[this_year] = finfish.length
	  	  	  	  
	  	  	  	  potable_water_search = Outbreak.search({:foods_food_category_name_like => 'POTABLE WATER', :year => this_year})
	  	  	  	  potable_water = potable_water_search.find(:all)
	  	  	  	  @potable_water_store[this_year] = potable_water.length
	  	  	  	  
	  	  	  	  rice_search = Outbreak.search({:foods_food_category_name_like => 'RICE', :year => this_year})
	  	  	  	  rice = rice_search.find(:all)
	  	  	  	  @rice_store[this_year] = rice.length
	  	  	  	  
	  	  	  	  other_foods_search = Outbreak.search({:foods_food_category_name_like => 'OTHER FOODS', :year => this_year})
	  	  	  	  other_foods = other_foods_search.find(:all)
	  	  	  	  @other_foods_store[this_year] = other_foods.length
	  	  	  	  
	  	  	  	  unknown_search = Outbreak.search({:foods_food_category_name_like => 'UNKNOWN', :year => this_year})
	  	  	  	  unknown = unknown_search.find(:all)
	  	  	  	  @unknown_store[this_year] = unknown.length
	  	  	  	  
	  	  	  	  mixed_meats_search = Outbreak.search({:foods_food_category_name_like => 'OTHER + MIXED MEATS', :year => this_year})
	  	  	  	  mixed_meats = mixed_meats_search.find(:all)
	  	  	  	  @mixed_meats_store[this_year] = mixed_meats.length
	  	  	  	  
	  	  	  	  crustacea_shellfish_search = Outbreak.search({:foods_food_category_name_like => 'CRUSTACEA + SHELLFISH', :year => this_year})
	  	  	  	  crustacea_shellfish = crustacea_shellfish_search.find(:all)
	  	  	  	  @crustacea_shellfish_store[this_year] = crustacea_shellfish.length
	  	  	  	  
	  	  	  	  mixed_finfish_shellfish_search = Outbreak.search({:foods_food_category_name_like => 'MIXED FINFISH + CRUSTACEA | SHELLFISH', :year => this_year})
	  	  	  	  mixed_finfish_shellfish = mixed_finfish_shellfish_search.find(:all)
	  	  	  	  @mixed_finfish_shellfish_store[this_year] = mixed_finfish_shellfish.length
	  	  	  	  
	  	  	  	  veg_fruit_search = Outbreak.search({:foods_food_category_name_like => 'VEGETABLES + FRUIT', :year => this_year})
	  	  	  	  veg_fruit = veg_fruit_search.find(:all)
	  	  	  	  @veg_fruit_store[this_year] = veg_fruit.length
	  	  	  	  
	  	  	  	  sauces_search = Outbreak.search({:foods_food_category_name_like => 'CONDIMENTS + SAUCES', :year => this_year})
	  	  	  	  sauces = sauces_search.find(:all)
	  	  	  	  @sauces_store[this_year] = sauces.length
	  	  	  	  
	  	  	  	  desserts_confectionery_search = Outbreak.search({:foods_food_category_name_like => 'DESSERTS, CAKES + CONFECTIONERY', :year => this_year})
	  	  	  	  desserts_confectionery = desserts_confectionery_search.find(:all)
	  	  	  	  @desserts_confectionery_store[this_year] = desserts_confectionery.length
	  	  	  	  
	  	  	  	  eggs_search = Outbreak.search({:foods_food_category_name_like => 'EGGS + EGG DISHES', :year => this_year})
	  	  	  	  eggs = eggs_search.find(:all)
	  	  	  	  @eggs_store[this_year] = eggs.length
	  	  	  	  
	  	  	  	  milk_search = Outbreak.search({:foods_food_category_name_like => 'MILK + DAIRY PRODUCTS', :year => this_year})
	  	  	  	  milk = milk_search.find(:all)
	  	  	  	  @milk_store[this_year] = milk.length
	  	  	  	  
	  	  	  	  mixed_foods_search = Outbreak.search({:foods_food_category_name_like => 'COMPOSITE | MIXED FOODS', :year => this_year})
	  	  	  	  mixed_foods = mixed_foods_search.find(:all)
	  	  	  	  @mixed_foods_store[this_year] = mixed_foods.length
	  	  	  	  
	  	  	  	  @years.push(this_year)
	  	  	  end
	  	  	  
	  	  	  @years.sort! { |a,b| a <=> b}
	  	  	  
	  	  end
	  	  
	  	  def causative_pathogen_by_transmission
	  	  	  
	  	  	  @transmission_modes = Array['WATER (RECREATIONAL)','ANIMAL CONTACT','PERSON TO PERSON (VTEC ONLY)', 'OTHER | OUTDOOR EVENTS']
	  	  	  @vtec_store = Hash.new
	  	  	  @salmonella_typhi_store = Hash.new
	  	  	  @cryptosporidium_store = Hash.new
	  	  	  @norovirus_store = Hash.new
	  	  	  @girdia_store = Hash.new
	  	  	  @campylobacter_store = Hash.new
	  	  	  @salmonella_enteri_store = Hash.new

	  	  	  
	  	  	  @transmission_modes.each do |this_mode|
	  	  	  	vtec_search = Outbreak.search({:bacterial_agents_category_like => 'CAUSATIVE', :bacterial_agents_bacterium_name_like => 'VTEC O157', :transmission_mode_like => "#{this_mode}"})
	  	  	  	vtec = vtec_search.find(:all)
	  	  	  	@vtec_store[this_mode] = vtec.length
	  	  	  	
	  	  	  	salmonella_typhi_search = Outbreak.search({:bacterial_agents_category_like => 'CAUSATIVE', :bacterial_agents_bacterium_name_like => 'SALMONELLA TYPHIMURIUM', :transmission_mode_like => "#{this_mode}"})
	  	  	  	salmonella_typhi = salmonella_typhi_search.find(:all)
	  	  	  	@salmonella_typhi_store[this_mode] = salmonella_typhi.length
	  	  	  	
	  	  	  	cryptosporidium_search = Outbreak.search({:protozoal_agents_category_like => 'CAUSATIVE', :protozoal_agents_protozoa_name_like => 'CRYPTOSPORIDIUM SPP.', :transmission_mode_like => "#{this_mode}"})
	  	  	  	cryptosporidium = cryptosporidium_search.find(:all)
	  	  	  	@cryptosporidium_store[this_mode] = cryptosporidium.length
	  	  	  	
	  	  	  	norovirus_search = Outbreak.search({:viral_agents_category_like => 'CAUSATIVE', :viral_agents_virus_name_like => 'NOROVIRUS', :transmission_mode_like => "#{this_mode}"})
	  	  	  	norovirus = norovirus_search.find(:all)
	  	  	  	@norovirus_store[this_mode] = norovirus.length
	  	  	  	
	  	  	  	girdia_search = Outbreak.search({:protozoal_agents_category_like => 'CAUSATIVE', :protozoal_agents_protozoa_name_like => 'GIARDIA LAMBLIA', :transmission_mode_like => "#{this_mode}"})
	  	  	  	girdia = girdia_search.find(:all)
	  	  	  	@girdia_store[this_mode] = girdia.length
	  	  	  	
	  	  	  	campylobacter_search = Outbreak.search({:bacterial_agents_category_like => 'CAUSATIVE', :bacterial_agents_bacterium_name_like => 'CAMPYLOBACTER SPP.', :transmission_mode_like => "#{this_mode}"})
	  	  	  	campylobacter = campylobacter_search.find(:all)
	  	  	  	@campylobacter_store[this_mode] = campylobacter.length
	  	  	  	
	  	  	  	salmonella_enteri_search = Outbreak.search({:bacterial_agents_category_like => 'CAUSATIVE', :bacterial_agents_bacterium_name_like => 'SALMONELLA ENTERITIDIS', :transmission_mode_like => "#{this_mode}"})
	  	  	  	salmonella_enteri = salmonella_enteri_search.find(:all)
	  	  	  	@salmonella_enteri_store[this_mode] = salmonella_enteri.length
	  	  	  	
	  	  	  end
	  	  end
	  	  
	  	  def non_foodborne_transmission_by_year
	  	  	
	  	  	  start_year = Date.parse(START_DATE).year
		  	  end_year = Date.today.year
		  	  year_range = Range.new(start_year,end_year)
	  	  	  @years = Array.new
	  	  	  @rec_water_store = Hash.new
	  	  	  @animal_contact_store = Hash.new
	  	  	  @vtec_store = Hash.new
	  	  	  @outdoor_store = Hash.new

	  	  	  year_range.each do |this_year|
	  	  	  	  
	  	  	  	  rec_water_search = Outbreak.search({:transmission_mode_like => 'WATER (RECREATIONAL)', :year => this_year})
	  	  	  	  rec_waters = rec_water_search.find(:all)
	  	  	  	  @rec_water_store[this_year] = rec_waters.length

	  	  	  	  animal_contact_search = Outbreak.search({:transmission_mode_like => 'ANIMAL CONTACT', :year => this_year})
	  	  	  	  animal_contacts = animal_contact_search.find(:all)
	  	  	  	  @animal_contact_store[this_year] = animal_contacts.length
	  	  	  	  
	  	  	  	  vtec_search = Outbreak.search({:transmission_mode_like => 'PERSON TO PERSON (VTEC ONLY)', :year => this_year})
	  	  	  	  vtecs = vtec_search.find(:all)
	  	  	  	  @vtec_store[this_year] = vtecs.length
	  	  	  	  
	  	  	  	  outdoor_search = Outbreak.search({:transmission_mode_like => 'OTHER | OUTDOOR EVENTS', :year => this_year})
	  	  	  	  outdoors = outdoor_search.find(:all)
	  	  	  	  @outdoor_store[this_year] = outdoors.length
	  	  	  	  @years.push(this_year)
	  	  	  end
	  	  	  
	  	  	  @years.sort! { |a,b| a <=> b}
	  	  end
	  	  
	  	  
	  	  def major_foodborne_pathogen_by_year
	  	  	   
	  	  	  start_year = Date.parse(START_DATE).year
		  	  end_year = Date.today.year
		  	  year_range = Range.new(start_year,end_year)
	  	  	  @years = Array.new
	  	  	  @salmonella_spp_store = Hash.new
	  	  	  @c_perfringens_store = Hash.new
	  	  	  @vtec_store = Hash.new
	  	  	  @campylobacter_store = Hash.new

	  	  	  year_range.each do |this_year|
	  	  	  	  #using the bacterium genus to bring out all salmonella spp
	  	  	  	  #could also use :bacterial_agents_bacterium_name_like_any => [name_1, name_2]
	  	  	  	  salmonella_spp_search = Outbreak.search({:bacterial_agents_category_like => 'CAUSATIVE', :bacterial_agents_bacterium_genus_like => 'Salmonella', :year => this_year})
	  	  	  	  salmonella_spp = salmonella_spp_search.find(:all)
	  	  	  	  @salmonella_spp_store[this_year] = salmonella_spp.length

	  	  	  	  c_perfringens_search = Outbreak.search({:bacterial_agents_category_like => 'CAUSATIVE', :bacterial_agents_bacterium_name_like => 'CLOSTRIDIUM PERFRINGENS', :year => this_year})
	  	  	  	  c_perfringens = c_perfringens_search.find(:all)
	  	  	  	  @c_perfringens_store[this_year] = c_perfringens.length
	  	  	  	 
	  	  	  	  vtec_search = Outbreak.search({:bacterial_agents_category_like => 'CAUSATIVE', :bacterial_agents_bacterium_name_like => 'VTEC O157', :year => this_year})
	  	  	  	  vtec = vtec_search.find(:all)
	  	  	  	  @vtec_store[this_year] = vtec.length
	  	  	  	  
	  	  	  	  campylobacter_search = Outbreak.search({:bacterial_agents_category_like => 'CAUSATIVE', :bacterial_agents_bacterium_name_like => 'CAMPYLOBACTER SPP.', :year => this_year})
	  	  	  	  campylobacter = campylobacter_search.find(:all)
	  	  	  	  @campylobacter_store[this_year] = campylobacter.length
	  	  	  	  
	  	  	  	  @years.push(this_year)
	  	  	  end
	  	  	  
	  	  	  @years.sort! { |a,b| a <=> b}
	  	  	  
	  	  	  
	  	  end
	  	  
	  	  
	  	  def foodborne_pathogens_total
	  	  	  @pathogens = Array['SALMONELLA SPP.','FOODBORNE VIRUSES','C. PERFRINGENS', 'UNKNOWN', 'CAMPYLOBACTER SPP.', 'VTEC O157', 'SCOMBROTOXIN', 'BACILLUS SPP.', 'S.AUREUS', 'CRYPTOSPORIDIUM','L. MONOCYTOGENES']
	  	  	  @pathogen_store = Hash.new
	  	  	  
	  	  	  salmonella_spp_search = Outbreak.search({:bacterial_agents_category_like => 'CAUSATIVE', :bacterial_agents_bacterium_genus_like => 'Salmonella', :outbreak_type_equals => 'FOODBORNE'})
	  	  	  salmonella_spp = salmonella_spp_search.find(:all)
	  	  	  @pathogen_store['SALMONELLA SPP.'] = salmonella_spp.length
	  	  	  
	  	  	  foodborne_viruses_search = Outbreak.search({:viral_agents_category_like => 'CAUSATIVE', :aetiology => 'VIRUS', :outbreak_type_equals => 'FOODBORNE'})
	  	  	  foodborne_viruses = foodborne_viruses_search.find(:all)
	  	  	  @pathogen_store['FOODBORNE VIRUSES'] = foodborne_viruses.length
	  	  	  
	  	  	  c_perfringens_search = Outbreak.search({:bacterial_agents_category_like => 'CAUSATIVE', :bacterial_agents_bacterium_name_like => 'CLOSTRIDIUM PERFRINGENS', :outbreak_type_equals => 'FOODBORNE'})
	  	  	  c_perfringens = c_perfringens_search.find(:all)
	  	  	  @pathogen_store['CLOSTRIDIUM PERFRINGENS'] = c_perfringens.length
	  	  	  
	  	  	  unknown_search = Outbreak.search({:aetiology => 'UNKNOWN', :outbreak_type_equals => 'FOODBORNE'})
	  	  	  unknown = unknown_search.find(:all)
	  	  	  @pathogen_store['UNKNOWN'] = unknown.length
	  	  	  
	  	  	  campylobacter_spp_search = Outbreak.search({:bacterial_agents_category_like => 'CAUSATIVE', :bacterial_agents_bacterium_name_like => 'CAMPYLOBACTER SPP.', :outbreak_type_equals => 'FOODBORNE'})
	  	  	  campylobacter_spp = campylobacter_spp_search.find(:all)
	  	  	  @pathogen_store['CAMPYLOBACTER SPP.'] = campylobacter_spp.length
	  	  	  
	  	  	  vtec_search = Outbreak.search({:bacterial_agents_category_like => 'CAUSATIVE', :bacterial_agents_bacterium_name_like => 'VTEC O157', :outbreak_type_equals => 'FOODBORNE'})
	  	  	  vtec = vtec_search.find(:all)
	  	  	  @pathogen_store['VTEC O157'] = vtec.length
	  	  	  
	  	  	  scombrotoxin_search = Outbreak.search({:toxic_agents_category_like => 'CAUSATIVE', :toxic_agents_toxin_name_like => 'SCOMBROTOXIN', :outbreak_type_equals => 'FOODBORNE'})
	  	  	  scombrotoxin = scombrotoxin_search.find(:all)
	  	  	  @pathogen_store['SCOMBROTOXIN'] = scombrotoxin.length
	  	  	  
	  	  	  bacillus_search = Outbreak.search({:bacterial_agents_category_like => 'CAUSATIVE', :bacterial_agents_bacterium_genus_like => 'Bacillus', :outbreak_type_equals => 'FOODBORNE'})
	  	  	  bacillus = bacillus_search.find(:all)
	  	  	  @pathogen_store['BACILLUS SPP.'] = bacillus.length
	  	  	  
	  	  	  s_aureus_search = Outbreak.search({:bacterial_agents_category_like => 'CAUSATIVE', :bacterial_agents_bacterium_name_like => 'STAPHYLOCOCCUS AUREUS', :outbreak_type_equals => 'FOODBORNE'})
	  	  	  s_aureus = s_aureus_search.find(:all)
	  	  	  @pathogen_store['S.AUREUS'] = s_aureus.length

	  	  	  cryptosporidium_search = Outbreak.search({:protozoal_agents_category_like => 'CAUSATIVE', :protozoal_agents_protozoa_name_like => 'CRYPTOSPORIDIUM SPP.', :outbreak_type_equals => 'FOODBORNE'})
	  	  	  cryptosporidium = cryptosporidium_search.find(:all)
	  	  	  @pathogen_store['CRYPTOSPORIDIUM'] = cryptosporidium.length
	  	  	  
	  	  	  listeria_search = Outbreak.search({:bacterial_agents_category_like => 'CAUSATIVE', :bacterial_agents_bacterium_name_like => 'LISTERIA MONOCYTOGENES', :outbreak_type_equals => 'FOODBORNE'})
	  	  	  listeria = listeria_search.find(:all)
	  	  	  @pathogen_store['L. MONOCYTOGENES'] = listeria.length
	  	  end
		  
	  	  
	  	  def seasonal_non_foodborne_by_transmission_mode
	  	  	  #NOT IN ROUTES YET
	  	  	  start_date = Date.parse(START_DATE)
			  
			  @start_year = start_date.year
			  @start_month = start_date.month
			  @start_day = start_date.day
			  end_date = Date.today.at_end_of_year
			  @dates = Hash.new
			  
			  
			  until start_date > end_date
				  end_of_month = start_date.at_end_of_month
				  monthly_collection = Outbreak.find(:all, :conditions => ["(onset_first BETWEEN ? AND ?)", start_date, end_of_month]) 	  	  
				  
				  recreational_water = Outbreak.find(:all, :conditions => ["(onset_first BETWEEN ? AND ?) AND outbreak_type ILIKE 'NON-FOODBORNE' AND transmission_mode ILIKE 'WATER (RECREATIONAL)'", start_date, end_of_month])
				  
				  if @dates.key?(start_date.year)
					  existing_month_hash = @dates[start_date.year]
					  existing_month_hash[start_date.month] = monthly_collection
					  @dates[start_date.year] = existing_month_hash
				  else
					  month_hash = Hash.new
					  month_hash[start_date.month] = monthly_collection
					  @dates[start_date.year] = month_hash
				  end
				  
				  start_date = start_date + 1.month
			  end 
	  	  end
end
