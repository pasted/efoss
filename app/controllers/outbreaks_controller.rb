class OutbreaksController < ApplicationController
  require 'country_select'
  require 'uuidtools'
  
  
  def add_foodborne
  	  #defaults to the rjs for the foodborne form	
  end
  
  def add_general
  	  #defaults to the rjs for the non-foodborne (general) form  
  end
  
  def add_investigation
  		  
  end
  
  #add associated_agent call
  def add_associated_agent
  	@agent_type = params[:agent_type]
  	@outbreak_type = params[:outbreak_type]

  	if @outbreak_type == "FOODBORNE" || @outbreak_type == "NON-FOODBORNE"
  		ob_type_query = "INVESTIGATIONS:CATEGORY:" << @outbreak_type
  		@sample_types = Property.find(:all, :conditions => {:field => ob_type_query})
  		render :update do |page|
  			case @agent_type
				when "BACTERIUM"
					@initial_bacterium = Bacterium.find_by_name("SUSPECT BACTERIAL")
					if @initial_bacterium.nil?
						    #Failover if "SUSPECT BACTERIAL" can't be found
						    @initial_bacterium = Bacterium.find(1)    
					 end
					@bacterial_agent = BacterialAgent.new
					@bacterial_agent.category = "ASSOCIATED"
					@bacterial_agent.build_investigation
					@bacteria = Bacterium.all
					page.insert_html :bottom, "associated_sample_listing", :partial => "bacterial_agents/bacterial_agent", :locals => { :bacteria => @bacteria, :bacterial_agent => @bacterial_agent, :initial_bacterium => @initial_bacterium }
				when "VIRUS"
					@initial_virus = Virus.find_by_name("SUSPECT VIRAL")
					if @initial_virus.nil?
						   #Failover if "SUSPECT VIRAL" can't be found
						    @initial_virus = Virus.find(1)	    
					end
					@viral_agent = ViralAgent.new
					@viral_agent.category = "ASSOCIATED"
					@viral_agent.build_investigation
					@viruses = Virus.all
					page.insert_html :bottom, "associated_sample_listing", :partial => "viral_agents/viral_agent", :locals => { :viruses => @viruses, :viral_agent => @viral_agent, :initial_virus => @initial_virus }
				when "PROTOZOA"
					@initial_protozoa = Protozoa.find_by_name("SUSPECT PROTOZOAL")
					if @initial_protozoa.nil?
					 	 #Failover if "SUSPECT PROTOZOA" can't be found
					 	 @initial_protozoa = Protozoa.find(1)	    
					end
					@protozoal_agent = ProtozoalAgent.new
					@protozoal_agent.category = "ASSOCIATED"
					@protozoal_agent.build_investigation
					@protozoas = Protozoa.all
					page.insert_html :bottom, "associated_sample_listing", :partial => "protozoal_agents/protozoal_agent", :locals => { :protozoas => @protozoas, :protozoal_agent => @protozoal_agent, :initial_protozoa => @initial_protozoa }
				when "TOXIN"
					@initial_toxin = Toxin.find_by_name("SUSPECT TOXIN")
					if @initial_toxin.nil?
						#Failover if "SUSPECT TOXIN" can't be found
						@initial_toxin = Toxin.find(1)    
					end
					@toxic_agent = ToxicAgent.new
					@toxic_agent.category = "ASSOCIATED"
					@toxic_agent.build_investigation
					@toxins = Toxin.all
					page.insert_html :bottom, "associated_sample_listing", :partial => "toxic_agents/toxic_agent", :locals => { :toxins => @toxins, :toxic_agent => @toxic_agent, :initial_toxin => @initial_toxin }
			end
				
		end
  	
  	end
	
  end
  
  # Action to activate the Outbreak.
  # Follows Acts_As_State_Machine logic.
  # Checks current user role.
  def activate_state
  	  if current_user.role?('admin')
  	  	  @outbreak = Outbreak.find(params[:outbreak])
  	  	  @outbreak.activate
  	  	  respond_to do |format|
			  if @outbreak.save
				  flash[:notice] = 'Outbreak was successfully activated.'
				  format.html { redirect_to(@outbreak) }
				  format.xml  { head :ok }
			  end
		  end

  	  end
  end
  
  # List all draft state Outbreaks.
  def review
  	  @outbreaks = Outbreak.draft.paginate(:page => params[:page], :per_page => '20')
  	  #, :conditions => ["current_state = ?", "draft"]
  	  #@outbreaks = Outbreak.draft
  	  
  	  respond_to do |format|
  	      format.html # review.html.erb
	      format.xml  { render :xml => @outbreaks }
      	  end
  end
  
  # List all inactive state Outbreaks.
  def inactive_list
  	  @outbreaks = Outbreak.inactive.paginate(:page => params[:page], :per_page => '20') 
  	  
  	  respond_to do |format|
  	      format.html # inactive_list.html.erb
	      format.xml  { render :xml => @outbreaks }  
  	  end
  end

  def add_factor
  	  
  	  factor_id = params['factor_id']
  	  if factor_id
  	  	  @factor = Factor.find(factor_id)
  	  end
  	  @risk = Risk.new
		
  end
  
  # Add a risk to the current Outbreak.
  # Renders the risk_form_partial.
  def add_risk
  	  
  	  factor_id = params['factor_id']
  	  
  	  if factor_id
  	  	  @factor = Factor.find(factor_id)
  	  end
  	 
  	  @next_child_index = params[:next_child_index]
  	 
  	  if !params[:id] == 0
  	  	@outbreak = Outbreak.find(params[:id])
  	  else
  	  	@outbreak = Outbreak.new
		@outbreak.risks.build
		@outbreak.incidents.build
		@outbreak.locations.build
		@outbreak.viral_agents.build
		@outbreak.bacterial_agents.build
		@outbreak.toxic_agents.build
		@outbreak.protozoal_agents.build	  
  	  end
  	  
  	  @risk = Risk.new
  	  @risk.factor_id = @factor.id
  	  
	  render :update do |page|
	  	  page.insert_html :bottom, :risk_list, :partial => 'risk_form_partial', :locals => {:risk => @risk, :outbreak => @outbreak, :factor_id => @factor.id, :next_child_index => @next_child_index}  
	  end	
  end
  
  # Add an investigation to the current Outbreak. 
  # Renders investigations/investigation_form_partial.
  def add_investigation
  	  @next_child_index = params[:this_index]
  	  @agent_investigation = ""
  	  if params[:confirmed] == "true"
  	  	  if params[:outbreak_type] == 'FOODBORNE'
  	  	  	  ob_type_query = "INVESTIGATIONS:CATEGORY:FOODBORNE" 
  	  	  	  @sample_types = Property.find(:all, :conditions => {:field => ob_type_query})
  	  	  elsif params[:outbreak_type] == 'NON-FOODBORNE'
  	  	  	  ob_type_query = "INVESTIGATIONS:CATEGORY:NON-FOODBORNE"
  	  	  	  @sample_types = Property.find(:all, :conditions => {:field => ob_type_query})
  	  	  end
  	  	  	  
		  if params[:agent_type] == 'bacteria'
		  	  @agent_investigation << "outbreak_bacterial_agents_attributes_" << @next_child_index.to_s << "_investigation_attributes_category"
		  	  @agent_type = params[:agent_type]
		  	  @bacteria = Bacterium.find(params[:agent_id])
			  if params[:id]
				@outbreak = Outbreak.find(params[:id])
			  else
				@outbreak = Outbreak.new
				@outbreak.bacterial_agents.build.build_investigation	  
			  end
			  
		  elsif params[:agent_type] == 'protozoa'
		  	  @agent_investigation << "outbreak_protozoal_agents_attributes_" << @next_child_index.to_s << "_investigation_attributes_category"
		  	  @agent_type = params[:agent_type]
		  	  @protozoa = Protozoa.find(params[:agent_id])
			  if params[:id]
				@outbreak = Outbreak.find(params[:id])
			  else
				@outbreak = Outbreak.new
				@outbreak.protozoal_agents.build.build_investigation	  
			  end  
		  elsif params[:agent_type] == 'viruses'
		  	  @agent_investigation << "outbreak_viral_agents_attributes_" << @next_child_index.to_s << "_investigation_attributes_category"
		  	  @agent_type = params[:agent_type]
		  	  @viruses = Virus.find(params[:agent_id])
			  if params[:id]
				@outbreak = Outbreak.find(params[:id])
			  else
				@outbreak = Outbreak.new
				@outbreak.viral_agents.build.build_investigation	  
			  end
		  elsif params[:agent_type] == 'toxins'
		  	  @agent_investigation << "outbreak_toxic_agents_attributes_" << @next_child_index.to_s << "_investigation_attributes_category"
		  	  @agent_type = params[:agent_type]
		  	  @toxins = Toxin.find(params[:agent_id])
			  if params[:id]
				@outbreak = Outbreak.find(params[:id])
			  else
				@outbreak = Outbreak.new
				@outbreak.toxic_agents.build.build_investigation	  
			  end 
		  end
	  end
  	  render :update do |page|
  	  	  if params[:confirmed] == "true"
  	  	  	  if @agent_type == "bacteria"
  	  	  	  	  page.replace_html :sample_listing, :partial => 'investigations/blank'
  	  	  	  	  page.insert_html :bottom, :sample_listing, :partial=> 'investigations/investigation_form_partial', :locals => {:outbreak => @outbreak, :agent_type => @agent_type, :agent => @bacteria, :sample_types => @sample_types, :next_child_index => @next_child_index} 
  	  	  	  elsif @agent_type == "protozoa"
  	  	  	  	  page.replace_html :sample_listing, :partial => 'investigations/blank'
  	  	  	  	  page.insert_html :bottom, :sample_listing, :partial=> 'investigations/investigation_form_partial', :locals => {:outbreak => @outbreak, :agent_type => @agent_type, :agent => @protozoa, :sample_types => @sample_types, :next_child_index => @next_child_index}    
  	  	  	  elsif @agent_type == "viruses"
  	  	  	  	  page.replace_html :sample_listing, :partial => 'investigations/blank'
  	  	  	  	  page.insert_html :bottom, :sample_listing, :partial=> 'investigations/investigation_form_partial', :locals => {:outbreak => @outbreak, :agent_type => @agent_type, :agent => @viruses, :sample_types => @sample_types, :next_child_index => @next_child_index} 
  	  	  	  elsif @agent_type == "toxins"
  	  	  	  	  page.replace_html :sample_listing, :partial => 'investigations/blank'
  	  	  	  	  page.insert_html :bottom, :sample_listing, :partial=> 'investigations/investigation_form_partial', :locals => {:outbreak => @outbreak, :agent_type => @agent_type, :agent => @toxins, :sample_types => @sample_types, :next_child_index => @next_child_index} 
  	  	  	  end
  	  	  
  	  	  
  	  	  elsif params[:confirmed] == "false"
  	  	  	  js_str = "alert( $$([id]='outbreak_bacterial_agents_attributes_0_investigation_attributes_category').up('.investigation').length);"
  	  	  	  
  	  	  	  page << js_str.to_s 
  	  	  	 
  	  	  end
  	  end
  	  
  	 
  end
	
  def remove_factor

  end
  
  def update_reporter
  	render :update do |page|
  		page[:outbreak_reporter_attributes_fname].value = current_user.fname
  		page[:outbreak_reporter_attributes_lname].value = current_user.lname
  		page[:outbreak_reporter_attributes_email].value = current_user.email
  	end  
  end
  
  def download_report
  	  @outbreak = Outbreak.find(params[:id])
  	  
  	  send_file @outbreak.report.path
  end
  
  def get_pathogen
  	   
  	  if params[:pathogen]
  	  	  pathogen = params[:pathogen].strip
  	  	  
  	  elsif params[:aetiology]
  	  	  pathogen = params[:aetiology].strip
  	  end
  	  
  	  #Use the outbreak type from the form to select the sample types for the investigation partial
  	  @outbreak_type = params[:outbreak_type]
  	  field_str = "INVESTIGATIONS:CATEGORY:" << @outbreak_type
  	  @sample_types = Property.find(:all, :conditions => {:field => field_str})
  	  
  	  @next_child_index ? params[:next_child_index] : 0
  	  if params[:id]
  	  	@outbreak = Outbreak.find(params[:id])
  	  else
  	  	@outbreak = Outbreak.new
		@outbreak.risks.build
		@outbreak.incidents.build.build_location
		
		@outbreak.viral_agents.build.build_investigation
		@outbreak.bacterial_agents.build.build_investigation
		@outbreak.toxic_agents.build.build_investigation
		@outbreak.protozoal_agents.build.build_investigation	  
  	  end
  	  
  	    #Initial value for the pathogen dropdowns (Bacterium, Virus, Protozoa, Toxin) if no Agent has been already associated with the Outbreak
	    @initial_bacterium = Bacterium.find_by_name("SUSPECT BACTERIAL")
	    if @initial_bacterium.nil?
		    #Failover if "SUSPECT BACTERIAL" can't be found
		    @initial_bacterium = Bacterium.find(1)    
	    end
	    
	    @initial_virus = Virus.find_by_name("SUSPECT VIRAL")
	    if @initial_virus.nil?
		    #Failover if "SUSPECT VIRAL" can't be found
		    @initial_virus = Virus.find(1)	    
	    end
	    
	    @initial_protozoa = Protozoa.find_by_name("SUSPECT PROTOZOAL")
	    if @initial_protozoa.nil?
		    #Failover if "SUSPECT PROTOZOA" can't be found
		    @initial_protozoa = Protozoa.find(1)	    
	    end
	    
	    @initial_toxin = Toxin.find_by_name("SUSPECT TOXIN")
	    if @initial_toxin.nil?
		    #Failover if "SUSPECT TOXIN" can't be found
		    @initial_toxin = Toxin.find(1)    
	    end
	    
	  
  	  @modifier = params[:modifier] ? params[:modifier].strip : nil
  	  @pathogen_types = Property.find(:all, :conditions => ["field = ? AND property_value != ?", "PATHOGENS:CATEGORY", "MIXED"])

  	 render :update do |page| 
		  case pathogen
		  when "VIRUS"
			  @viruses = Virus.all
			  @viral_agent = ViralAgent.new
			  @viral_agent.category = "CAUSATIVE"
			  if params[:pathogen]
			  	  @next_child_index = params[:next_va_index]
			  	  page.insert_html :bottom, "mixed_pathogen_select", :partial => "viral_agents/viral_agent_form_partial", :locals => { :viruses => @viruses, :viral_agent => @viral_agent, :initial_virus => @initial_virus }
			  	  #page.replace_html "only_pathogen_types_div", :partial => "shards/empty"
			  elsif params[:aetiology]
			  	  page.replace_html "pathogen_select", :partial => "viral_agents/viral_agent_form_partial", :locals => { :viruses => @viruses, :viral_agent => @viral_agent, :initial_virus => @initial_virus }
			  	  page.replace_html "only_pathogen_types_div", :partial => "shards/empty"
			  	  page.replace_html "mixed_pathogen_select"
			  end
		  when "BACTERIUM"
			  @bacteria = Bacterium.all
			  @bacterial_agent = BacterialAgent.new
			  @bacterial_agent.category = "CAUSATIVE"
			  if params[:pathogen]
			  	  @next_child_index = params[:next_ba_index]
			  	  page.insert_html :bottom, "mixed_pathogen_select", :partial => "bacterial_agents/bacterial_agent_form_partial", :locals => { :bacteria => @bacteria, :bacterial_agent => @bacterial_agent, :initial_bacterium => @initial_bacterium }
			  	  #page.replace_html "only_pathogen_types_div", :partial => "shards/empty"
			  elsif params[:aetiology]
			  	  page.replace_html "pathogen_select", :partial => "bacterial_agents/bacterial_agent_form_partial", :locals => { :bacteria => @bacteria, :bacterial_agent => @bacterial_agent, :initial_bacterium => @initial_bacterium }  
			  	  page.replace_html "only_pathogen_types_div", :partial => "shards/empty"
			  	  page.replace_html "mixed_pathogen_select"
			  end
		  when "PROTOZOA"
			  @protozoas = Protozoa.all
			  @protozoal_agent = ProtozoalAgent.new
			  @protozoal_agent.category = "CAUSATIVE"
			  if params[:pathogen]
			  	  @next_child_index = params[:next_pa_index]
			  	  page.insert_html :bottom, "mixed_pathogen_select", :partial => "protozoal_agents/protozoal_agent_form_partial", :locals => { :protozoa => @protozoa, :protozoal_agent => @protozoal_agent, :initial_protozoa => @initial_protozoa }
			  	  #page.replace_html "only_pathogen_types_div", :partial => "shards/empty"
			  elsif params[:aetiology]
			  	  page.replace_html "pathogen_select", :partial => "protozoal_agents/protozoal_agent_form_partial", :locals => { :protozoas => @protozoas, :protozoal_agent => @protozoal_agent, :initial_protozoa => @initial_protozoa }
			  	  page.replace_html "only_pathogen_types_div", :partial => "shards/empty"
			  	  page.replace_html "mixed_pathogen_select"
			  end
			  
		  when "TOXIN"
			  @toxins = Toxin.all
			  @toxic_agent = ToxicAgent.new
			  @toxic_agent.category = "CAUSATIVE"
			  if params[:pathogen]
			  	  @next_child_index = params[:next_ta_index]
			  	  page.insert_html :bottom, "mixed_pathogen_select", :partial => "toxic_agents/toxic_agent_form_partial", :locals => { :toxins => @toxins, :toxic_agent => @toxic_agent, :initial_toxin => @initial_toxin }
			  	  #page.replace_html "only_pathogen_types_div", :partial => "shards/empty"
			  elsif params[:aetiology]
			  	  page.replace_html "pathogen_select", :partial => "toxic_agents/toxic_agent_form_partial", :locals => { :toxins => @toxins, :toxic_agent => @toxic_agent, :initial_toxin => @initial_toxin }
			  	  page.replace_html "only_pathogen_types_div", :partial => "shards/empty"
			  	  page.replace_html "mixed_pathogen_select"
			  end

		  when "MIXED"
			  page.replace_html "only_pathogen_types_div", :partial => "shards/mixed", :locals => {:pathogen_types => @pathogen_types}
			  page.replace_html "pathogen_select", :partial => "shards/empty"
			  page.replace_html "mixed_pathogen_select"
		  when "UNKNOWN"
		  	  page.replace_html "only_pathogen_types_div", :partial => "shards/empty"
			  page.replace_html "pathogen_select", :partial => "shards/empty"
			  page.replace_html "mixed_pathogen_select"
		  else
		  	  page.replace_html "only_pathogen_types_div", :partial => "shards/empty"
			  page.replace_html "pathogen_select", :partial => "shards/empty"
			  page.replace_html "mixed_pathogen_select"
		  end
	  end
  end
  
  # Add a new food vehicle to the Outbreak.
  # Renders foods/food_form_partial.
  def new_food
  	  @food = Food.new
  	  @evidence = Evidence.new
  	  @food.evidence = @evidence
  	  @food_categories = FoodCategory.all
  	  @food_subcategories = FoodSubcategory.all
  	  
  	  @uid = UUIDTools::UUID.random_create
  	  
  	  @evidence_categories = EvidenceCategory.find(:all, :conditions =>{:outbreak_type => "FOODBORNE"})
  	  	final_epi_array = Array.new
		final_epi_array.push("EPIDEMIOLOGICAL")
		final_micro_array = Array.new
		final_micro_array.push("MICROBIOLOGICAL")
		final_empty_array = Array.new
		final_empty_array.push("NONE")
		final_multi_array = Array.new
		final_multi_array.push("MULTIPLE")

		  
		epi_array = Array.new
		micro_array = Array.new
		empty_array = Array.new
		multi_array = Array.new
		
			@evidence_categories.each do |ec|
				
				if ec.category == "EPIDEMIOLOGICAL"
	
					epi_array.push([ec.name, ec.id.to_s])
					
				elsif ec.category == "MICROBIOLOGICAL"
	
					micro_array.push([ec.name, ec.id.to_s])
				elsif ec.category == "NONE"

					empty_array.push([ec.name, ec.id.to_s])
				elsif ec.category == "MULTIPLE"
					
					multi_array.push([ec.name, ec.id.to_s])
				end
			end
			
				final_epi_array.push(epi_array)	

				final_micro_array.push(micro_array)	

				final_empty_array.push(empty_array)
				
				final_multi_array.push(multi_array)
			
				@options_for_categories = Array[final_epi_array, final_micro_array, final_multi_array, final_empty_array]
			

  	  render :update do |page| 
  	  	  page.insert_html :bottom, "food_vehicles", :partial => "foods/food", :locals => { :food_categories => @food_categories, :food_subcategories => @food_subcategories, :food => @food, :options_for_categories => @options_for_categories, :uid => @uid }			  
  	  end	   
  end
  
  # Adds a new control measure to the Outbreak (through Response).
  # Renders measure_form_partial.
  def new_control_measure
  	  @next_child_index = params[:next_child_index]
  	 
  	  if params[:id]
  	  	@outbreak = Outbreak.find(params[:id])
  	  	
  	  	@meaasure = Measure.new
  	  	@outbreak.response.measures.push(@measure)
  	  else
  	  	@outbreak = Outbreak.new
		@outbreak.risks.build
		@outbreak.incidents.build.build_location
		@outbreak.foods.build.build_evidence
		@outbreak.viral_agents.build
		@outbreak.bacterial_agents.build
		@outbreak.toxic_agents.build
		@outbreak.protozoal_agents.build
		#@outbreak.build_response.measure.build
		
		@measure = Measure.new
		@outbreak.build_response
		@outbreak.response.measures.push(@measure)
		
  	  end
  	  
  	  #@response = @outbreak.response
  	  
  	  render :update do |page| 
  	  	  page.insert_html :bottom, "measures_list", :partial => "measure", :locals => { :outbreak => @outbreak, :measure => @measure }			  
  	  end
  	  
  end
  
  # Resets and updates form menus based on :outbreak_type.
  def update_select_menus
  	  @outbreak_type = params[:outbreak_type].strip
  	  #next_child_index will only be used if 
  	  @next_child_index ? params[:next_child_index] : 0
  	  if params[:id]
  	  	@outbreak = Outbreak.find(params[:id])
  	  	
  	  	@response = @outbreak.response
  	  else
  	  	@outbreak = Outbreak.new
		@outbreak.risks.build
		@outbreak.incidents.build.build_location
		@outbreak.incidents.first.build_evidence
		@outbreak.viral_agents.build
		@outbreak.bacterial_agents.build
		@outbreak.toxic_agents.build
		@outbreak.protozoal_agents.build
		@outbreak.foods.build.build_evidence
		@outbreak.build_response.measures.build
		
		@response = @outbreak.response
  	  end
  	  
  	  if @outbreak_type == "FOODBORNE"
  	  	  ob_type_query = "OUTBREAKS:TRANSMISSION_MODE:" << @outbreak_type
		  @transmission_modes = Property.find(:all, :conditions => {:field => ob_type_query})
			 
		  ob_type_query = "INVESTIGATIONS:CATEGORY:" << @outbreak_type
		  
		  @sample_types = Property.find(:all, :conditions => {:field => ob_type_query})
		  @categories = Category.find(:all, :conditions => { :outbreak_type => "FOODBORNE"})
		  @subcategories = Subcategory.find(:all, :conditions => { :category_id => @categories.first.id})
		  @subtypes = Subtype.find(:all, :conditions => { :subcategory_id => @subcategories.first.id})
		  

  	  elsif @outbreak_type == "NON-FOODBORNE"
  	  	  ob_type_query = "OUTBREAKS:TRANSMISSION_MODE:" << @outbreak_type
		  @transmission_modes = Property.find(:all, :conditions => {:field => ob_type_query})
		 
		  ob_type_query = "INVESTIGATIONS:CATEGORY:" << @outbreak_type
		  
		  @sample_types = Property.find(:all, :conditions => {:field => ob_type_query})
		  @categories = Category.find(:all, :conditions => { :outbreak_type => "NON-FOODBORNE"})
		  @subcategories = Subcategory.find(:all, :conditions => { :category_id => @categories.first.id})
		  @subtypes = Subtype.find(:all, :conditions => { :subcategory_id => @subcategories.first.id})
  	 end
  	 
  	 @pathogen_types = Property.find(:all, :conditions => {:field => "PATHOGENS:CATEGORY"})
  	 @outbreak_types = Property.find(:all, :conditions => {:field => "OUTBREAKS:OUTBREAK_TYPE"} )
  	 @factors = Factor.find(:all, :conditions => {:outbreak_type => @outbreak.outbreak_type})
  	 @hpus = Hpu.find(:all, :order => "name ASC")
         @regions = Region.find(:all, :order => "name ASC")
         
         @cuisines = Cuisine.all
  	 @viruses = Virus.all
  	 
  	 @factors = Factor.find(:all, :conditions => {:outbreak_type => @outbreak_type.to_s})

  	 #evidence dropdowns
	 @evidence_categories = EvidenceCategory.find(:all, :conditions =>{:outbreak_type => @outbreak_type.to_s})
	 	final_epi_array = Array.new
		final_epi_array.push("EPIDEMIOLOGICAL")
		final_micro_array = Array.new
		final_micro_array.push("MICROBIOLOGICAL")
		final_empty_array = Array.new
		final_empty_array.push("NONE")
		final_multi_array = Array.new
		final_multi_array.push("MULTIPLE")

		  
		epi_array = Array.new
		micro_array = Array.new
		empty_array = Array.new
		multi_array = Array.new
		
			@evidence_categories.each do |ec|
				
				if ec.category == "EPIDEMIOLOGICAL"
	
					epi_array.push([ec.name, ec.id.to_s])
					
				elsif ec.category == "MICROBIOLOGICAL"
	
					micro_array.push([ec.name, ec.id.to_s])
				elsif ec.category == "NONE"

					empty_array.push([ec.name, ec.id.to_s])
				elsif ec.category == "MULTIPLE"
					
					multi_array.push([ec.name, ec.id.to_s])
				end
			end
			
				final_epi_array.push(epi_array)	

				final_micro_array.push(micro_array)	

				final_empty_array.push(empty_array)
				
				final_multi_array.push(multi_array)
			
				@options_for_categories = Array[final_epi_array, final_micro_array, final_multi_array, final_empty_array]
			

			   
	 render :update do |page|
	 	 page.replace 'outbreak_transmission_div', :partial => 'transmission_mode_select_update'
	 	 page.replace 'incident_category_select', :partial => 'incident_category_select_update'
	 	 page.replace 'incident_subcategory_select', :partial => 'incident_subcategory_select_update'
	 	 page.replace 'incident_subtype_select', :partial => 'incident_subtype_select_update'
	 	 page.replace 'risk_select', :partial => 'risk_select_update'
	 	 
	 	  if @outbreak_type == "FOODBORNE"
	 	  	  page.replace_html 'tabs_f_link', 'Foods'
	 	  	  page.replace 'food_vehicle_div', :partial => 'food_vehicle_list'
	 	  	  page.replace 'cuisine_div', :partial => 'cuisine_select_update'
	 	  	  page.replace 'food_premise_rating', :partial => 'food_premise_rating'
	 	  	  page.replace 'food_hygiene_rating', :partial => 'food_hygiene_rating'
	 	  	  page.replace_html 'incident_evidence_div', :partial => 'evidences/incident_evidence_update_empty' 
	 	 	 	 
	 	  elsif @outbreak_type == "NON-FOODBORNE"
	 	  	  page.replace_html 'tabs_f_link', 'Origins'
	 	  	  page.replace_html 'food_vehicle_div', ''
	 	  	  page.replace 'cuisine_div', :partial => 'cuisine_empty'
		  
	 	  	  page.replace 'food_premise_rating', :partial => 'food_premise_rating_empty', :locals => { :response => @response}
	 	  	  page.replace 'food_hygiene_rating', :partial => 'food_hygiene_rating_empty', :locals => { :response => @response}
	 	  	  page.replace_html 'incident_evidence_div', :partial => 'evidences/incident_evidence_update', :locals => { :options_for_categories => @options_for_categories, :this_child_index => 0}  
			  	
	 	  end
	 end	  
  	
  end
  
  
  def update_transmission_mode
  	  @ob_type = params[:outbreak_type].strip
  	  
  	  
  	  if @ob_type
		  ob_type_query = "OUTBREAKS:TRANSMISSION_MODE:" << @ob_type
		  @transmission_modes = Property.find(:all, :conditions => {:field => ob_type_query})
		 
		  ob_type_query = "INVESTIGATIONS:CATEGORY:" << @ob_type
		  @sample_types = Property.find(:all, :conditions => {:field => ob_type_query})
		  
		  if @ob_type == "FOODBORNE"
		  	  @cuisines = Cuisine.all  
		  	  @factors = Factor.find(:all, :conditions => {:outbreak_type => "FOODBORNE"})
		  	  @categories = Category.find(:all, :conditions => { :outbreak_type => "FOODBORNE"})
		  	  @error_check = 0
			  render :update do |page|
			  	
			  	  begin
			  	  	  page.replace_html 'outbreak_transmission_div', :partial => 'transmission_mode_select', :locals => { :transmission_modes => @transmission_modes}
			  	  rescue
			  	  	  page.insert_html :bottom, 'ajax_error', '<p>Error :: transmission modes update select</p>'
			  	  	  page.show 'ajax_error'
			  	  	  @error_check += 1
			  	  end
			  	  
			  	  begin
			  	  	  page.replace_html 'incident_category_select', :partial => 'category_select', :locals => { :categories => @categories }  
			  	  rescue
			  	  	  page.insert_html :bottom, 'ajax_error', '<p>Error :: incident category update select</p>'
			  	  	  page.show 'ajax_error'
			  	  	  @error_check += 1
			  	  end
			  	  
			  	  begin
			  	  	  page.replace_html 'investigation_category_div', :partial => 'investigation_select', :object => @sample_types
			  	  rescue
			  	  	  page.insert_html  :bottom, 'ajax_error', '<p>Error :: investigation category update select</p>'
			  	  	  page.show 'ajax_error'
			  	  	  @error_check += 1
			  	  end
			  	  begin
			  	  	  page.replace_html 'cuisine_div', :partial => 'cuisine_select', :object => @cuisines
			  	  rescue
			  	  	  page.insert_html :bottom, 'ajax_error', '<p>Error :: cuisine partial</p>'
			  	  	  page.show 'ajax_error'
			  	  	  @error_check += 1
			  	  end
			  	  begin
			  	  	  page.replace_html 'factor_div', :partial => 'factor_select', :object => @factors
			  	  rescue
			  	  	  page.insert_html :bottom, 'ajax_error', '<p>Error :: factor update select</p>'
			  	  	  page.show 'ajax_error'
			  	  	  @error_check += 1
			  	  end
			  	  begin
			  	  	  page.replace_html 'investigation_outbreak_type_div', :partial => 'investigation_outbreak_type_hidden', :object => @ob_type
			  	  rescue
			  	  	  page.show 'ajax_error'
			  	  	  page.insert_html :bottom, 'ajax_error', '<p>Error :: investigation outbreak type hidden field</p>'

			  	  	  @error_check += 1
			  	  end
			  	  if @error_check == 0
			  	  	  #error_check is 0 so empty the ajax_error div
					  begin
					  	  page.hide 'ajax_error' 
					  	  page.replace_html 'ajax_error', ''
					  	  
					  rescue
					  end
				  end
			  end
		  else
		  	@factors = Factor.find(:all, :conditions => {:outbreak_type => "NON-FOODBORNE"})
		  	@categories = Category.find(:all, :conditions => { :outbreak_type => "NON-FOODBORNE"})
  	
		  	@error_check = 0
		  	  render :update do |page|
		  	  	 
		  	  
		  	  	  begin
		  	  	  	  page.replace_html 'outbreak_transmission_div', :partial => 'transmission_mode_select', :locals => { :transmission_modes => @transmission_modes}
		  	  	  rescue
		  	  	  	  page.replace_html 'ajax_error', '<p>Error :: transmission modes update select</p>'
		  	  	  	  page.show 'ajax_error'
			  	  	  @error_check += 1
		  	  	  end
		  	  	  
		  	  	  begin
			  	  	  page.replace_html 'incident_category_select', :partial => 'category_select', :locals => { :categories => @categories }  
			  	  rescue
			  	  	  page.insert_html :bottom, 'ajax_error', '<p>Error :: incident category update select</p>'
			  	  	  page.show 'ajax_error'
			  	  	  @error_check += 1
			  	  end
		  	  	  
		  	  	  begin
		  	  	  	  page.replace_html 'investigation_category_div', :partial => 'investigation_select', :object => @sample_types
		  	  	  rescue
		  	  	  	  page.replace_html 'ajax_error', '<p>Error :: investigation category update select</p>'
		  	  	  	  page.show 'ajax_error'
			  	  	  @error_check += 1
		  	  	  end
		  	  	  begin
		  	  	  	  page.replace_html 'cuisine_div', :partial => 'cuisine_empty'
		  	  	  rescue
		  	  	  	  page.replace_html 'ajax_error', '<p>Error :: cuisine empty partial</p>'
		  	  	  	  page.show 'ajax_error'
			  	  	  @error_check += 1
		  	  	  end
		  	  	  begin
		  	  	  	  page.replace_html 'factor_div', :partial => 'factor_select', :object => @factors
		  	  	  rescue
		  	  	  	  page.replace_html 'ajax_error', '<p>Error :: factor update select</p>'
		  	  	  	  page.show 'ajax_error'
			  	  	  @error_check += 1
		  	  	  end
		  	  	  begin
		  	  	  	  page.replace_html 'investigation_outbreak_type_div', :partial => 'investigation_outbreak_type_hidden', :object => @ob_type
		  	  	  rescue
		  	  	  	  page.replace_html 'ajax_error', '<p>Error :: investigation outbreak type hidden field</p>'
		  	  	  	  page.show 'ajax_error'
			  	  	  @error_check += 1
		  	  	  end
		  	  	  if @error_check == 0
			  	  	 
					  begin
					  	  page.hide 'ajax_error' 
					  	  page.replace_html 'ajax_error', ''  
					  rescue
					  end
				  end
			  end  
		  end
	  end
  end
  
  def update_subcategory 
  	  this_category_id = params[:category_id]
  	  @next_child_index ? params[:next_child_index] : 0
  	  if params[:id]
  	  	@outbreak = Outbreak.find(params[:id])
  	  else
  	  	@outbreak = Outbreak.new
		@outbreak.risks.build
		@outbreak.incidents.build.build_location
		@outbreak.incidents.first.build_evidence
		@outbreak.viral_agents.build
		@outbreak.bacterial_agents.build
		@outbreak.toxic_agents.build
		@outbreak.protozoal_agents.build	  
  	  end
	  
  	  category = Category.find(this_category_id)
  	  @subcategories = category.subcategories
  	  @subtypes = category.subtypes
  	 
  	  render :update do |page|
  	  	  page.replace_html 'incident_subcategory_select', :partial => 'incident_subcategory_select_update'
  	  	  page.replace_html 'incident_subtype_select',   :partial => 'incident_subtype_select_update'
	  end
  	  
  end
  
  def update_subtype 
  	  this_subcategory_id = params[:subcategory_id]
  	  @next_child_index ? params[:next_child_index] : 0
  	  if params[:id]
  	  	@outbreak = Outbreak.find(params[:id])
  	  else
  	  	@outbreak = Outbreak.new
		@outbreak.risks.build
		@outbreak.incidents.build.build_location
		@outbreak.incidents.first.build_evidence
		@outbreak.viral_agents.build
		@outbreak.bacterial_agents.build
		@outbreak.toxic_agents.build
		@outbreak.protozoal_agents.build	  
  	  end
  	  
  	  subcategory = Subcategory.find(this_subcategory_id)
  	  
  	  @subtypes = subcategory.subtypes
  	  
  	  render :update do |page|
  	  	  page.replace_html 'incident_subtype_select',   :partial => 'incident_subtype_select_update'
	  end
  	  
  end
  
  def chart
  	  
  	  
  	  #find earliest onset date
  	  #@earliest_outbreak = Outbreak.first(:order => :onset_first)
  	  
  	  #start_date = @earliest_outbreak.onset_first.at_beginning_of_year
  	 
  	  #hardcoded start date
  	  start_date = Date.parse("01/01/1992")
  	  
  	  @start_year = start_date.year
	  @start_month = start_date.month
	  @start_day = start_date.day
  	  end_date = Date.today.at_end_of_year
  	  @dates = Hash.new
  	  until start_date > end_date
  	  	  end_of_month = start_date.at_end_of_month
  	  	  monthly_collection = Outbreak.find(:all, :conditions => ["(onset_first BETWEEN ? AND ?)", start_date, end_of_month]) 	  	  
  	  	  
  	  	  if @dates.key?(start_date.year)
  	  	  	  existing_month_hash = @dates[start_date.year]
  	  	  	  existing_month_hash[start_date.month] = monthly_collection
  	  	  	  @dates[start_date.year] = existing_month_hash
  	  	  else
  	  	  	  month_hash = Hash.new
  	  	  	  month_hash[start_date.month] = monthly_collection
  	  	  	  @dates[start_date.year] = month_hash
  	  	  end
  	  	  
  	  	  start_date = start_date + 1.month
  	  end
  	  @category_series = ""
  	  @outbreak_series = ""
  	  @ordered_series = Hash.new
  	  @years = Array.new
  	  #raise @dates.inspect
  	  @dates.each_pair do |this_year, this_date|
  	  	  temp_data = "name: #{this_year},"
  	  	  temp_data += " data: ["
  	  	  
  	  	  temp_array = Array.new
  	  	  #raise this_date.inspect
  	  	  this_date.each_pair do |this_month, outbreaks|
  	  	 	 temp_array.push(outbreaks.length)
  	 	  end
  	 	  
  	 	 temp_data << temp_array.join(",")
  	 	 temp_data << "]"
  	 	 
  	 	 @ordered_series[this_year] = temp_data
  	 	 @years.push(this_year)
	  end
	  @years.sort! { |a,b| a <=> b}
  	  
  end

  # GET /outbreaks
  # GET /outbreaks.xml
  def index
  	 
  	  #Collect the data for populating the search partial select menus etc.
  	  @hpus = Hpu.find(:all, :order => :name).collect{|c| [c.name, c.name]}.insert(0,["", ""])
  	  @regions = Region.find(:all, :order => :name).collect{|c| [c.name, c.name]}.insert(0,["", ""])
  	  @local_authorities = LocalAuthority.find(:all, :order => :name).collect{|c| [c.name, c.name]}.insert(0,["", ""])
  	  
  	  
  	  @categories = Category.find(:all, :order => "outbreak_type ASC").collect{|c| [c.name, c.name]}.insert(0,["", ""])
  	  @subcategories = Subcategory.find(:all, :order => "category_id ASC").collect{|c| [c.name, c.name]}.insert(0,["", ""])
  	  @subtypes = Subtype.find(:all, :order => "subcategory_id ASC").collect{|c| [c.name, c.name]}.insert(0,["", ""])
  	  
  	 
  	  @options_for_subcategories = Array.new
  	  Category.all.each do |this_category|
  	  	  final_array = Array.new
  	  	  final_array.push(this_category.name)
  	  	  @subcategories = Subcategory.find(:all, :conditions => {:category_id => this_category.id})
  	  	  subcategory_array = Array.new
  	  	  subcategory_array.push(['',''])
  	  	  @subcategories.each do |this_subcategory|
  	  	  	  subcategory_array.push([this_subcategory.name, this_subcategory.name])
  	  	  end
  	  	  final_array.push(subcategory_array)
  	  	  @options_for_subcategories.push(final_array)
  	  end
  	  
  	  @options_for_subtypes = Array.new
  	  Subcategory.all.each do |this_subcategory|
  	  	  final_array = Array.new
  	  	  final_array.push(this_subcategory.name)
  	  	  @subtypes = Subtype.find(:all, :conditions => {:subcategory_id => this_subcategory.id})
  	  	  subtype_array = Array.new
  	  	  subtype_array.push(['',''])
  	  	  @subtypes.each do |this_subtype|
  	  	  	  subtype_array.push([this_subtype.name, this_subtype.name])  
  	  	  end
  	  	  final_array.push(subtype_array)
  	  	  @options_for_subtypes.push(final_array)
  	  end
  	  
  	  @options_for_hpus = Array.new
  	  Region.all.each do |this_region|
  	  	  final_array = Array.new
  	  	  final_array.push(this_region.name)
  	  	  @hpus = Hpu.find(:all, :conditions => {:region_id => this_region.id})
  	  	  hpu_array = Array.new
  	  	  hpu_array.push(['',''])
  	  	  @hpus.each do |this_hpu|
  	  	  	  hpu_array.push([this_hpu.name, this_hpu.name])  
  	  	  end
  	  	  final_array.push(hpu_array)
  	  	  @options_for_hpus.push(final_array)
  	  end
  	  
  	  @options_for_authorities = Array.new
  	  Hpu.all.each do |this_hpu|
  	  	  final_array = Array.new
  	  	  final_array.push(this_hpu.name)
  	  	  @authorities = LocalAuthority.find(:all, :conditions => {:hpu_id => this_hpu.id})
  	  	  la_array = Array.new
  	  	  la_array.push(['',''])
  	  	  @authorities.each do |this_authority|
  	  	  	  la_array.push([this_authority.name, this_authority.name])  
  	  	  end
  	  	  final_array.push(la_array)
  	  	  @options_for_authorities.push(final_array)
  	  end
  	  
  	  @options_for_food_subcategories = Array.new
  	  FoodCategory.all.each do |this_food|
  	  	  final_array = Array.new
  	  	  final_array.push(this_food.name)
  	  	  @food_subcategories = FoodSubcategory.find(:all, :conditions => {:food_category_id => this_food.id})
  	  	  subcategory_array = Array.new
  	  	  subcategory_array.push(['',''])
  	  	  @food_subcategories.each do |food_subcategory|
  	  	  	  subcategory_array.push([food_subcategory.name, food_subcategory.name])  
  	  	  end
  	  	  final_array.push(subcategory_array)
  	  	  @options_for_food_subcategories.push(final_array)
  	  end	 

  	  
  	  @viruses = Virus.all.collect{|c| [c.name, c.name]}.insert(0,["", ""])  	    
  	  @bacteria = Bacterium.all.collect{|c| [c.name, c.name]}.insert(0,["", ""])
  	  @protozoas = Protozoa.all.collect{|c| [c.name, c.name]}.insert(0,["", ""])    
  	  @toxins = Toxin.all.collect{|c| [c.name, c.name]}.insert(0,["", ""])
  	  @outbreak_types = Property.find(:all, :conditions => {:field => "OUTBREAKS:OUTBREAK_TYPE"} ).collect{|c| [c.property_value, c.property_value]}.insert(0,["", ""])
  	  @transmission_modes = Property.find(:all, :conditions => ["field ILIKE ? OR field ILIKE ?", "OUTBREAKS:TRANSMISSION_MODE:FOODBORNE", "OUTBREAKS:TRANSMISSION_MODE:NON-FOODBORNE"]).collect{|c| [c.property_value, c.property_value]}.insert(0, ["",""])
  	  @pathogens = Property.find(:all, :conditions => {:field => "PATHOGENS:CATEGORY"}).collect{|c| [c.property_value, c.property_value]}.insert(0,["", ""])
  	  @earliest_date_year = Property.find_by_field("EARLIEST_DATE:YEAR").property_value.to_i ? Property.find_by_field("EARLIEST_DATE:YEAR").property_value.to_i : 1992
  	  
  	  @food_categories = FoodCategory.all.collect{|c| [c.name, c.name]}.insert(0,["", ""])
  	  @food_subcategories = FoodSubcategory.all.collect{|c| [c.name, c.name]}.insert(0,["", ""])
  	
  	  @factors = Factor.all
	
		final_foodborne_array = Array.new
		final_foodborne_array.push("FOODBORNE")
		final_nonfoodborne_array = Array.new
		final_nonfoodborne_array.push("NON-FOODBORNE")
		foodborne_array = Array.new
		nonfoodborne_array = Array.new
		foodborne_array.push(['',''])
		nonfoodborne_array.push(['',''])
  	  @factors.each do |this_factor|
  	  	  if this_factor.outbreak_type == "FOODBORNE"
  	  	  	  foodborne_array.push([this_factor.name, this_factor.id.to_s])  
  	  	  elsif this_factor.outbreak_type == "NON-FOODBORNE"
  	  	  	  nonfoodborne_array.push([this_factor.name, this_factor.id.to_s])
  	  	  end
  	  end
  	  final_foodborne_array.push(foodborne_array)
  	  final_nonfoodborne_array.push(nonfoodborne_array)
  	  @options_for_factors = Array[final_foodborne_array, final_nonfoodborne_array]
  	  
  	  #Parse the returned onset and point source dates into a suitable format for the database
  	  if params[:onset_first_year] != nil && !params[:onset_first_year].empty?
  	  	  onset_first_start = Date.new(params[:onset_first_year].to_i, 1, 1)
  	  	  onset_first_end = Date.new(params[:onset_first_year].to_i, 12, 31)
  	  	  params[:search][:onset_first_year] = [onset_first_start.strftime("%Y-%m-%d"), onset_first_end.strftime("%Y-%m-%d")]
  	  	  
  	  end
  	  if params[:onset_last_year] != nil && !params[:onset_last_year].empty?
  	  	  onset_last_start = Date.new(params[:onset_last_year].to_i, 1, 1)
  	  	  onset_last_end = Date.new(params[:onset_last_year].to_i, 12, 31)
  	  	  params[:search][:onset_last_year] = [onset_last_start.strftime("%Y-%m-%d"), onset_last_end.strftime("%Y-%m-%d")]
  	  	  
  	  end
  	  if params[:point_source_date_year] != nil && !params[:point_source_date_year].empty?
  	  	  point_source_date_start = Date.new(params[:point_source_date_year].to_i, 1, 1)
  	  	  point_source_date_end = Date.new(params[:point_source_date_year].to_i, 12, 31)
  	  	  params[:search][:point_source_year] = [point_source_date_start.strftime("%Y-%m-%d"), point_source_date_end.strftime("%Y-%m-%d")]
  	  	  
  	  end
  	  
  	  #get the current whitelist from the Property table
  	  begin
		  whitelist_properties = Property.find(:all, :conditions => {:field => "WHITELIST::SEARCHES"} )
		  whitelist_array = whitelist_properties.collect {|p| p.property_value}
		  whitelist_array.collect! {|wh| wh.split(',')}
		  whitelist_array[0].collect! {|wh| wh.strip!}
		  whitelist = whitelist_array[0].compact
	  rescue
	  	  #If the whitelist can't be found pass an empty array
	  	  #No search params will be accepted
	  	  whitelist = Array.new  
	  end


  	  if params[:search]
  	  	  #set the default index order to be descending Outbreak id
  	  	  if !params[:search][:order]
  	  	  	  params[:search][:order] = "descend_by_id"  
  	  	  end
  	  	  if params[:search][:bacterial_agents_bacterium_name_like_any] != nil && !params[:search][:bacterial_agents_bacterium_name_like_any].empty?
  	  	  	  params[:search][:bacterial_agents_category_like] = "CAUSATIVE"
  	  	  end
  	  	  if params[:search][:viral_agents_virus_name_like_any] != nil && !params[:search][:viral_agents_virus_name_like_any].empty?
  	  	  	  params[:search][:viral_agents_category_like] = "CAUSATIVE"
  	  	  end
  	  	  if params[:search][:protozoal_agents_protozoa_name_like_any] != nil && !params[:search][:protozoal_agents_protozoa_name_like_any].empty?
  	  	  	  params[:search][:protozoal_agents_category_like] = "CAUSATIVE"
  	  	  end
  	  	  if params[:search][:toxic_agents_toxin_name_like_any] != nil && !params[:search][:toxic_agents_toxin_name_like_any].empty?
  	  	  	  params[:search][:toxic_agents_category_like] = "CAUSATIVE"
  	  	  end
  	  	  if params[:search][:incidents_location_encrypted_postcode_like] != nil && !params[:search][:incidents_location_encrypted_postcode_like].empty?
			  tmp_location = Location.new
			  tmp_location.postcode = params[:search][:incidents_location_encrypted_postcode_like]
			  tmp_location.encrypted_postcode
			  params[:search][:incidents_location_encrypted_postcode_like] = tmp_location.encrypted_postcode
		  end
		  
		  #Location search
		  #Encrypted fields will have to be unencrypted and checked
		  if params[:location_search] != nil && !params[:location_search].empty?
		  	  upcased_search = params[:location_search].upcase
		  	  if params[:location_search_type] == true
		  	  	locations = Location.all
		  	  	locations.each do |this_location|
		  	  		match_flag = false
		  	  		match_flag == false ? this_location.placename.upcase.include?(upcased_search) : true
		  	  		match_flag == false ? this_location.address_1.upcase.include?(upcased_search) : true
		  	  		match_flag == false ? this_location.address_2.upcase.include?(upcased_search) : true
		  	  		match_flag == false ? this_location.address_3.upcase.include?(upcased_search) : true
		  	  		match_flag == false ? this_location.town.upcase.include?(upcased_search) : true
		  	  		match_flag == false ? this_location.postcode.upcase.include?(upcased_search) : true
		  	  		begin
		  	  			match_flag == false ? this_location.hpu.upcase.include?(upcased_search) : true
		  	  		rescue		
		  	  		end
		  	  		begin
		  	  			match_flag == false ? this_location.local_authority.upcase.include?(upcased_search) : true
		  	  		rescue	
		  	  		end
		  	  		begin
		  	  			match_flag == false ? this_location.region.upcase.include?(upcased_search) : true
		  	  		rescue
		  	  		end
		  		end
		  	elsif params[:location_search_type] == true
		  		
		  		
		  	end
		  end
		  
		  #pass the pass parameters to the whietlist to check that each search param is included
		  #if not remove that param from the params
		  params[:search].each do |this_search|
				  
			if !whitelist.include?(this_search[0].upcase) 
				params[:search].delete(this_search[0].to_sym)
			end
		 end

	 else
	 	 #set the default order if the params::search is empty 
	 	 params[:search] = {:order => "descend_by_id"}	 
	 	 
	 end
	 
	
	  
	  #pass the search params to a variable so it can be echoed in the search summary
	  @search_params = params[:search]
  	  #call the search scopes on the Outbreak
  	  @search = Outbreak.search(params[:search])
	 
  	  if current_user.role?('admin')
  	  	  #currently return all outbreaks regardless of status
  	  	  #can be changed to only return all outbreaks for admin users, while submitters only see active status outbreaks
  	  	  @outbreaks = @search.find(:all, :select => "DISTINCT outbreaks.*").paginate(:page => params[:page], :per_page => '20')
  	  else	  
  	  	  @outbreaks = @search.find(:all, :select => "DISTINCT outbreaks.*").paginate(:page => params[:page], :per_page => '20')
  	  end

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @outbreaks }
    end
  end

  # GET /outbreaks/1
  # GET /outbreaks/1.xml
  def show
  	  
    @map = Mapstraction.new("map_div_small", :microsoft)
    @map.control_init(:small_map => true, :map_type => true)
    
  
    @outbreak = Outbreak.find(params[:id])
    @locations = @outbreak.locations
    @location = @locations.first
    @bacterial_agents = @outbreak.bacterial_agents
    @viral_agents = @outbreak.viral_agents
    @protozoal_agents = @outbreak.protozoal_agents
    @toxic_agents = @outbreak.toxic_agents
    if @location.lat && @location.lon
    	    
    	    window_info = "<fieldset class=\"map_results\"><legend class=\"legend_title\">Outbreak " + @outbreak.id.to_s + "</legend> HPU :: " + @location.hpu.name
    	    window_info << "</fieldset>"
    	   
    	    @map.center_zoom_init([@location.lat.to_f, @location.lon.to_f],12)
    	    @map.marker_init(Marker.new([@location.lat.to_f, @location.lon.to_f], :title => "Outbreak", :info_bubble => window_info, :icon => "../images/dred_marker.png"))
    else
    	    @map.center_zoom_init([53,-0,5],4)
    	    
    end
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @outbreak }
    end
  end

  # GET /outbreaks/new
  # GET /outbreaks/new.xml
  def new
    #build an instance of the outbreak and associated models, used to construct the nested forms from partials
    @outbreak = Outbreak.new
    @outbreak.risks.build
    @outbreak.incidents.build.build_location
    @outbreak.incidents.first.build_evidence
    @outbreak.build_reporter
    
    #call build on associated objects - creates a blank instance of the object to be used
    #in building the form
    @outbreak.viral_agents.build.build_investigation
    @outbreak.bacterial_agents.build.build_investigation
    @outbreak.toxic_agents.build.build_investigation
    @outbreak.protozoal_agents.build.build_investigation
    @outbreak.build_response.measures.build
    
    #populate the dropdowns
    @outbreak_types = Property.find(:all, :conditions => {:field => "OUTBREAKS:OUTBREAK_TYPE"} )
    @ob = @outbreak_types.first
    field_str = "OUTBREAKS:TRANSMISSION_MODE:" << @ob.property_value
    @transmission_modes = Property.find(:all, :conditions => {:field => field_str} )
    @origins = Origin.all
    @pathogen_types = Property.find(:all, :conditions => {:field => "PATHOGENS:CATEGORY"} )
     
    field_str = "INVESTIGATIONS:CATEGORY:" << @ob.property_value
    @sample_types = Property.find(:all, :conditions => {:field => field_str})
    
    #location dropdowns
    @hpus = Hpu.find(:all, :order => :id)
    @regions = Region.find(:all, :order => :name)
    @local_authorities = LocalAuthority.find(:all, :order => :name)
    @categories = Category.find(:all, :conditions => {:outbreak_type => @ob.property_value}, :order => "outbreak_type ASC")
    @subcategories = Subcategory.find(:all, :order => "category_id ASC")
    @subtypes = Subtype.find(:all, :order => "subcategory_id ASC")
    @cuisines = Cuisine.all
    
    #evidence dropdowns :: NON-FOODBORNE
    @evidence_categories = EvidenceCategory.find(:all, :conditions =>{:outbreak_type => "NON-FOODBORNE"})
    
	 	final_epi_array = Array.new
		final_epi_array.push("EPIDEMIOLOGICAL")
		final_micro_array = Array.new
		final_micro_array.push("MICROBIOLOGICAL")
		final_empty_array = Array.new
		final_empty_array.push("NONE")
		final_multi_array = Array.new
		final_multi_array.push("MULTIPLE")
  
		epi_array = Array.new
		micro_array = Array.new
		empty_array = Array.new
		multi_array = Array.new
		
			@evidence_categories.each do |ec|
				
				if ec.category == "EPIDEMIOLOGICAL"
	
					epi_array.push([ec.name, ec.id.to_s])
					
				elsif ec.category == "MICROBIOLOGICAL"
	
					micro_array.push([ec.name, ec.id.to_s])
				elsif ec.category == "NONE"

					empty_array.push([ec.name, ec.id.to_s])
				elsif ec.category == "MULTIPLE"
					
					multi_array.push([ec.name, ec.id.to_s])
				end
			end
			
				final_epi_array.push(epi_array)	

				final_micro_array.push(micro_array)	

				final_empty_array.push(empty_array)
				
				final_multi_array.push(multi_array)
			
				@options_for_categories = Array[final_epi_array, final_micro_array, final_multi_array, final_empty_array]
    
    #pathogen dropdowns
    @viruses = Virus.all
    
      #Initial value for the pathogen dropdowns (Bacterium, Virus, Protozoa, Toxin) if no Agent has been already associated with the Outbreak
	    @initial_bacterium = Bacterium.find_by_name("SUSPECT BACTERIAL")
	    if @initial_bacterium.nil?
		    #Failover if "SUSPECT BACTERIAL" can't be found
		    @initial_bacterium = Bacterium.find(1)    
	    end
	    
	    @initial_virus = Virus.find_by_name("SUSPECT VIRAL")
	    if @initial_virus.nil?
		    #Failover if "SUSPECT VIRAL" can't be found
		    @initial_virus = Virus.find(1)	    
	    end
	    
	    @initial_protozoa = Protozoa.find_by_name("SUSPECT PROTOZOAL")
	    if @initial_protozoa.nil?
		    #Failover if "SUSPECT PROTOZOA" can't be found
		    @initial_protozoa = Protozoa.find(1)	    
	    end
	    
	    @initial_toxin = Toxin.find_by_name("SUSPECT TOXIN")
	    if @initial_toxin.nil?
		    #Failover if "SUSPECT TOXIN" can't be found
		    @initial_toxin = Toxin.find(1)    
	    end
	    
    @earliest_date_year = Property.find_by_field("EARLIEST_DATE:YEAR").property_value.to_i ? Property.find_by_field("EARLIEST_DATE:YEAR").property_value.to_i : 1992
    @latest_date_year = Date.today.year.to_i
	   
    #factors dropdowns
    @factors = Factor.find(:all, :conditions => {:outbreak_type => "FOODBORNE"})
    
    #foods dropdown
    @food_categories = FoodCategory.all
    @food_subcategories = FoodSubcategory.find(:all, :conditions => {:food_category_id => 1} )
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @outbreak }
    end
  end

  # GET /outbreaks/1/edit
  def edit
    #@cgi = CGI.new

    @outbreak = Outbreak.find(params[:id])
    @risks = @outbreak.risks
    @location = @outbreak.locations.first
    @incident = @outbreak.incidents.first
    @response = @outbreak.response ? @outbreak.response : @outbreak.build_response
    @origins = Origin.all
    @viruses = Virus.all  	    
    @bacteria = Bacterium.all
    @protozoas = Protozoa.all    
    @toxins = Toxin.all
    @outbreak_types = Property.find(:all, :conditions => {:field => "OUTBREAKS:OUTBREAK_TYPE"} )
    
    #Initial value for the pathogen dropdowns (Bacterium, Virus, Protozoa, Toxin) if no Agent has been already associated with the Outbreak
    @initial_bacterium = Bacterium.find_by_name("SUSPECT BACTERIAL")
    if @initial_bacterium == nil
    	    #Failover if "SUSPECT BACTERIAL" can't be found
    	    @initial_bacterium = Bacterium.find(1)    
    end
    
    @initial_virus = Virus.find_by_name("SUSPECT VIRAL")
    if @initial_virus == nil
    	    #Failover if "SUSPECT VIRAL" can't be found
    	    @initial_virus = Virus.find(1)	    
    end
    
    @initial_protozoa = Protozoa.find_by_name("SUSPECT PROTOZOAL")
    if @initial_protozoa == nil
    	    #Failover if "SUSPECT PROTOZOA" can't be found
    	    @initial_protozoa = Protozoa.find(1)	    
    end
    
    @initial_toxin = Toxin.find_by_name("SUSPECT TOXIN")
    if @initial_toxin == nil
    	    #Failover if "SUSPECT TOXIN" can't be found
    	    @initial_toxin = Toxin.find(1)    
    end
    
    if @outbreak.bacterial_agents.length > 0
    	    
    	    @ba_search = @outbreak.bacterial_agents.search(:category => "CAUSATIVE")
    	    @bacterial_agents = @ba_search.all
    end
    if @outbreak.viral_agents.length > 0
    	    @va_search = @outbreak.viral_agents.search(:category => "CAUSATIVE")
    	    @viral_agents = @va_search.all
    end
    if @outbreak.protozoal_agents.length > 0
    	    @pa_search = @outbreak.protozoal_agents.search(:category => "CAUSATIVE")
    	    @protozoal_agents = @pa_search.all
    end

    if @outbreak.toxic_agents.length > 0
    	    @ta_search = @outbreak.toxic_agents.search(:category => "CAUSATIVE")
    	    @toxic_agents = @ta_search.all
    end


    ob = @outbreak.outbreak_type
    field_str = "OUTBREAKS:TRANSMISSION_MODE:" << ob
    
    @transmission_modes = Property.find(:all, :conditions => {:field => field_str} ) 
  
    @pathogen_types = Property.find(:all, :conditions => {:field => "PATHOGENS:CATEGORY"})
    
    @factors = Factor.find(:all, :conditions => {:outbreak_type => @outbreak.outbreak_type})
    
    field_str = "INVESTIGATIONS:CATEGORY:" << ob
    @sample_types = Property.find(:all, :conditions => {:field => field_str})
    @earliest_date_year = Property.find_by_field("EARLIEST_DATE:YEAR").property_value.to_i ? Property.find_by_field("EARLIEST_DATE:YEAR").property_value.to_i : 1992
    @latest_date_year = Date.today.year.to_i
    #location dropdowns
    @hpus = Hpu.find(:all, :order => "name ASC")
    @regions = Region.find(:all, :order => "name ASC")
    @local_authorities = LocalAuthority.find(:all, :order => :name)
    @categories = Category.find(:all, :conditions => {:outbreak_type => @outbreak.outbreak_type})


    #evidence dropdowns
	 @evidence_categories = EvidenceCategory.find(:all, :conditions =>{:outbreak_type => @outbreak.outbreak_type})
	 	final_epi_array = Array.new
		final_epi_array.push("EPIDEMIOLOGICAL")
		final_micro_array = Array.new
		final_micro_array.push("MICROBIOLOGICAL")
		final_empty_array = Array.new
		final_empty_array.push("NONE")
		final_multi_array = Array.new
		final_multi_array.push("MULTIPLE")

		  
		epi_array = Array.new
		micro_array = Array.new
		empty_array = Array.new
		multi_array = Array.new
		
			@evidence_categories.each do |ec|
				
				if ec.category == "EPIDEMIOLOGICAL"
	
					epi_array.push([ec.name, ec.id.to_s])
					
				elsif ec.category == "MICROBIOLOGICAL"
	
					micro_array.push([ec.name, ec.id.to_s])
				elsif ec.category == "NONE"

					empty_array.push([ec.name, ec.id.to_s])
				elsif ec.category == "MULTIPLE"
					
					multi_array.push([ec.name, ec.id.to_s])
				end
			end
			
				final_epi_array.push(epi_array)	

				final_micro_array.push(micro_array)	

				final_empty_array.push(empty_array)
				
				final_multi_array.push(multi_array)
			
				@options_for_categories = Array[final_epi_array, final_micro_array, final_multi_array, final_empty_array]

    
    @subcategories = Subcategory.find(:all, :conditions => {:category_id => @incident.category_id})
	    if @subcategories == nil
		    @subcategories = Subcategory.all    
	    end
    @subtypes = Subtype.find(:all, :conditions => {:subcategory_id => @incident.subcategory_id})
	    if @subtypes == nil
	    	    @subtypes = Subtype.all 
	    end
    @food_categories = FoodCategory.all
    @food_subcategories = FoodSubcategory.all
    @cuisines = Cuisine.all
    
    if params[:outbreak_type] == 'FOODBORNE'
  	  ob_type_query = "INVESTIGATIONS:CATEGORY:FOODBORNE" 
  	  @sample_types = Property.find(:all, :conditions => {:field => ob_type_query})
    elsif params[:outbreak_type] == 'NON-FOODBORNE'
  	  ob_type_query = "INVESTIGATIONS:CATEGORY:NON-FOODBORNE"
  	  @sample_types = Property.find(:all, :conditions => {:field => ob_type_query})
    end 
   
    
  end

  # POST /outbreaks
  # POST /outbreaks.xml
  def create
  	
    @outbreak = Outbreak.new(params[:outbreak])
    @user = User.find(current_user.id)
    @outbreak.user = @user
    @outbreak_types = Property.find(:all, :conditions => {:field => "OUTBREAKS:OUTBREAK_TYPE"} )
    @ob = @outbreak_types.first
    field_str = "OUTBREAKS:TRANSMISSION_MODE:" << @ob.property_value
    @transmission_modes = Property.find(:all, :conditions => {:field => field_str} )
    @origins = Origin.all
    @pathogen_types = Property.find(:all, :conditions => {:field => "PATHOGENS:CATEGORY"} )
     
    field_str = "INVESTIGATIONS:CATEGORY:" << @ob.property_value
    @sample_types = Property.find(:all, :conditions => {:field => field_str})
    
    #location dropdowns
    @hpus = Hpu.find(:all, :order => :name)
    @regions = Region.find(:all, :order => :name)
    @local_authorities = LocalAuthority.find(:all, :order => :name)
    @categories = Category.find(:all, :conditions => {:outbreak_type => @ob.property_value}, :order => "outbreak_type ASC")
    @subcategories = Subcategory.find(:all, :order => "category_id ASC")
    @subtypes = Subtype.find(:all, :order => "subcategory_id ASC")
    @cuisines = Cuisine.all
    
    @earliest_date_year = Property.find_by_field("EARLIEST_DATE:YEAR").property_value.to_i ? Property.find_by_field("EARLIEST_DATE:YEAR").property_value.to_i : 1992
    @latest_date_year = Date.today.year.to_i
    #evidence dropdowns :: NON-FOODBORNE
   
   @evidence_categories = EvidenceCategory.find(:all, :conditions =>{:outbreak_type => "NON-FOODBORNE"})
	 	final_epi_array = Array.new
		final_epi_array.push("EPIDEMIOLOGICAL")
		final_micro_array = Array.new
		final_micro_array.push("MICROBIOLOGICAL")
		final_empty_array = Array.new
		final_empty_array.push("NONE")
		final_multi_array = Array.new
		final_multi_array.push("MULTIPLE")

		  
		epi_array = Array.new
		micro_array = Array.new
		empty_array = Array.new
		multi_array = Array.new
		
			@evidence_categories.each do |ec|
				
				if ec.category == "EPIDEMIOLOGICAL"
	
					epi_array.push([ec.name, ec.id.to_s])
					
				elsif ec.category == "MICROBIOLOGICAL"
	
					micro_array.push([ec.name, ec.id.to_s])
				elsif ec.category == "NONE"

					empty_array.push([ec.name, ec.id.to_s])
				elsif ec.category == "MULTIPLE"
					
					multi_array.push([ec.name, ec.id.to_s])
				end
			end
			
				final_epi_array.push(epi_array)	

				final_micro_array.push(micro_array)	

				final_empty_array.push(empty_array)
				
				final_multi_array.push(multi_array)
			
				@options_for_categories = Array[final_epi_array, final_micro_array, final_multi_array, final_empty_array]

    
    #pathogen dropdowns
    @viruses = Virus.all
   
    #factors dropdowns
    @factors = Factor.find(:all, :conditions => {:outbreak_type => "FOODBORNE"})
    
    #foods dropdown
    @food_categories = FoodCategory.all
    @food_subcategories = FoodSubcategory.find(:all, :conditions => {:food_category_id => 1} )
 
    respond_to do |format|
     if @outbreak.save
        flash[:notice] = 'Outbreak was successfully created.'
        format.html { redirect_to(@outbreak) }
        format.xml  { render :xml => @outbreak, :status => :created, :location => @outbreak }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @outbreak.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /outbreaks/1
  # PUT /outbreaks/1.xml
  def update

    @outbreak = Outbreak.find(params[:id]) 
    
    @risks = @outbreak.risks
    @location = @outbreak.locations.first
    @incident = @outbreak.incidents.first
    @origins = Origin.all
    @viruses = Virus.all  	    
    @bacteria = Bacterium.all
    @protozoas = Protozoa.all    
    @toxins = Toxin.all
    @outbreak_types = Property.find(:all, :conditions => {:field => "OUTBREAKS:OUTBREAK_TYPE"} )
    
    if @outbreak.bacterial_agents.length > 0
    	    @bacterial_agent = @outbreak.bacterial_agents.first
    end
    if @outbreak.viral_agents.length > 0
    	    @viral_agent = @outbreak.viral_agents.first
    end
    if @outbreak.protozoal_agents.length > 0
    	    @protozoal_agent = @outbreak.protozoal_agents.first
    end

    if @outbreak.toxic_agents.length > 0
    	    @toxic_agent = @outbreak.toxic_agents.first
    end


    ob = @outbreak.outbreak_type
    field_str = "OUTBREAKS:TRANSMISSION_MODE:" << ob
    
    @transmission_modes = Property.find(:all, :conditions => {:field => field_str} ) 
  
    @pathogen_types = Property.find(:all, :conditions => {:field => "PATHOGENS:CATEGORY"})
    
    @factors = Factor.find(:all, :conditions => {:outbreak_type => @outbreak.outbreak_type})
    
    field_str = "INVESTIGATIONS:CATEGORY:" << ob
    @sample_types = Property.find(:all, :conditions => {:field => field_str})
    
    #location dropdowns
    @hpus = Hpu.find(:all, :order => "name ASC")
    @regions = Region.find(:all, :order => "name ASC")
    @local_authorities = LocalAuthority.find(:all, :order => :name)
    @categories = Category.find(:all, :conditions => {:outbreak_type => @outbreak.outbreak_type})

    #evidence dropdowns
    @evidence_categories = EvidenceCategory.find(:all, :conditions =>{:outbreak_type => @outbreak.outbreak_type})
   	 	final_epi_array = Array.new
		final_epi_array.push("EPIDEMIOLOGICAL")
		final_micro_array = Array.new
		final_micro_array.push("MICROBIOLOGICAL")
		final_empty_array = Array.new
		final_empty_array.push("NONE")
		final_multi_array = Array.new
		final_multi_array.push("MULTIPLE")

		  
		epi_array = Array.new
		micro_array = Array.new
		empty_array = Array.new
		multi_array = Array.new
		
			@evidence_categories.each do |ec|
				
				if ec.category == "EPIDEMIOLOGICAL"
	
					epi_array.push([ec.name, ec.id.to_s])
					
				elsif ec.category == "MICROBIOLOGICAL"
	
					micro_array.push([ec.name, ec.id.to_s])
				elsif ec.category == "NONE"

					empty_array.push([ec.name, ec.id.to_s])
				elsif ec.category == "MULTIPLE"
					
					multi_array.push([ec.name, ec.id.to_s])
				end
			end
			
				final_epi_array.push(epi_array)	

				final_micro_array.push(micro_array)	

				final_empty_array.push(empty_array)
				
				final_multi_array.push(multi_array)
			
				@options_for_categories = Array[final_epi_array, final_micro_array, final_multi_array, final_empty_array]

    
    @subcategories = Subcategory.find(:all, :conditions => {:category_id => @incident.category_id})
	    if @subcategories == nil
		    @subcategories = Subcategory.all    
	    end
    @subtypes = Subtype.find(:all, :conditions => {:subcategory_id => @incident.subcategory_id})
	    if @subtypes == nil
	    	    @subtypes = Subtype.all 
	    end
    @food_categories = FoodCategory.all
    @food_subcategories = FoodSubcategory.all
    @cuisines = Cuisine.all
    
    @earliest_date_year = Property.find_by_field("EARLIEST_DATE:YEAR").property_value.to_i ? Property.find_by_field("EARLIEST_DATE:YEAR").property_value.to_i : 1992
    @latest_date_year = Date.today.year.to_i
   
    
    if params[:outbreak_type] == 'FOODBORNE'
  	  ob_type_query = "INVESTIGATIONS:CATEGORY:FOODBORNE" 
  	  @sample_types = Property.find(:all, :conditions => {:field => ob_type_query})
    elsif params[:outbreak_type] == 'NON-FOODBORNE'
  	  ob_type_query = "INVESTIGATIONS:CATEGORY:NON-FOODBORNE"
  	  @sample_types = Property.find(:all, :conditions => {:field => ob_type_query})
    end 
    
    if @outbreak.current_state != "draft"
	    #set state machine to draft since the record has been edited
	    @outbreak.draft
    end
    
    respond_to do |format|
      if @outbreak.update_attributes(params[:outbreak])
      	
        flash[:notice] = 'Outbreak was successfully updated.'
        format.html { redirect_to(@outbreak) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @outbreak.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /outbreaks/1
  # DELETE /outbreaks/1.xml
  def destroy
  	  if current_user.role?('admin')
  	  	  #only allow users with admin access to delete outbreaks
		    @outbreak = Outbreak.find(params[:id])
		    @incidents = @outbreak.incidents
		    @locations = @outbreak.locations
		    @risks = @outbreak.risks
		    if @outbreak.bacterial_agents.length > 0
			    @outbreak.bacterial_agents.each do |this_agent|
				    agent = BacterialAgent.find(this_agent.id)
				    agent.destroy
			    end
		    end
		    if @outbreak.viral_agents.length > 0
			    @outbreak.viral_agents.each do |this_agent|
				    agent = ViralAgent.find(this_agent.id)
				    agent.destroy
			    end
		    end
		    if @outbreak.protozoal_agents.length > 0
			   @outbreak.protozoal_agents.each do |this_agent|
				    agent = ProtozoalAgent.find(this_agent.id)
				    agent.destroy
			    end 
		    end
		    if @outbreak.toxic_agents.length > 0
			    @outbreak.toxic_agents.each do |this_agent|
				    agent = ToxicAgent.find(this_agent.id)
				    agent.destroy
			    end
		    end
		    if @locations.length > 0
			    @locations.each do |this_location|
				    location = Location.find(this_location.id)
				    location.destroy
			    end
		    end
		    if @incidents.length > 0
			    @incidents.each do |this_incident|
				    incident = Incident.find(this_incident.id)
				    incident.destroy
			    end
		    end
		
		    
		
		    @outbreak.destroy
		
		    respond_to do |format|
		      format.html { redirect_to(outbreaks_url) }
		      format.xml  { head :ok }
		    end
	   end
  end
  
  
end
