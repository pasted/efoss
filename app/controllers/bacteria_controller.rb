class BacteriaController < ApplicationController
  # GET /bacteria
  # GET /bacteria.xml
  def index
    @bacteria = Bacterium.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @bacteria }
    end
  end

  # GET /bacteria/1
  # GET /bacteria/1.xml
  def show
    @bacterium = Bacterium.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @bacterium }
    end
  end

  # GET /bacteria/new
  # GET /bacteria/new.xml
  def new
    @bacterium = Bacterium.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @bacterium }
    end
  end

  # GET /bacteria/1/edit
  def edit
    @bacterium = Bacterium.find(params[:id])
  end

  # POST /bacteria
  # POST /bacteria.xml
  def create
    @bacterium = Bacterium.new(params[:bacterium])

    respond_to do |format|
      if @bacterium.save
        flash[:notice] = 'Bacterium was successfully created.'
        format.html { redirect_to(@bacterium) }
        format.xml  { render :xml => @bacterium, :status => :created, :location => @bacterium }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @bacterium.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /bacteria/1
  # PUT /bacteria/1.xml
  def update
    @bacterium = Bacterium.find(params[:id])

    respond_to do |format|
      if @bacterium.update_attributes(params[:bacterium])
        flash[:notice] = 'Bacterium was successfully updated.'
        format.html { redirect_to(@bacterium) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @bacterium.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /bacteria/1
  # DELETE /bacteria/1.xml
  def destroy
    @bacterium = Bacterium.find(params[:id])
    @bacterium.destroy

    respond_to do |format|
      format.html { redirect_to(bacteria_url) }
      format.xml  { head :ok }
    end
  end
  

  
end
