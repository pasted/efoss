class FoodSubcategoriesController < ApplicationController
  # GET /food_subcategories
  # GET /food_subcategories.xml
  def index
    @food_subcategories = FoodSubcategory.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @food_subcategories }
    end
  end

  # GET /food_subcategories/1
  # GET /food_subcategories/1.xml
  def show
    @food_subcategory = FoodSubcategory.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @food_subcategory }
    end
  end

  # GET /food_subcategories/new
  # GET /food_subcategories/new.xml
  def new
    @food_subcategory = FoodSubcategory.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @food_subcategory }
    end
  end

  # GET /food_subcategories/1/edit
  def edit
    @food_subcategory = FoodSubcategory.find(params[:id])
  end

  # POST /food_subcategories
  # POST /food_subcategories.xml
  def create
    @food_subcategory = FoodSubcategory.new(params[:food_subcategory])

    respond_to do |format|
      if @food_subcategory.save
        flash[:notice] = 'FoodSubcategory was successfully created.'
        format.html { redirect_to(@food_subcategory) }
        format.xml  { render :xml => @food_subcategory, :status => :created, :location => @food_subcategory }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @food_subcategory.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /food_subcategories/1
  # PUT /food_subcategories/1.xml
  def update
    @food_subcategory = FoodSubcategory.find(params[:id])

    respond_to do |format|
      if @food_subcategory.update_attributes(params[:food_subcategory])
        flash[:notice] = 'FoodSubcategory was successfully updated.'
        format.html { redirect_to(@food_subcategory) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @food_subcategory.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /food_subcategories/1
  # DELETE /food_subcategories/1.xml
  def destroy
    @food_subcategory = FoodSubcategory.find(params[:id])
    @food_subcategory.destroy

    respond_to do |format|
      format.html { redirect_to(food_subcategories_url) }
      format.xml  { head :ok }
    end
  end
end
