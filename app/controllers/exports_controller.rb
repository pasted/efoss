class ExportsController < ApplicationController
require 'fastercsv'
require 'generic_agent'
require 'generic_record'


	def listing
		if params[:with] == 'all'
			@outbreaks = Outbreak.search("order_by_id").all
			
		elsif params[:with]
			
			@outbreaks = Outbreak.search(params[:with])
		else
			@outbreaks = Outbreak.search("order_by_id").all
			
		end
		
		@records = Array.new
		
		
		#hard-coded path for the html folder on F0
		@this_filepath = "/home/f0/html/efoss/" << Time.now.strftime("%I:%M:%S_%d:%m:%y")  << ".csv"
		
		@spawn_id = spawn(:nice => 1) do
			#error_log = File.open("/home/f0/html/efoss/error_log.txt", 'w')
			#@csv_file = File.open(@this_filepath, 'w').flock(LOCK_EX)
			FasterCSV.open(@this_filepath, "w") do |csv|
			#csv_string = FasterCSV.generate do |csv|
							csv << [ "outbreak_id", "reporter_fname", "reporter_lname", "reporter_email", "outbreak_current_state",  "outbreak_onset_first", "outbreak_onset_last", "outbreak_user_reference", "outbreak_gezi_reference",
						"outbreak_total_affected", "outbreak_at_risk", "outbreak_lab_confirmed", "outbreak_admitted", "outbreak_died", "outbreak_aetiology", "outbreak_transmission_mode", "outbreak_outbreak_type", "outbreak_comment",
						"outbreak_point_source", "outbreak_point_source_date", "outbreak_point_source_detail", "incident_category", "incident_subcategory", "incident_subtype", "incident_cuisine", "incident_detail", "incident_evidence_category", "incident_evidence_name",
						"location_placename", "location_address_1", "location_address_2", "location_address_3", "location_town",
						"location_region", "location_hpu", "location_local_authority", "location_postcode", "location_lon", "location_lat",
						"agent_1_name", "agent_1_serotype", "agent_1_phagetype", "agent_1_other_typing", "agent_1_concentration", "agent_1_confirmed", "agent_1_comment",
						"agent_2_name", "agent_2_serotype", "agent_2_phagetype", "agent_2_other_typing", "agent_2_concentration", "agent_2_confirmed", "agent_2_comment",
						"agent_3_name", "agent_3_serotype", "agent_3_phagetype", "agent_3_other_typing", "agent_3_concentration", "agent_3_confirmed", "agent_3_comment",
						"risk_1_factor_name", "risk_1_detail", "risk_2_factor_name", "risk_2_detail", "risk_3_factor_name", "risk_3_detail",
						"food_1_category", "food_1_subcategory", "food_1_description", "food_1_contains_rse", "food_1_country", "food_1_evidence_category", "food_1_evidence_name",
						"food_2_category", "food_2_subcategory", "food_2_description", "food_2_contains_rse", "food_2_country", "food_2_evidence_category", "food_2_evidence_name",
						"food_3_category", "food_3_subcategory", "food_3_description", "food_3_contains_rse", "food_3_country", "food_3_evidence_category", "food_3_evidence_name",
						"response_premise_status", "response_prosecution_status", "response_notice_served", "response_inspection_rating", "response_comment",
						"measure_1_description", "measure_2_description", "measure_3_description"
							]
				
				begin
							@outbreaks.each do |ob|
									this_record = GenericRecord.new
									this_record.outbreak_id = ob.id ? ob.id : ''
									this_record.outbreak_current_state = ob.current_state ? ob.current_state : ''
									this_record.outbreak_onset_first = ob.onset_first ? ob.onset_first : ''
									this_record.outbreak_onset_last = ob.onset_last ? ob.onset_last : ''
									this_record.outbreak_user_reference = ob.user_reference ? ob.user_reference : ''
									this_record.outbreak_gezi_reference = ob.gezi_reference ? ob.gezi_reference : ''
									this_record.outbreak_transmission_mode = ob.transmission_mode ? ob.transmission_mode : ''
									this_record.outbreak_total_affected = ob.total_affected ? ob.total_affected : ''
									this_record.outbreak_at_risk = ob.at_risk ? ob.at_risk : ''
									this_record.outbreak_lab_confirmed = ob.lab_confirmed ? ob.lab_confirmed : ''
									this_record.outbreak_admitted = ob.admitted ? ob.admitted : ''
									this_record.outbreak_died = ob.died ? ob.died : ''
									this_record.outbreak_aetiology = ob.aetiology ? ob.aetiology : ''
									this_record.outbreak_outbreak_type = ob.outbreak_type ? ob.outbreak_type : ''
									this_record.outbreak_comment = ob.comment ? ob.comment : ''
									this_record.outbreak_point_source = ob.point_source ? ob.point_source : ''
									this_record.outbreak_point_source_date = ob.point_source_date ? ob.point_source_date : ''
									this_record.outbreak_point_source_detail = ob.point_source_detail ? ob.point_source_detail : ''
									
									this_record.reporter_fname = ob.reporter ? ob.reporter.fname : ''
									this_record.reporter_lname = ob.reporter ? ob.reporter.lname : ''
									this_record.reporter_email = ob.reporter ? ob.reporter.email : ''
									
									begin
										if ob.incidents.first.category
											this_record.incident_category = ob.incidents.first.category ? ob.incidents.first.category.name : ''
										else
											this_record.incident_category = ''	
										end
										if ob.incidents.first.subcategory
											this_record.incident_subcategory = ob.incidents.first.subcategory ? ob.incidents.first.subcategory.name : ''
										else
											this_record.incident_subcategory = ''	
										end
										if ob.incidents.first.subtype
											this_record.incident_subtype = ob.incidents.first.subtype ? ob.incidents.first.subtype.name : ''
										else
											this_record.incident_subtype = ''	
										end
										if ob.incidents.first.cuisine
											this_record.incident_cuisine = ob.incidents.first.cuisine ? ob.incidents.first.cuisine.name : ''
										else
											this_record.incident_cuisine = ''	
										end
										if ob.incidents.first.evidence != nil
											this_record.incident_evidence_category = ob.incidents.first.evidence ? ob.incidents.first.evidence.evidence_category.category : ''
											this_record.incident_evidence_name = ob.incidents.first.evidence ? ob.incidents.first.evidence.evidence_category.name : ''
										else
											this_record.incident_evidence_category = ''
											this_record.incident_evidence_name = ''
										end
									rescue Exception => e
												#logger.error "ERROR INCIDENT :: outbreak #{ob.id}"
												#logger.error e.inspect
												
												#error_log.puts "ERROR INCIDENT :: outbreak #{ob.id}"
												#error_log.puts e.inspect
									end
									 
									begin
										this_record.location_placename = ob.incidents.first.location ? ob.incidents.first.location.placename : ''
										this_record.location_address_1 = ob.incidents.first.location ? ob.incidents.first.location.address_1 : ''
										this_record.location_address_2 = ob.incidents.first.location ? ob.incidents.first.location.address_2 : ''
										this_record.location_address_3 = ob.incidents.first.location ? ob.incidents.first.location.address_3 : ''
										this_record.location_town = ob.incidents.first.location ? ob.incidents.first.location.town : ''
										if ob.incidents.first.location.region != nil
											this_record.location_region = ob.incidents.first.location.region ? ob.incidents.first.location.region.name : ''
										else
											this_record.location_region = ''	
										end
										if ob.incidents.first.location.hpu != nil
											this_record.location_hpu = ob.incidents.first.location.hpu ? ob.incidents.first.location.hpu.name : ''
										else
											this_record.location_hpu = ''	
										end
										if ob.incidents.first.location.local_authority != nil
											this_record.location_local_authority = ob.incidents.first.location.local_authority ? ob.incidents.first.location.local_authority.name : ''
										else
											this_record.location_local_authority = ''	
										end
										this_record.location_postcode = ob.incidents.first.location ? ob.incidents.first.location.postcode : ''
										this_record.location_lon = ob.incidents.first.location ? ob.incidents.first.location.lon : ''
										this_record.location_lat = ob.incidents.first.location ? ob.incidents.first.location.lat : ''
										
									rescue Exception => e
										#logger.error "ERROR LOCATION :: outbreak #{ob.id}"
										#logger.error e.inspect
										
										#error_log.puts "ERROR LOCATION :: outbreak #{ob.id}"
										#error_log.puts e.inspect
									end
									
									begin
										
										if ob.aetiology == "BACTERIUM"
											 
											ba_agent = ob.bacterial_agents.find_by_category("CAUSATIVE") ? ob.bacterial_agents.find_by_category("CAUSATIVE") : ob.bacterial_agents.first
												
											this_record.agent_1_name = ba_agent.bacterium ? ba_agent.bacterium.name : ''
											this_record.agent_1_serotype = ba_agent.serotype ? ba_agent.serotype : ''
											this_record.agent_1_phagetype = ba_agent.phagetype ? ba_agent.phagetype : ''
											this_record.agent_1_other_typing = ba_agent.other_typing ? ba_agent.other_typing : ''
											this_record.agent_1_concentration = ''
											this_record.agent_1_confirmed = ba_agent.confirmed ? ba_agent.confirmed : ''
											this_record.agent_1_comment = ba_agent.comment ? ba_agent.comment : ''
										elsif ob.aetiology == "VIRUS"
											vi_agent = ob.viral_agents.find_by_category("CAUSATIVE") ? ob.viral_agents.find_by_category("CAUSATIVE") : ob.viral_agents.first
			
											this_record.agent_1_name = vi_agent.virus ? vi_agent.virus.name : ''
											this_record.agent_1_serotype = ''
											this_record.agent_1_phagetype = ''
											this_record.agent_1_other_typing = vi_agent.other_typing ? vi_agent.other_typing : ''
											this_record.agent_1_concentration = ''
											this_record.agent_1_confirmed = vi_agent.confirmed ? vi_agent.confirmed : ''
											this_record.agent_1_comment = vi_agent.comment ? vi_agent.comment : ''
						
												
										elsif ob.aetiology == "PROTOZOA"
											pr_agent = ob.protozoal_agents.find_by_category("CAUSATIVE") ? ob.protozoal_agents.find_by_category("CAUSATIVE") : ob.protozoal_agents.first
			
											this_record.agent_1_name = pr_agent.protozoa ? pr_agent.protozoa.name : ''
											this_record.agent_1_serotype = ''
											this_record.agent_1_phagetype = ''
											this_record.agent_1_other_typing = pr_agent.other_typing ? pr_agent.other_typing : ''
											this_record.agent_1_concentration = ''
											this_record.agent_1_confirmed = pr_agent.confirmed ? pr_agent.confirmed : ''
											this_record.agent_1_comment = pr_agent.comment ? pr_agent.comment : ''
											
										elsif ob.aetiology == "TOXIN"
											to_agent = ob.toxic_agents.find_by_category("CAUSATIVE") ? ob.toxic_agents.find_by_category("CAUSATIVE") : ob.toxic_agents.first
											
											this_record.agent_1_name = to_agent.toxin ? to_agent.toxin.name : ''
											this_record.agent_1_serotype = ''
											this_record.agent_1_phagetype = ''
											this_record.agent_1_other_typing = ''
											this_record.agent_1_concentration = to_agent.concentration ? to_agent.concentration : ''
											this_record.agent_1_confirmed = to_agent.confirmed ? to_agent.confirmed : ''
											this_record.agent_1_comment = to_agent.comment ? to_agent.comment : ''
						
										end
										
										if ob.bacterial_agents.length > 0
												ob.bacterial_agents.each_with_index do |ba, i|
													
													if i == 0
															
													else
														if i == 1
															this_record.agent_2_name = ba.bacterium ? ba.bacterium.name : ''
															this_record.agent_2_serotype = ba.serotype ? ba.serotype : ''
															this_record.agent_2_phagetype = ba.phagetype ? ba.phagetype : ''
															this_record.agent_2_other_typing = ba.other_typing ? ba.other_typing : ''
															this_record.agent_2_concentration = ''
															this_record.agent_2_confirmed = ba.confirmed ? ba.confirmed : ''
															this_record.agent_2_comment = ba.comment ? ba.comment : ''
														elsif i == 2
															this_record.agent_3_name = ba.bacterium ? ba.bacterium.name : ''
															this_record.agent_3_serotype = ba.serotype ? ba.serotype : ''
															this_record.agent_3_phagetype = ba.phagetype ? ba.phagetype : ''
															this_record.agent_3_other_typing = ba.other_typing ? ba.other_typing : ''
															this_record.agent_3_concentration = ''
															this_record.agent_3_confirmed = ba.confirmed ? ba.confirmed : ''
															this_record.agent_3_comment = ba.comment ? ba.comment : ''
														end
													end
												end
										end
										
										if ob.viral_agents.length > 0
												ob.viral_agents.each_with_index do |va, i|
													
													if i == 0
															
													else
														if i == 1
															this_record.agent_2_name = va.virus ? va.virus.name : ''
															this_record.agent_2_serotype = ''
															this_record.agent_2_phagetype = ''
															this_record.agent_2_other_typing = va.other_typing ? va.other_typing : ''
															this_record.agent_2_concentration = ''
															this_record.agent_2_confirmed = va.confirmed ? va.confirmed : ''
															this_record.agent_2_comment = va.comment ? va.comment : ''
														elsif i == 2
															this_record.agent_3_name = va.virus ? va.virus.name : ''
															this_record.agent_3_serotype = ''
															this_record.agent_3_phagetype = ''
															this_record.agent_3_other_typing = va.other_typing ? va.other_typing : ''
															this_record.agent_3_concentration = ''
															this_record.agent_3_confirmed = va.confirmed ? va.confirmed : ''
															this_record.agent_3_comment = va.comment ? va.comment : ''
														end
													end
												end
										end
										
										if ob.protozoal_agents.length > 0 
											ob.protozoal_agents.each_with_index do |pr, i|
														if i == 0
															
													else
														if i == 1
															this_record.agent_2_name = pr.protozoa ? pr.protozoa.name : ''
															this_record.agent_2_serotype = ''
															this_record.agent_2_phagetype = ''
															this_record.agent_2_other_typing = pr.other_typing ? pr.other_typing : ''
															this_record.agent_2_concentration = ''
															this_record.agent_2_confirmed = pr.confirmed ? pr.confirmed : ''
															this_record.agent_2_comment = pr.comment ? pr.comment : ''
														elsif i == 2
															this_record.agent_3_name = pr.protozoa ? pr.protozoa.name : ''
															this_record.agent_3_serotype = ''
															this_record.agent_3_phagetype = ''
															this_record.agent_3_other_typing = pr.other_typing ? pr.other_typing : ''
															this_record.agent_3_concentration = ''
															this_record.agent_3_confirmed = pr.confirmed ? pr.confirmed : ''
															this_record.agent_3_comment = pr.comment ? pr.comment : ''
														end
													end
											end	
										end
										
										if ob.toxic_agents.length > 0
											ob.toxic_agents.each_with_index do |to, i|
														if i == 0
															
													else
														if i == 1
															this_record.agent_2_name = to.toxin ? to.toxin.name : ''
															this_record.agent_2_serotype = ''
															this_record.agent_2_phagetype = ''
															this_record.agent_2_other_typing = ''
															this_record.agent_2_concentration = to.concentration ? to.concentration : ''
															this_record.agent_2_confirmed = to.confirmed ? to.confirmed : ''
															this_record.agent_2_comment = to.comment ? to.comment : ''
														elsif i == 2
															this_record.agent_3_name = to.toxin ? to.toxin.name : ''
															this_record.agent_3_serotype = ''
															this_record.agent_3_phagetype = ''
															this_record.agent_3_other_typing = ''
															this_record.agent_3_concentration = to.concentration ? to.concentration : ''
															this_record.agent_3_confirmed = to.confirmed ? to.confirmed : ''
															this_record.agent_3_comment = to.comment ? to.comment : ''
														end
													end
											end	
						
										end		
									rescue Exception => e
										#logger.error "ERROR AGENT :: outbreak #{ob.id}"
										#logger.error e.inspect
										
										#error_log.puts "ERROR AGENT :: outbreak #{ob.id}"
										#error_log.puts e.inspect
									end
									
									begin
										 if ob.risks.length > 0
											 ob.risks.each_with_index do |r, i|
												 if i == 0
													 this_record.risk_1_factor_name = r.factor ? r.factor.name : ''
													 this_record_risk_1_detail = r.detail ? r.detail : ''
												 elsif i == 1
													 this_record.risk_2_factor_name = r.factor ? r.factor.name : ''
													 this_record_risk_2_detail = r.detail ? r.detail : ''
												 elsif i == 2
													 this_record.risk_3_factor_name = r.factor ? r.factor.name : ''
													 this_record.risk_3_detail = r.detail ? r.detail : ''	
												 end
											 end	
										 end
									rescue Exception => e
										#logger.error "ERROR RISK :: outbreak #{ob.id}"
										#logger.error e.inspect
										
										#error_log.puts "ERROR RISK :: outbreak #{ob.id}"
										#error_log.puts  e.inspect
									end
									
									begin
										 if ob.foods.length > 0
											 ob.foods.each_with_index do |f, i|
												 if i == 0
													 if f.food_category != nil
														 this_record.food_1_category = f.food_category ? f.food_category.name : ''
													 else
														 this_record.food_1_category = ''	
													 end
													 if f.food_subcategory != nil
														 this_record.food_1_subcategory = f.food_subcategory ? f.food_subcategory.name : ''
													 else
														 this_record.food_1_subcategory = ''	
													 end
													 this_record.food_1_description = f.description ? f.description : ''
													 this_record.food_1_contains_rse = f.contains_rse ? f.contains_rse : ''
													 this_record.food_1_country = f.country ? f.country : ''
													 if f.evidence != nil
														 this_record.food_1_evidence_category = f.evidence ? f.evidence.evidence_category.category : ''
														 this_record.food_1_evidence_name = f.evidence ? f.evidence.evidence_category.name : ''
													 else
														 this_record.food_1_evidence_category = ''
														 this_record.food_1_evidence_name = ''
													 end
													 
													 
												 elsif i == 1
													 if f.food_category != nil
														 this_record.food_2_category = f.food_category ? f.food_category.name : ''
													 else
														 this_record.food_2_category = ''	
													 end
													 if f.food_subcategory != nil
														 this_record.food_2_subcategory = f.food_subcategory ? f.food_subcategory.name : ''
													 else
														 this_record.food_2_subcategory = ''	
													 end
													 
													 this_record.food_2_description = f.description ? f.description : ''
													 this_record.food_2_contains_rse = f.contains_rse ? f.contains_rse : ''
													 this_record.food_2_country = f.country ? f.country : ''
													 if f.evidence != nil
														 this_record.food_2_evidence_category = f.evidence ? f.evidence.evidence_category.category : ''
														 this_record.food_2_evidence_name = f.evidence ? f.evidence.evidence_category.name : ''
													 else
														 this_record.food_2_evidence_category = ''
														 this_record.food_2_evidence_name = ''
													 end
													 
												 elsif i == 2
													 if f.food_category != nil
														 this_record.food_3_category = f.food_category ? f.food_category.name : ''
													 else
														 this_record.food_3_category = ''	
													 end
													 if f.food_subcategory != nil
														 this_record.food_3_subcategory = f.food_subcategory ? f.food_subcategory.name : ''
													 else
														 this_record.food_3_subcategory = ''	
													 end
													 this_record.food_3_description = f.description ? f.description : ''
													 this_record.food_3_contains_rse = f.contains_rse ? f.contains_rse : ''
													 this_record.food_3_country = f.country ? f.country : ''
													 if f.evidence != nil
														 this_record.food_3_evidence_category = f.evidence ? f.evidence.evidence_category.category : ''
														 this_record.food_3_evidence_name = f.evidence ? f.evidence.evidence_category.name : ''
													 else
														 this_record.food_3_evidence_category = ''
														 this_record.food_3_evidence_name = ''
													 end
												 end
											 end
										 end
							
									rescue Exception => e
										#logger.error "ERROR FOOD : outbreak #{ob.id}"
										#logger.error e.inspect
										
										#error_log.puts "ERROR FOOD : outbreak #{ob.id}"
										#error_log.puts e.inspect
									end
									
									begin
										 if ob.response != nil
											 this_record.response_premise_status = ob.response ? ob.response.premise_status : ''
											 this_record.response_prosecution_status = ob.response ? ob.response.prosecution_status : ''
											 this_record.response_notice_served = ob.response ? ob.response.notice_served : ''
											 this_record.response_inspection_rating = ob.response ? ob.response.inspection_rating : ''
											 this_record.response_comment = ob.response ? ob.response.comment : ''
										 else
											 this_record.response_premise_status = ''
											 this_record.response_prosecution_status = ''
											 this_record.response_notice_served = ''
											 this_record.response_inspection_rating = ''
											 this_record.response_comment = ''
										 end
										 
									rescue Exception => e
										#logger.error "ERROR RESPONSE : outbreak #{ob.id}"
										#logger.error e.inspect
										
										#error_log.puts "ERROR RESPONSE : outbreak #{ob.id}"
										#error_log.puts e.inspect
										
									end
									
									begin
										 if ob.response != nil && ob.response.measures.length > 0
											 ob.response.measures.each_with_index do |m, i|
												 if i == 0
													 this_record.measure_1_description = m.description ? m.description : ''
												 elsif i == 1
													 this_record.measure_2_description = m.description ? m.description : ''
												 elsif i == 2
													 this_record.measure_3_description = m.description ? m.description : ''
												 end
											 end
										 end
						
									rescue Exception => e
											#logger.error "ERROR MEASURE : outbreak #{ob.id}"
											#logger.error e.inspect
											
											#error_log.puts "ERROR MEASURE : outbreak #{ob.id}"
											#error_log.puts e.inspect
							
									end
						
									@records.push(this_record)
							end
					
					
					
					
					@records.each do |r|
						csv << [
						r.outbreak_id ? r.outbreak_id : '', r.reporter_fname ? r.reporter_fname : '', r.reporter_lname ? r.reporter_lname : '', r.reporter_email ? r.reporter_email : '',
						r.outbreak_current_state ? r.outbreak_current_state : '', r.outbreak_onset_first ? r.outbreak_onset_first : '', r.outbreak_onset_last ? r.outbreak_onset_last : '',
						r.outbreak_user_reference ? r.outbreak_user_reference : '', r.outbreak_gezi_reference ? r.outbreak_gezi_reference : '', 
						r.outbreak_total_affected ? r.outbreak_total_affected : '', r.outbreak_at_risk ? r.outbreak_at_risk : '',  r.outbreak_lab_confirmed ? r.outbreak_lab_confirmed : '',
						r.outbreak_admitted ? r.outbreak_admitted : '', r.outbreak_died ? r.outbreak_died : '', r.outbreak_aetiology ? r.outbreak_aetiology : '',
						r.outbreak_transmission_mode ? r.outbreak_transmission_mode : '', r.outbreak_outbreak_type ? r.outbreak_outbreak_type : '', r.outbreak_comment ? r.outbreak_comment : '',
						r.outbreak_point_source ? r.outbreak_point_source : '', r.outbreak_point_source_date ? r.outbreak_point_source_date : '', r.outbreak_point_source_detail ? r.outbreak_point_source_detail : '',
						r.incident_category ? r.incident_category : '', r.incident_subcategory ? r.incident_subcategory : '', r.incident_subtype ? r.incident_subtype : '', r.incident_cuisine ? r.incident_cuisine : '',
						r.incident_detail ? r.incident_detail : '', r.incident_evidence_category ? r.incident_evidence_category : '', r.incident_evidence_name ? r.incident_evidence_name : '',
						r.location_placename ? r.location_placename : '', r.location_address_1 ? r.location_address_1 : '', r.location_address_2 ? r.location_address_2 : '',
						r.location_address_3 ? r.location_address_3 : '', r.location_town ? r.location_town : '', r.location_region ? r.location_region : '', r.location_hpu ? r.location_hpu : '',
						r.location_local_authority ? r.location_local_authority : '', r.location_postcode ? r.location_postcode : '' , r.location_lon ? r.location_lon : '',
						r.location_lat ? r.location_lon : '',
						r.agent_1_name ? r.agent_1_name : '', r.agent_1_serotype ? r.agent_1_serotype : '', r.agent_1_phagetype ? r.agent_1_phagetype : '', r.agent_1_other_typing ? r.agent_1_other_typing : '',
						r.agent_1_concentration ? r.agent_1_concentration : '', r.agent_1_confirmed ? r.agent_1_confirmed : '', r.agent_1_comment ? r.agent_1_comment : '',
						r.agent_2_name ? r.agent_2_name : '', r.agent_2_serotype ? r.agent_2_serotype : '', r.agent_2_phagetype ? r.agent_2_phagetype : '', r.agent_2_other_typing ? r.agent_2_other_typing : '',
						r.agent_2_concentration ? r.agent_2_concentration : '', r.agent_2_confirmed ? r.agent_2_confirmed : '', r.agent_2_comment ? r.agent_2_comment : '',
						r.agent_3_name ? r.agent_3_name : '', r.agent_3_serotype ? r.agent_3_serotype : '', r.agent_3_phagetype ? r.agent_3_phagetype : '', r.agent_3_other_typing ? r.agent_3_other_typing : '',
						r.agent_3_concentration ? r.agent_3_concentration : '', r.agent_3_confirmed ? r.agent_3_confirmed : '', r.agent_3_comment ? r.agent_3_comment : '',
						r.risk_1_factor_name ? r.risk_1_factor_name : '', r.risk_1_detail ? r.risk_1_detail : '', r.risk_2_factor_name ? r.risk_2_factor_name : '', r.risk_2_detail ? r.risk_2_detail : '',
						r.risk_3_factor_name ? r.risk_3_factor_name : '', r.risk_3_detail ? r.risk_3_detail : '',
						r.food_1_category ? r.food_1_category : '', r.food_1_subcategory ? r.food_1_subcategory : '', r.food_1_description ? r.food_1_description : '', r.food_1_contains_rse ? r.food_1_contains_rse : '', r.food_1_country ? r.food_1_country : '',
						r.food_1_evidence_category ? r.food_1_evidence_category : '', r.food_1_evidence_name ? r.food_1_evidence_name : '',
						r.food_2_category ? r.food_2_category : '', r.food_2_subcategory ? r.food_2_subcategory : '', r.food_2_description ? r.food_2_description : '', r.food_2_contains_rse ? r.food_2_contains_rse : '', r.food_2_country ? r.food_2_country : '',
						r.food_2_evidence_category ? r.food_2_evidence_category : '', r.food_2_evidence_name ? r.food_2_evidence_name : '',
						r.food_3_category ? r.food_3_category : '', r.food_3_subcategory ? r.food_3_subcategory : '', r.food_3_description ? r.food_3_description : '', r.food_3_contains_rse ? r.food_3_contains_rse : '', r.food_3_country ? r.food_3_country : '',
						r.food_3_evidence_category ? r.food_3_evidence_category : '', r.food_3_evidence_name ? r.food_3_evidence_name : '',
						r.response_premise_status ? r.response_premise_status : '', r.response_prosecution_status ? r.response_prosecution_status : '', r.response_notice_served ? r.response_notice_served : '', r.response_inspection_rating ? r.response_inspection_rating : '',
						r.response_comment ? r.response_comment : '',
						r.measure_1_description ? r.measure_1_description : '', r.measure_2_description ? r.measure_2_description : '', r.measure_3_description ? r.measure_3_description : ''
						]
						
					end
				rescue Exception => e
					#logger.error "ERROR CSV_EXPORT"
					#logger.error e.inspect
					
					#error_log.puts "ERROR CSV_EXPORT"
					#error_log.puts e.inspect
				end
			end
			
			#write the csv output to file and unlock
			#@csv_file.puts(csv_string)
			#@csv_file.flock(File::LOCK_UN)
			#error_log.close
		end
		
		render :update do |page|
			page.replace_html 'export_status', :partial => 'export_status_partial'
	  	end
	
	end

	def listing_old
		if params[:with] == 'all'
			@outbreaks = Outbreak.search("order_by_id").all
			
		elsif params[:with]
			
			@outbreaks = Outbreak.search(params[:with])
		else
			@outbreaks = Outbreak.search("order_by_id").all
			
		end
		
		@this_filepath = "/home/f0/html/efoss/" << Time.now.strftime("%I:%M:%S_%d:%m:%y")  << ".csv"
		
		@spawn_id = spawn(:nice => 7) do
			FasterCSV.open(@this_filepath, "w") do |csv|
				csv << [ "Outbreak.id", "Outbreak.user_id", "Outbreak.current_state", "Outbreak.onset_first", "Outbreak.onset_last", "Outbreak.user_reference",
				"Outbreak.gezi_reference", "Outbreak.transmission_mode", "Outbreak.total_affected", "Outbreak.at_risk", "Outbreak.lab_confirmed", 
				"Outbreak.admitted", "Outbreak.died", "Outbreak.aetiology", "Outbreak.outbreak_type", "Outbreak.comment", "Outbreak.point_source",
				"Outbreak.point_source_date", "Outbreak.point_source_detail", "Incident.category", "Incident.subcategory", "Incident.subtype",
				"Location.placename", "Location.address_1", "Location.address_2", "Location.address_3", "Location.town", "Location.postcode",
				"Location.local_authority", "Location.hpu", "Location.region", "Location.postcode", "Agent.name", "Agent.serotype", "Agent.phagetype", "Agent.other_typing",
				"Agent.concentration", "Agent.confirmed?", "Agent.comment"
				]
				
				@outbreaks.each do |o|
					     begin
						if o.aetiology == "BACTERIUM"
							this_agent = GenericAgent.new
							ba_agent = o.bacterial_agents.first
								this_agent.id = ba_agent.id
								this_agent.outbreak_id = ba_agent.outbreak_id
								this_agent.serotype = ba_agent.serotype
								this_agent.phagetype = ba_agent.phagetype
								this_agent.other_typing = ba_agent.other_typing
								this_agent.confirmed = ba_agent.confirmed
								this_agent.comment = ba_agent.comment
	
							this_pathogen = ba_agent.bacterium
						elsif o.aetiology == "VIRUS"
							this_agent = GenericAgent.new
							vi_agent = o.viral_agents.first
								this_agent.id = vi_agent.id
								this_agent.outbreak_id = vi_agent.outbreak_id
								this_agent.other_typing = vi_agent.other_typing
								this_agent.confirmed = vi_agent.confirmed
								this_agent.comment = vi_agent.comment
	
							this_pathogen = vi_agent.virus
						elsif o.aetiology == "PROTOZOA"
							this_agent = GenericAgent.new
							pr_agent = o.protozoal_agents.first
								this_agent.id = pr_agent.id
								this_agent.outbreak_id = pr_agent.outbreak_id
								this_agent.other_typing = pr_agent.other_typing
								this_agent.confirmed = pr_agent.confirmed
								this_agent.comment = pr_agent.comment
								
							this_pathogen = pr_agent.protozoa
						elsif o.aetiology == "TOXIN"
							this_agent = GenericAgent.new
							to_agent = o.toxic_agents.first
								this_agent.id = to_agent.id
								this_agent.outbreak_id = to_agent.outbreak_id
								this_agent.concentration = to_agent.concentration
								this_agent.confirmed = to_agent.confirmed
								this_agent.comment = to_agent.comment
							this_pathogen = to_agent.toxin
						elsif o.aetiology == "MIXED"
							
						else
							
							#UNKNOWNS and NULLS	
						end
						
						this_incident = o.incidents.first
						this_location = o.incidents.first.location
					
						csv << [ o.id, o.user_id, o.current_state, o.onset_first.to_s, o.onset_last.to_s, o.user_reference, o.gezi_reference, o.transmission_mode,
						o.total_affected, o.at_risk, o.lab_confirmed, o.admitted, o.died, o.aetiology, o.outbreak_type, o.comment, o.point_source, o.point_source_date.to_s,
						o.point_source_detail, o.incidents.first.category ? o.incidents.first.category.name : '' , o.incidents.first.subcategory ? o.incidents.first.subcategory.name : '',
						this_incident.subtype ? this_incident.subtype.name : '',
						this_location ? this_location.placename : '', this_location ? this_location.address_1 : '',
						this_location ? this_location.address_2 : '', this_location ? this_location.address_3 : '',
						this_location ? this_location.town : '', this_location ? this_location.postcode : '',
						this_location.local_authority ? this_location.local_authority.name : '', this_location.hpu ? this_location.hpu.name : '',
						this_location.region ? this_location.region.name : '', this_location.postcode ? this_location.postcode : '',
						this_pathogen.name ? this_pathogen.name : '', this_agent.serotype ? this_agent.serotype : '', this_agent.phagetype ? this_agent.phagetype : '',
						this_agent.other_typing ? this_agent.other_typing : '', this_agent.concentration ? this_agent.concentration : '', this_agent.confirmed ? this_agent.confirmed : '',
						this_agent.comment ? this_agent.comment : ''
						]
					rescue Exception => e
						logger.error "ERROR CSV_EXPORT outbreak #{o.id}"
						logger.error e.inspect
						
					end
						
					
					
					
				end
			end
		end
		render :update do |page|
			page.replace_html 'export_status', :partial => 'export_status_partial'
	  	end

	end
	
	def send_export
		@this_filepath = params[:with]
		csv_file = File.open(@this_filepath.to_s, 'r')
		
		csv_string = ""
		csv_file.each_line do |line|
			csv_string << line
		end
  

		send_data csv_string, :filename => "export.csv",
			    :type => 'text/csv; charset=iso-8859-1; header=present',
			    :disposition => "attachment; filename=export.csv"
		#send_file @this_filepath.to_s, :stream => false, :type=>"text/csv", :x_sendfile=>true

		#send_data csv_string, :filename => �export.csv�
	
		#File.delete(@this_filepath.to_s)
	end
	
	def export_checker
		filename_array = params['filename'].split(/\//)
		@file_found = 0
		@file_ready = 0
		
		@file_size = File.size(params['filename'])
		@this_filepath =  params['filename']
		
		if File.exists?(params['filename'])
			release_time = Time.now - 5.seconds
			if File.mtime(params['filename']).utc < release_time.utc
				
				@file_found = 1
				@file_ready = 1
				@file_access_time = File.mtime(params['filename'])
				@file_release_time = release_time
				@file_size = File.size(params['filename'])
				
				
		
				
			else
				@file_found = 1
				@file_ready = 0
				@file_size = File.size(params['filename'])
				
			end
			
		else
			
			@file_found = 0
			@file_ready = 0
			@file_size = File.size(params['filename'])
			
		end

		render :action => "export_checker"
	end
end
