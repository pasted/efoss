class FoodsController < ApplicationController
  # GET /foods
  # GET /foods.xml
  def index
    @foods = Food.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @foods }
    end
  end

  # GET /foods/1
  # GET /foods/1.xml
  def show
    @food = Food.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @food }
    end
  end

  # GET /foods/new
  # GET /foods/new.xml
  def new
    @food = Food.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @food }
    end
  end

  # GET /foods/1/edit
  def edit
    @food = Food.find(params[:id])
  end

  # POST /foods
  # POST /foods.xml
  def create
    @food = Food.new(params[:food])

    respond_to do |format|
      if @food.save
        flash[:notice] = 'Food was successfully created.'
        format.html { redirect_to(@food) }
        format.xml  { render :xml => @food, :status => :created, :location => @food }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @food.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /foods/1
  # PUT /foods/1.xml
  def update
    @food = Food.find(params[:id])

    respond_to do |format|
      if @food.update_attributes(params[:food])
        flash[:notice] = 'Food was successfully updated.'
        format.html { redirect_to(@food) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @food.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /foods/1
  # DELETE /foods/1.xml
  def destroy
    @food = Food.find(params[:id])
    @food.destroy

    respond_to do |format|
      format.html { redirect_to(foods_url) }
      format.xml  { head :ok }
    end
  end
  
  def update_food_subcategory
  	  this_food_category_id = params[:food_category_id]
  	  this_food_id = params[:food_id]
  	  
  	  food_category = FoodCategory.find(this_food_category_id)
  	  @food_subcategories = food_category.food_subcategories
  	  respond_to do |format|
  	  	  format.js  
  	  end
  end
  # Updates the food subcategory select menu based on the food category
  # Uses the :this_child_index parameter to pass the id of the current food element, and update the relevant food subcategory
  def update_food_subcategory_old
  	  this_food_category_id = params[:food_category_id]
  	  @this_child_index = params[:this_child_index] ? params[:this_child_index] : 0
  	  
  	  if params[:id]
  	  	  @outbreak = Outbreak.find(params[:id])
  	  else
  	  	  @outbreak = Outbreak.new
  	  	  @outbreak.risks.build
  	  	  @outbreak.incidents.build.build_location
  	  	  @outbreak.incidents.first.build_evidence
  	  	  @outbreak.viral_agents.build
  	  	  @outbreak.bacterial_agents.build
  	  	  @outbreak.toxic_agents.build
  	  	  @outbreak.protozoal_agents.build
  	  	  @outbreak.foods.build
  	  	  @food = @outbreak.foods.first
  	  end
  	  
  	  food_category = FoodCategory.find(this_food_category_id)
  	  @food_subcategories = food_category.food_subcategories
  	 
  	  render :update do |page|
  	  	food_subcategory_id = "food_subcategory_select_" << @this_child_index.to_s
  	  	page.replace_html food_subcategory_id.to_s, :partial => 'foods/food_subcategory_select_update', :locals => {:this_child_index => @this_child_index, :food_subcategories => @food_subcategories, :outbreak => @outbreak, :food => @food}
  	  end
  
  end
  
    
  def update_food_subcategory
  	  this_food_category_id = params[:food_category_id]
  	  this_food_id = params[:food_id]
  	  food_uid = params[:food_uid] ? params[:food_uid] : this_food_id
  	  food_div_uid = "food_" << food_uid
  	 
  	  
  	  if params[:id]
  	  	  @outbreak = Outbreak.find(params[:id])
  	  	  @food = Food.find(this_food_id)
  	  else
  	  	  @outbreak = Outbreak.new
  	  	  @outbreak.foods.build
  	  	  @food = @outbreak.foods.first
  	  end
  	  
  	  food_category = FoodCategory.find(this_food_category_id)
  	  @food_subcategories = food_category.food_subcategories
  	  render :update do |page|
  	  	  page.replace_html food_div_uid, :partial => 'foods/food_subcategory_select_update', :locals => { :food_subcategories => @food_subcategories, :outbreak => @outbreak, :food => @food}  	  
	  end
  end
  
end
