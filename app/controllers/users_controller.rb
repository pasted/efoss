class UsersController < ApplicationController
	before_filter :authenticate_user!
	
  def account
  	  
  	  @user = User.find(params[:user_id])
  	  @outbreaks = Outbreak.find_all_by_user_id(@user.id)
  	  @outbreaks_confirmed = Outbreak.find(:all, :conditions => {:user_id => @user.id, :current_state => "active"})

  end
	
  # GET /users
  # GET /users.xml
  def index
    @users = User.all


    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @users }
    end
  end
  
  # GET /users/1
  # GET /users/1.xml
  def show
  	  
  	  @user = User.find(params[:id])
	
	  respond_to do |format|
	      format.html # show.html.erb
	      format.xml  { render :xml => @user }
	  end

  rescue ActiveRecord::RecordNotFound
    	    respond_to_not_found(:json, :xml, :html)
  end


  # GET /users/new
  # GET /users/new.xml
  def new
    @user = User.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @user }
    end
  end

  # GET /users/1/edit
  def edit
    @user = User.find(params[:id])
  end

  # POST /users
  # POST /users.xml
  def create
    @user.attributes = params[:user]
    @user.role_ids = params[:user][:role_ids] if params[:user]
    
    respond_to do |format|
      if @user.save
        flash[:notice] = 'User was successfully created.'
        format.html { redirect_to(@user) }
        format.xml  { render :xml => @user, :status => :created, :location => @user }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @user.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /users/1
  # PUT /users/1.xml
  def update
    @user = User.find(params[:id])

    respond_to do |format|
      if @user.update_attributes(params[:user])
        flash[:notice] = 'User was successfully updated.'
        format.html { redirect_to(@user) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @user.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.xml
  def destroy
    @user = User.find(params[:id])
    @user.destroy

    respond_to do |format|
      format.html { redirect_to(users_url) }
      format.xml  { head :ok }
    end
  end
  
  # Remote check of existing emails
  def check_email
	@user = User.find_by_email(params[:user][:email])
	
	respond_to do |format|
		format.json { render :json => !@user }
	end
  end

  
  
end
