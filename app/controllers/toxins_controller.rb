class ToxinsController < ApplicationController
  # GET /toxins
  # GET /toxins.xml
  def index
    @toxins = Toxin.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @toxins }
    end
  end

  # GET /toxins/1
  # GET /toxins/1.xml
  def show
    @toxin = Toxin.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @toxin }
    end
  end

  # GET /toxins/new
  # GET /toxins/new.xml
  def new
    @toxin = Toxin.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @toxin }
    end
  end

  # GET /toxins/1/edit
  def edit
    @toxin = Toxin.find(params[:id])
  end

  # POST /toxins
  # POST /toxins.xml
  def create
    @toxin = Toxin.new(params[:toxin])

    respond_to do |format|
      if @toxin.save
        flash[:notice] = 'Toxin was successfully created.'
        format.html { redirect_to(@toxin) }
        format.xml  { render :xml => @toxin, :status => :created, :location => @toxin }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @toxin.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /toxins/1
  # PUT /toxins/1.xml
  def update
    @toxin = Toxin.find(params[:id])

    respond_to do |format|
      if @toxin.update_attributes(params[:toxin])
        flash[:notice] = 'Toxin was successfully updated.'
        format.html { redirect_to(@toxin) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @toxin.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /toxins/1
  # DELETE /toxins/1.xml
  def destroy
    @toxin = Toxin.find(params[:id])
    @toxin.destroy

    respond_to do |format|
      format.html { redirect_to(toxins_url) }
      format.xml  { head :ok }
    end
  end
end
