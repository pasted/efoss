class LocalAuthoritiesController < ApplicationController
  # GET /local_authorities
  # GET /local_authorities.xml
  def index
  	  @local_authorities = LocalAuthority.all(:order => :id)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @local_authorities }
    end
  end

  # GET /local_authorities/1
  # GET /local_authorities/1.xml
  def show
    @local_authority = LocalAuthority.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @local_authority }
    end
  end

  # GET /local_authorities/new
  # GET /local_authorities/new.xml
  def new
    @local_authority = LocalAuthority.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @local_authority }
    end
  end

  # GET /local_authorities/1/edit
  def edit
    @local_authority = LocalAuthority.find(params[:id])
  end

  # POST /local_authorities
  # POST /local_authorities.xml
  def create
    @local_authority = LocalAuthority.new(params[:local_authority])

    respond_to do |format|
      if @local_authority.save
        flash[:notice] = 'LocalAuthority was successfully created.'
        format.html { redirect_to(@local_authority) }
        format.xml  { render :xml => @local_authority, :status => :created, :location => @local_authority }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @local_authority.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /local_authorities/1
  # PUT /local_authorities/1.xml
  def update
    @local_authority = LocalAuthority.find(params[:id])

    respond_to do |format|
      if @local_authority.update_attributes(params[:local_authority])
        flash[:notice] = 'LocalAuthority was successfully updated.'
        format.html { redirect_to(@local_authority) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @local_authority.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /local_authorities/1
  # DELETE /local_authorities/1.xml
  def destroy
    @local_authority = LocalAuthority.find(params[:id])
    @local_authority.destroy

    respond_to do |format|
      format.html { redirect_to(local_authorities_url) }
      format.xml  { head :ok }
    end
  end
end
