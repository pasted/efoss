class EvidenceCategoriesController < ApplicationController
  # GET /evidence_categories
  # GET /evidence_categories.xml
  def index
    @evidence_categories = EvidenceCategory.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @evidence_categories }
    end
  end

  # GET /evidence_categories/1
  # GET /evidence_categories/1.xml
  def show
    @evidence_category = EvidenceCategory.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @evidence_category }
    end
  end

  # GET /evidence_categories/new
  # GET /evidence_categories/new.xml
  def new
    @evidence_category = EvidenceCategory.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @evidence_category }
    end
  end

  # GET /evidence_categories/1/edit
  def edit
    @evidence_category = EvidenceCategory.find(params[:id])
  end

  # POST /evidence_categories
  # POST /evidence_categories.xml
  def create
    @evidence_category = EvidenceCategory.new(params[:evidence_category])

    respond_to do |format|
      if @evidence_category.save
        flash[:notice] = 'EvidenceCategory was successfully created.'
        format.html { redirect_to(@evidence_category) }
        format.xml  { render :xml => @evidence_category, :status => :created, :location => @evidence_category }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @evidence_category.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /evidence_categories/1
  # PUT /evidence_categories/1.xml
  def update
    @evidence_category = EvidenceCategory.find(params[:id])

    respond_to do |format|
      if @evidence_category.update_attributes(params[:evidence_category])
        flash[:notice] = 'EvidenceCategory was successfully updated.'
        format.html { redirect_to(@evidence_category) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @evidence_category.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /evidence_categories/1
  # DELETE /evidence_categories/1.xml
  def destroy
    @evidence_category = EvidenceCategory.find(params[:id])
    @evidence_category.destroy

    respond_to do |format|
      format.html { redirect_to(evidence_categories_url) }
      format.xml  { head :ok }
    end
  end
end
