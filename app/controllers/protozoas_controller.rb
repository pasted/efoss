class ProtozoasController < ApplicationController
  # GET /protozoas
  # GET /protozoas.xml
  def index
    @protozoas = Protozoa.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @protozoas }
    end
  end

  # GET /protozoas/1
  # GET /protozoas/1.xml
  def show
    @protozoa = Protozoa.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @protozoa }
    end
  end

  # GET /protozoas/new
  # GET /protozoas/new.xml
  def new
    @protozoa = Protozoa.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @protozoa }
    end
  end

  # GET /protozoas/1/edit
  def edit
    @protozoa = Protozoa.find(params[:id])
  end

  # POST /protozoas
  # POST /protozoas.xml
  def create
    @protozoa = Protozoa.new(params[:protozoa])

    respond_to do |format|
      if @protozoa.save
        flash[:notice] = 'Protozoa was successfully created.'
        format.html { redirect_to(@protozoa) }
        format.xml  { render :xml => @protozoa, :status => :created, :location => @protozoa }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @protozoa.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /protozoas/1
  # PUT /protozoas/1.xml
  def update
    @protozoa = Protozoa.find(params[:id])

    respond_to do |format|
      if @protozoa.update_attributes(params[:protozoa])
        flash[:notice] = 'Protozoa was successfully updated.'
        format.html { redirect_to(@protozoa) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @protozoa.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /protozoas/1
  # DELETE /protozoas/1.xml
  def destroy
    @protozoa = Protozoa.find(params[:id])
    @protozoa.destroy

    respond_to do |format|
      format.html { redirect_to(protozoas_url) }
      format.xml  { head :ok }
    end
  end
end
