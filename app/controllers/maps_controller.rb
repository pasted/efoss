require 'event'
class MapsController < ApplicationController
	def overview
			@map = Mapstraction.new("map_div", :microsoft)
			@map.control_init(:small_map => true, :map_type => true)

				 @markers = Array.new
				 @events = Array.new
				 @outbreaks = Array.new
				 
				 @regions = Region.find(:all, :order => :name).collect{|c| [c.name, c.name]}.insert(0,["", ""])

				 @categories = Category.find(:all, :order => "outbreak_type ASC").collect{|c| [c.name, c.name]}.insert(0,["", ""])

				 
				  
				  
				 @viruses = Virus.all.collect{|c| [c.name, c.name]}.insert(0,["", ""])  	    
				 @bacteria = Bacterium.all.collect{|c| [c.name, c.name]}.insert(0,["", ""])
				 @protozoas = Protozoa.all.collect{|c| [c.name, c.name]}.insert(0,["", ""])    
				 @toxins = Toxin.all.collect{|c| [c.name, c.name]}.insert(0,["", ""])
				 
				 @outbreak_types = Property.find(:all, :conditions => {:field => "OUTBREAKS:OUTBREAK_TYPE"} ).collect{|c| [c.property_value, c.property_value]}.insert(0,["", ""])
				 @transmission_modes = Property.find(:all, :conditions => ["field ILIKE ? OR field ILIKE ?", "OUTBREAKS:TRANSMISSION_MODE:FOODBORNE", "OUTBREAKS:TRANSMISSION_MODE:NON-FOODBORNE"]).collect{|c| [c.property_value, c.property_value]}.insert(0, ["",""])
				 @pathogens = Property.find(:all, :conditions => {:field => "PATHOGENS:CATEGORY"}).collect{|c| [c.property_value, c.property_value]}.insert(0,["", ""])
				 @earliest_date_year = Property.find_by_field("EARLIEST_DATE:YEAR").property_value.to_i ? Property.find_by_field("EARLIEST_DATE:YEAR").property_value.to_i : 1992
				 
				 @food_categories = FoodCategory.all.collect{|c| [c.name, c.name]}.insert(0,["", ""])
				 
				  
			  @options_for_subcategories = Array.new
			  Category.all.each do |this_category|
				  final_array = Array.new
				  final_array.push(this_category.name)
				  @subcategories = Subcategory.find(:all, :conditions => {:category_id => this_category.id})
				  subcategory_array = Array.new
				  subcategory_array.push(['',''])
				  @subcategories.each do |this_subcategory|
					  subcategory_array.push([this_subcategory.name, this_subcategory.name])
				  end
				  final_array.push(subcategory_array)
				  @options_for_subcategories.push(final_array)
			  end
			  
			  #raise @options_for_subcategories.inspect
			  @options_for_subtypes = Array.new
			  Subcategory.all.each do |this_subcategory|
				  final_array = Array.new
				  final_array.push(this_subcategory.name)
				  @subtypes = Subtype.find(:all, :conditions => {:subcategory_id => this_subcategory.id})
				  subtype_array = Array.new
				  subtype_array.push(['',''])
				  @subtypes.each do |this_subtype|
					  subtype_array.push([this_subtype.name, this_subtype.name])  
				  end
				  final_array.push(subtype_array)
				  @options_for_subtypes.push(final_array)
			  end
			  
			  @options_for_hpus = Array.new
			  Region.all.each do |this_region|
				  final_array = Array.new
				  final_array.push(this_region.name)
				  @hpus = Hpu.find(:all, :conditions => {:region_id => this_region.id})
				  hpu_array = Array.new
				  hpu_array.push(['',''])
				  @hpus.each do |this_hpu|
					  hpu_array.push([this_hpu.name, this_hpu.name])  
				  end
				  final_array.push(hpu_array)
				  @options_for_hpus.push(final_array)
			  end
			  
			  @options_for_authorities = Array.new
			  Hpu.all.each do |this_hpu|
				  final_array = Array.new
				  final_array.push(this_hpu.name)
				  @authorities = LocalAuthority.find(:all, :conditions => {:hpu_id => this_hpu.id})
				  la_array = Array.new
				  la_array.push(['',''])
				  @authorities.each do |this_authority|
					  la_array.push([this_authority.name, this_authority.name])  
				  end
				  final_array.push(la_array)
				  @options_for_authorities.push(final_array)
			  end
			  
			  @options_for_food_subcategories = Array.new
			  FoodCategory.all.each do |this_food|
				  final_array = Array.new
				  final_array.push(this_food.name)
				  @food_subcategories = FoodSubcategory.find(:all, :conditions => {:food_category_id => this_food.id})
				  subcategory_array = Array.new
				  subcategory_array.push(['',''])
				  @food_subcategories.each do |food_subcategory|
					  subcategory_array.push([food_subcategory.name, food_subcategory.name])  
				  end
				  final_array.push(subcategory_array)
				  @options_for_food_subcategories.push(final_array)
			  end	
				  
				  @factors = Factor.all
	
					final_foodborne_array = Array.new
					final_foodborne_array.push("FOODBORNE")
					final_nonfoodborne_array = Array.new
					final_nonfoodborne_array.push("NON-FOODBORNE")
					foodborne_array = Array.new
					nonfoodborne_array = Array.new
			
				  @factors.each do |this_factor|
					  if this_factor.outbreak_type == "FOODBORNE"
						  foodborne_array.push([this_factor.name, this_factor.id.to_s])  
					  elsif this_factor.outbreak_type == "NON-FOODBORNE"
						  nonfoodborne_array.push([this_factor.name, this_factor.id.to_s])
					  end
				  end
				  final_foodborne_array.push(foodborne_array)
				  final_nonfoodborne_array.push(nonfoodborne_array)
				  @options_for_factors = Array[final_foodborne_array, final_nonfoodborne_array]
				  
				  if params[:onset_first_year] != nil && !params[:onset_first_year].empty?
					  onset_first_start = Date.new(params[:onset_first_year].to_i, 1, 1)
					  onset_first_end = Date.new(params[:onset_first_year].to_i, 12, 31)
					  params[:search][:onset_first_year] = [onset_first_start.strftime("%Y-%m-%d"), onset_first_end.strftime("%Y-%m-%d")]
					  
				  end
				  if params[:onset_last_year] != nil && !params[:onset_last_year].empty?
					  onset_last_start = Date.new(params[:onset_last_year].to_i, 1, 1)
					  onset_last_end = Date.new(params[:onset_last_year].to_i, 12, 31)
					  params[:search][:onset_last_year] = [onset_last_start.strftime("%Y-%m-%d"), onset_last_end.strftime("%Y-%m-%d")]
					  
				  end
				  if params[:point_source_date_year] != nil && !params[:point_source_date_year].empty?
					  point_source_date_start = Date.new(params[:point_source_date_year].to_i, 1, 1)
					  point_source_date_end = Date.new(params[:point_source_date_year].to_i, 12, 31)
					  params[:search][:point_source_year] = [point_source_date_start.strftime("%Y-%m-%d"), point_source_date_end.strftime("%Y-%m-%d")]
					  
				  end
				  
				  whitelist_properties = Property.find(:all, :conditions => {:field => "WHITELIST::SEARCHES"} )
				  whitelist_array = whitelist_properties.collect {|p| p.property_value}
				  whitelist_array.collect! {|wh| wh.split(',')}
				  whitelist_array[0].collect! {|wh| wh.strip!}
				  whitelist = whitelist_array[0].compact
  	 
				  if params[:search] != nil
					  params[:search].each do |this_search|
						  
						  if !whitelist.include?(this_search[0].upcase)
							  
							  params[:search].delete(this_search[0].to_sym)
						  end
					  end
				  end
				  
				  @search_params = params[:search]
				  if params[:search] == nil
				  	  @search = Outbreak.search(:onset_first_after => "2009-01-01")
				  	  @search_params = Hash["onset_first_after","2009-01-01"]
				  else
				  	  
				  	  @search = Outbreak.search(params[:search])
				  end
				  if current_user.role?('admin')
					  @outbreaks_collection = @search.all
					 
				  else	  
					
					  @outbreaks_collection = @search.all
				  end
				  
				  
				  
		        @markers = Array.new  
		        @outbreaks_collection.each do |outbreak|
			  	  begin
					 if outbreak.locations.first
						 #if outbreak.onset_first != nil && outbreak.onset_last != nil
						 	  
							 this_location = outbreak.locations.first
							 #Default unmappable locations to lon / lat of 0 / 0 
							 #Escape these from the map
							 if (this_location.lat && this_location.lon) && (this_location.lat != 0 && this_location != 0)
								    window_info = ""
								    this_event_header = ""
								   
								   
								    window_info = render_to_string :partial => "maps/marker_info", :locals => {:outbreak => outbreak}
								    if outbreak.aetiology == "BACTERIUM"
									   # @map.marker_init(Marker.new([this_location.lat.to_f, this_location.lon.to_f], :title => "Bacterial", :info_bubble => window_info, :icon => "../images/amber_marker.png"))
									    ba_name = outbreak.bacterial_agents.first != nil ? outbreak.bacterial_agents.first.bacterium.name : "Unknown bacterium"
									    this_marker = Marker.new([this_location.lat.to_f, this_location.lon.to_f], :title => "Bacterial", :info_bubble => window_info, :icon => "../images/amber_marker.png" )
									    begin
									    	    this_event_header = "<div class=\"bacterium_header\" title=\"Bacterial\">" << ba_name << "</div>"
									    rescue
									    	    this_event_header = "<div class=\"bacterium_header\" title=\"Bacterial\">Unknown bacterium</div>"    
									    end
								    elsif outbreak.aetiology == "VIRUS" 
									   # @map.marker_init(Marker.new([this_location.lat.to_f, this_location.lon.to_f], :title => "Viral", :info_bubble => window_info, :icon => "../images/dred_marker.png"))
									    va_name = outbreak.viral_agents.first != nil ? outbreak.viral_agents.first.virus.name : "Unknown Virus"
									    this_marker = Marker.new([this_location.lat.to_f, this_location.lon.to_f], :title => "Viral", :info_bubble => window_info, :icon => "../images/dred_marker.png")
									    begin
									    	    this_event_header = "<div class=\"virus_header\" title=\"Viral\">" << va_name << "</div>"
									    rescue
									    	    this_event_header = "<div class=\"virus_header\" title=\"Viral\"></div>"	    
									    end
								    elsif outbreak.aetiology == "PROTOZOA"
									   # @map.marker_init(Marker.new([this_location.lat.to_f, this_location.lon.to_f], :title => "Protozoal", :info_bubble => window_info, :icon => "../images/blue_marker.png"))
									    pa_name = outbreak.protozoal_agents.first != nil ? outbreak.protozoal_agents.first.protozoa.name : "Unknown Protozoa"
									    this_marker = Marker.new([this_location.lat.to_f, this_location.lon.to_f], :title => "Protozoal", :info_bubble => window_info, :icon => "../images/blue_marker.png")
									    begin
									    	    this_event_header = "<div class=\"protozoa_header\" title=\"Protozoal\">" << pa_name << "</div>"
									    rescue
									    	    this_event_header = "<div class=\"protozoa_header\" title=\"Protozoal\"></div>"    
									    end
								    elsif outbreak.aetiology == "TOXIN" 
									   # @map.marker_init(Marker.new([this_location.lat.to_f, this_location.lon.to_f], :title => "Toxic", :info_bubble => window_info, :icon => "../images/green_marker.png"))
									    ta_name = outbreak.toxic_agents.first != nil ? outbreak.toxic_agents.first.toxin.name : "Unknown Toxin"
									    this_marker = Marker.new([this_location.lat.to_f, this_location.lon.to_f], :title => "Toxic", :info_bubble => window_info, :icon => "../images/green_marker.png")
									    begin
									    	    this_event_header = "<div class=\"toxin_header\" title=\"Toxic\">" << ta_name << "</div>"
									    rescue
									    	    this_event_header = "<div class=\"toxin_header\" title=\"Toxic\"></div>"    
									    end
								    elsif outbreak.aetiology == "UNKNOWN"
									   # @map.marker_init(Marker.new([this_location.lat.to_f, this_location.lon.to_f], :title => "Unknown", :info_bubble => window_info, :icon => "../images/purple_marker.png"))
									   
									   this_marker = Marker.new([this_location.lat.to_f, this_location.lon.to_f], :title => "Unknown", :info_bubble => window_info, :icon => "../images/purple_marker.png")
									    this_event_header = "<div class=\"unknown_header\" title=\"Unknown\">Unknown agent</div>"
									    
								    elsif outbreak.aetiology == "MIXED"
									   # @map.marker_init(Marker.new([this_location.lat.to_f, this_location.lon.to_f], :title => "Mixed", :info_bubble => window_info, :icon => "../images/yellowd_marker.png"))
									   this_marker = Marker.new([this_location.lat.to_f, this_location.lon.to_f], :title => "Mixed", :info_bubble => window_info, :icon => "../images/yellowd_marker.png")
									    this_event_header = "<div class=\"mixed_header\" title=\"Mixed\">Mixed agent</div>"
								    end
								    
								    @markers.push(this_marker)
								    
									     
							end
							
							 if outbreak.onset_first && outbreak.onset_last
										     this_event = Event.new
										     this_event.onset_first = outbreak.onset_first
										     this_event.onset_last = outbreak.onset_last
										     this_event.info = this_event_header
										     this_event.info << "<div>Outbreak :: " << (outbreak.id ? outbreak.id.to_s : "0") <<  "</div>"
										     this_event.info << "<div><table><tr><th>Number at risk</th><th>Total affected</th><th>Number admitted</th><th>Died</th></tr><tr><td>" << (outbreak.at_risk != nil ? outbreak.at_risk.to_s : "0").to_s
										     this_event.info << "</td><td>" << (outbreak.total_affected != nil ? outbreak.total_affected.to_s : "0").to_s
										     this_event.info << "</td><td>" << (outbreak.admitted != nil ? outbreak.admitted.to_s : "0").to_s
										     this_event.info << "</td><td>" << (outbreak.died != nil ? outbreak.died.to_s : "0").to_s
										     this_event.info << "</td></tr></table></div>"
										     @events.push(this_event)
							end
							
							if @outbreaks.length == 0
								@outbreaks.push(outbreak)

							elsif outbreak.id > @outbreaks.last.id
								@outbreaks.push(outbreak)
							
							elsif outbreak.id < @outbreaks.last.id
								last_outbreak = @outbreaks.pop
								@outbreaks.push(outbreak)
								@outbreaks.push(last_outbreak)
							end
						#end 
					 end
				 rescue Exception => e  
				 	 logger.error("ERROR MAP " << outbreak.inspect << " :::::: " << e.inspect )
				 end
			 end
			 if @markers.length > 200
				 clusterer = Clusterer.new(@markers, :max_visible_markers => 50, :icon => "../images/orange_marker.png")
				 @map.clusterer_init(clusterer)
			 else
			 	 marker_group = MarkerGroup.new(@markers, "markers")
			 	 @map.marker_group_init(marker_group)	 
			 end
			 @map.center_zoom_init([53,-3],6)
 
			 respond_to do |format|
			 	 format.html 
			 	 format.js	 
			 end
		
		
	end
	
	
	def national_map
		
			@map = Mapstraction.new("map_div", :microsoft)
			@map.control_init(:small_map => true, :map_type => true)

				 @markers = Array.new
				 @events = Array.new
				 @outbreaks = Array.new
				 
				 @regions = Region.find(:all, :order => :name).collect{|c| [c.name, c.name]}.insert(0,["", ""])

				 @categories = Category.find(:all, :order => "outbreak_type ASC").collect{|c| [c.name, c.name]}.insert(0,["", ""])

				 
				  
				  
				 @viruses = Virus.all.collect{|c| [c.name, c.name]}.insert(0,["", ""])  	    
				 @bacteria = Bacterium.all.collect{|c| [c.name, c.name]}.insert(0,["", ""])
				 @protozoas = Protozoa.all.collect{|c| [c.name, c.name]}.insert(0,["", ""])    
				 @toxins = Toxin.all.collect{|c| [c.name, c.name]}.insert(0,["", ""])
				 
				 @outbreak_types = Property.find(:all, :conditions => {:field => "OUTBREAKS:OUTBREAK_TYPE"} ).collect{|c| [c.property_value, c.property_value]}.insert(0,["", ""])
				 @transmission_modes = Property.find(:all, :conditions => ["field ILIKE ? OR field ILIKE ?", "OUTBREAKS:TRANSMISSION_MODE:FOODBORNE", "OUTBREAKS:TRANSMISSION_MODE:NON-FOODBORNE"]).collect{|c| [c.property_value, c.property_value]}.insert(0, ["",""])
				 @pathogens = Property.find(:all, :conditions => {:field => "PATHOGENS:CATEGORY"}).collect{|c| [c.property_value, c.property_value]}.insert(0,["", ""])
				 @earliest_date_year = Property.find_by_field("EARLIEST_DATE:YEAR").property_value.to_i ? Property.find_by_field("EARLIEST_DATE:YEAR").property_value.to_i : 1992
				 
				 @food_categories = FoodCategory.all.collect{|c| [c.name, c.name]}.insert(0,["", ""])
				 
				  
			  @options_for_subcategories = Array.new
			  Category.all.each do |this_category|
				  final_array = Array.new
				  final_array.push(this_category.name)
				  @subcategories = Subcategory.find(:all, :conditions => {:category_id => this_category.id})
				  subcategory_array = Array.new
				  subcategory_array.push(['',''])
				  @subcategories.each do |this_subcategory|
					  subcategory_array.push([this_subcategory.name, this_subcategory.name])
				  end
				  final_array.push(subcategory_array)
				  @options_for_subcategories.push(final_array)
			  end
			  
			  #raise @options_for_subcategories.inspect
			  @options_for_subtypes = Array.new
			  Subcategory.all.each do |this_subcategory|
				  final_array = Array.new
				  final_array.push(this_subcategory.name)
				  @subtypes = Subtype.find(:all, :conditions => {:subcategory_id => this_subcategory.id})
				  subtype_array = Array.new
				  subtype_array.push(['',''])
				  @subtypes.each do |this_subtype|
					  subtype_array.push([this_subtype.name, this_subtype.name])  
				  end
				  final_array.push(subtype_array)
				  @options_for_subtypes.push(final_array)
			  end
			  
			  @options_for_hpus = Array.new
			  Region.all.each do |this_region|
				  final_array = Array.new
				  final_array.push(this_region.name)
				  @hpus = Hpu.find(:all, :conditions => {:region_id => this_region.id})
				  hpu_array = Array.new
				  hpu_array.push(['',''])
				  @hpus.each do |this_hpu|
					  hpu_array.push([this_hpu.name, this_hpu.name])  
				  end
				  final_array.push(hpu_array)
				  @options_for_hpus.push(final_array)
			  end
			  
			  @options_for_authorities = Array.new
			  Hpu.all.each do |this_hpu|
				  final_array = Array.new
				  final_array.push(this_hpu.name)
				  @authorities = LocalAuthority.find(:all, :conditions => {:hpu_id => this_hpu.id})
				  la_array = Array.new
				  la_array.push(['',''])
				  @authorities.each do |this_authority|
					  la_array.push([this_authority.name, this_authority.name])  
				  end
				  final_array.push(la_array)
				  @options_for_authorities.push(final_array)
			  end
			  
			  @options_for_food_subcategories = Array.new
			  FoodCategory.all.each do |this_food|
				  final_array = Array.new
				  final_array.push(this_food.name)
				  @food_subcategories = FoodSubcategory.find(:all, :conditions => {:food_category_id => this_food.id})
				  subcategory_array = Array.new
				  subcategory_array.push(['',''])
				  @food_subcategories.each do |food_subcategory|
					  subcategory_array.push([food_subcategory.name, food_subcategory.name])  
				  end
				  final_array.push(subcategory_array)
				  @options_for_food_subcategories.push(final_array)
			  end	
				  
				  @factors = Factor.all
	
					final_foodborne_array = Array.new
					final_foodborne_array.push("FOODBORNE")
					final_nonfoodborne_array = Array.new
					final_nonfoodborne_array.push("NON-FOODBORNE")
					foodborne_array = Array.new
					nonfoodborne_array = Array.new
			
				  @factors.each do |this_factor|
					  if this_factor.outbreak_type == "FOODBORNE"
						  foodborne_array.push([this_factor.name, this_factor.id.to_s])  
					  elsif this_factor.outbreak_type == "NON-FOODBORNE"
						  nonfoodborne_array.push([this_factor.name, this_factor.id.to_s])
					  end
				  end
				  final_foodborne_array.push(foodborne_array)
				  final_nonfoodborne_array.push(nonfoodborne_array)
				  @options_for_factors = Array[final_foodborne_array, final_nonfoodborne_array]
				  
				  if params[:onset_first_year] != nil && !params[:onset_first_year].empty?
					  onset_first_start = Date.new(params[:onset_first_year].to_i, 1, 1)
					  onset_first_end = Date.new(params[:onset_first_year].to_i, 12, 31)
					  params[:search][:onset_first_year] = [onset_first_start.strftime("%Y-%m-%d"), onset_first_end.strftime("%Y-%m-%d")]
					  
				  end
				  if params[:onset_last_year] != nil && !params[:onset_last_year].empty?
					  onset_last_start = Date.new(params[:onset_last_year].to_i, 1, 1)
					  onset_last_end = Date.new(params[:onset_last_year].to_i, 12, 31)
					  params[:search][:onset_last_year] = [onset_last_start.strftime("%Y-%m-%d"), onset_last_end.strftime("%Y-%m-%d")]
					  
				  end
				  if params[:point_source_date_year] != nil && !params[:point_source_date_year].empty?
					  point_source_date_start = Date.new(params[:point_source_date_year].to_i, 1, 1)
					  point_source_date_end = Date.new(params[:point_source_date_year].to_i, 12, 31)
					  params[:search][:point_source_year] = [point_source_date_start.strftime("%Y-%m-%d"), point_source_date_end.strftime("%Y-%m-%d")]
					  
				  end
				  
				  whitelist_properties = Property.find(:all, :conditions => {:field => "WHITELIST::SEARCHES"} )
				  whitelist_array = whitelist_properties.collect {|p| p.property_value}
				  whitelist_array.collect! {|wh| wh.split(',')}
				  whitelist_array[0].collect! {|wh| wh.strip!}
				  whitelist = whitelist_array[0].compact
  	 
				  if params[:search] != nil
				  	  
				  	  if params[:search][:bacterial_agents_bacterium_name_like_any] != nil && !params[:search][:bacterial_agents_bacterium_name_like_any].empty?
						  params[:search][:bacterial_agents_category_like] = "CAUSATIVE"
					  end
					  if params[:search][:viral_agents_virus_name_like_any] != nil && !params[:search][:viral_agents_virus_name_like_any].empty?
						  params[:search][:viral_agents_category_like] = "CAUSATIVE"
					  end
					  if params[:search][:protozoal_agents_protozoa_name_like_any] != nil && !params[:search][:protozoal_agents_protozoa_name_like_any].empty?
						  params[:search][:protozoal_agents_category_like] = "CAUSATIVE"
					  end
					  if params[:search][:toxic_agents_toxin_name_like_any] != nil && !params[:search][:toxic_agents_toxin_name_like_any].empty?
						  params[:search][:toxic_agents_category_like] = "CAUSATIVE"
					  end
					  if params[:search][:incidents_location_encrypted_postcode_like] != nil && !params[:search][:incidents_location_encrypted_postcode_like].empty?
						  tmp_location = Location.new
						  tmp_location.postcode = params[:search][:incidents_location_encrypted_postcode_like]
						  tmp_location.encrypted_postcode
						  params[:search][:incidents_location_encrypted_postcode_like] = tmp_location.encrypted_postcode
					  end
					  params[:search].each do |this_search|
						  
						  if !whitelist.include?(this_search[0].upcase)
							  
							  params[:search].delete(this_search[0].to_sym)
						  end
					  end
				  end
				  
				  @search_params = params[:search]
				  if params[:search] == nil
				  	  @search = Outbreak.search(:onset_first_after => "2009-01-01")
				  	  @search_params = Hash["onset_first_after","2009-01-01"]
				  else
				  	  
				  	  @search = Outbreak.search(params[:search])
				  end
				  if current_user.role?('admin')
					  @outbreaks_collection = @search.all
					 
				  else	  
					
					  @outbreaks_collection = @search.all
				  end
				  
				  
				  
		        @markers = Array.new  
		        @outbreaks_collection.each do |outbreak|
			  	 # begin
					 if outbreak.locations.first
						 #if outbreak.onset_first != nil && outbreak.onset_last != nil
						 	  
							 this_location = outbreak.locations.first
							 #Default unmappable locations to lon / lat of 0 / 0 
							 #Escape these from the map
							 if (this_location.lat && this_location.lon) && (this_location.lat != 0 && this_location.lon != 0)
								    window_info = ""
								    this_event_header = ""
								   
								   	begin
								   		window_info = render_to_string :partial => "maps/marker_info", :locals => {:outbreak => outbreak}
									rescue
										window_info = "ERROR"
									end
								    if outbreak.aetiology == "BACTERIUM"
									   # @map.marker_init(Marker.new([this_location.lat.to_f, this_location.lon.to_f], :title => "Bacterial", :info_bubble => window_info, :icon => "../images/amber_marker.png"))
									    ba_name = outbreak.bacterial_agents.first != nil ? outbreak.bacterial_agents.first.bacterium.name : "Unknown bacterium"
									    this_marker = Marker.new([this_location.lat.to_f, this_location.lon.to_f], :title => "Bacterial", :info_bubble => window_info, :icon => "../images/amber_marker.png" )
									    begin
									    	    this_event_header = "<div class=\"bacterium_header\" title=\"Bacterial\">" << ba_name << "</div>"
									    rescue
									    	    this_event_header = "<div class=\"bacterium_header\" title=\"Bacterial\">Unknown bacterium</div>"    
									    end
								    elsif outbreak.aetiology == "VIRUS" 
									   # @map.marker_init(Marker.new([this_location.lat.to_f, this_location.lon.to_f], :title => "Viral", :info_bubble => window_info, :icon => "../images/dred_marker.png"))
									    va_name = outbreak.viral_agents.first != nil ? outbreak.viral_agents.first.virus.name : "Unknown Virus"
									    this_marker = Marker.new([this_location.lat.to_f, this_location.lon.to_f], :title => "Viral", :info_bubble => window_info, :icon => "../images/dred_marker.png")
									    begin
									    	    this_event_header = "<div class=\"virus_header\" title=\"Viral\">" << va_name << "</div>"
									    rescue
									    	    this_event_header = "<div class=\"virus_header\" title=\"Viral\"></div>"	    
									    end
								    elsif outbreak.aetiology == "PROTOZOA"
									   # @map.marker_init(Marker.new([this_location.lat.to_f, this_location.lon.to_f], :title => "Protozoal", :info_bubble => window_info, :icon => "../images/blue_marker.png"))
									    pa_name = outbreak.protozoal_agents.first != nil ? outbreak.protozoal_agents.first.protozoa.name : "Unknown Protozoa"
									    this_marker = Marker.new([this_location.lat.to_f, this_location.lon.to_f], :title => "Protozoal", :info_bubble => window_info, :icon => "../images/blue_marker.png")
									    begin
									    	    this_event_header = "<div class=\"protozoa_header\" title=\"Protozoal\">" << pa_name << "</div>"
									    rescue
									    	    this_event_header = "<div class=\"protozoa_header\" title=\"Protozoal\"></div>"    
									    end
								    elsif outbreak.aetiology == "TOXIN" 
									   # @map.marker_init(Marker.new([this_location.lat.to_f, this_location.lon.to_f], :title => "Toxic", :info_bubble => window_info, :icon => "../images/green_marker.png"))
									    ta_name = outbreak.toxic_agents.first != nil ? outbreak.toxic_agents.first.toxin.name : "Unknown Toxin"
									    this_marker = Marker.new([this_location.lat.to_f, this_location.lon.to_f], :title => "Toxic", :info_bubble => window_info, :icon => "../images/green_marker.png")
									    begin
									    	    this_event_header = "<div class=\"toxin_header\" title=\"Toxic\">" << ta_name << "</div>"
									    rescue
									    	    this_event_header = "<div class=\"toxin_header\" title=\"Toxic\"></div>"    
									    end
								    elsif outbreak.aetiology == "UNKNOWN"
									   # @map.marker_init(Marker.new([this_location.lat.to_f, this_location.lon.to_f], :title => "Unknown", :info_bubble => window_info, :icon => "../images/purple_marker.png"))
									   
									   this_marker = Marker.new([this_location.lat.to_f, this_location.lon.to_f], :title => "Unknown", :info_bubble => window_info, :icon => "../images/purple_marker.png")
									    this_event_header = "<div class=\"unknown_header\" title=\"Unknown\">Unknown agent</div>"
									    
								    elsif outbreak.aetiology == "MIXED"
									   # @map.marker_init(Marker.new([this_location.lat.to_f, this_location.lon.to_f], :title => "Mixed", :info_bubble => window_info, :icon => "../images/yellowd_marker.png"))
									   this_marker = Marker.new([this_location.lat.to_f, this_location.lon.to_f], :title => "Mixed", :info_bubble => window_info, :icon => "../images/yellowd_marker.png")
									    this_event_header = "<div class=\"mixed_header\" title=\"Mixed\">Mixed agent</div>"
								    else
								    	     this_event_header = "<div class=\"mixed_header\" title=\"Mixed\">ERROR</div>"   
								    end
								    
								    @markers.push(this_marker)
								    
									     
							else
								this_event_header = "<div class=\"mixed_header\" title=\"Mixed\">ERROR :: not on map</div>"      
							end
							
							if outbreak.onset_first && outbreak.onset_last
										     this_event = Event.new
										     this_event.onset_first = outbreak.onset_first
										     this_event.onset_last = outbreak.onset_last
										     this_event.info = this_event_header
										     this_event.info << "<div>Outbreak :: " << (outbreak.id ? outbreak.id.to_s : "0") <<  "</div>"
										     this_event.info << "<div><table><tr><th>Number at risk</th><th>Total affected</th><th>Number admitted</th><th>Died</th></tr><tr><td>" << (outbreak.at_risk != nil ? outbreak.at_risk.to_s : "0").to_s
										     this_event.info << "</td><td>" << (outbreak.total_affected != nil ? outbreak.total_affected.to_s : "0").to_s
										     this_event.info << "</td><td>" << (outbreak.admitted != nil ? outbreak.admitted.to_s : "0").to_s
										     this_event.info << "</td><td>" << (outbreak.died != nil ? outbreak.died.to_s : "0").to_s
										     this_event.info << "</td></tr></table></div>"
										     @events.push(this_event)
							end
							
							if @outbreaks.length == 0
								@outbreaks.push(outbreak)

							elsif outbreak.id > @outbreaks.last.id
								@outbreaks.push(outbreak)
							
							elsif outbreak.id < @outbreaks.last.id
								last_outbreak = @outbreaks.pop
								@outbreaks.push(outbreak)
								@outbreaks.push(last_outbreak)
							end
						#end 
					 end
				# rescue Exception => e  
				# 	 logger.error("ERROR MAP " << outbreak.inspect << " :::::: " << e.inspect )
				# end
			 end
			# if @markers.length > 200
			#	 clusterer = Clusterer.new(@markers, :max_visible_markers => 50, :icon => "../images/orange_marker.png")
			#	 @map.clusterer_init(clusterer)
			# else
			 	 marker_group = MarkerGroup.new(@markers, "markers")
			 	 @map.marker_group_init(marker_group)	 
			# end
			 @map.center_zoom_init([53,-3],6)
 
	end
	
	
	def redraw
		@year = params[:year]
		@map = Variable.new("map")
	
	end
	
	def timeline
		  id = session[:user_id].to_s
		  @user = User.find(id)
		  events = []
		  @cases = Case.find(:all, :conditions => {:HPU => @user.HPU})
		  @cases.each do |event|
		     # dates need to be utc
		     events << {:start => DateTime.strptime(event.studyDate.to_s, '%Y-%m-%d').utc,
		     :title => event.patientID, :description => event.LA}
		  end
	
		 respond_to do |format|
		      format.json{
			# the timeline needs the key events to point to the array of event objects:
			data = {"events" => events}
			render :json => data.to_json
		      }
		 end
	end
	
	
end
