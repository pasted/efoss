class InvestigationsController < ApplicationController
	
	
  def add_investigation_agent
  	  @agent_type = params[:agent_type]
  	  @outbreak_type = params[:outbreak_type]
  	 
  	  @next_ba_index = params[:next_ba_index]
  	  @next_va_index = params[:next_va_index]
  	  @next_pa_index = params[:next_pa_index]
  	  @next_ta_index = params[:next_ta_index]
  	  
  	if params[:id] != "0"
  		
  	  @outbreak = Outbreak.find(params[:id])
  	else
  		
  	  @outbreak = Outbreak.new
  	  @outbreak.risks.build
  	  @outbreak.incidents.build.build_location
  	  @outbreak.foods.build.build_evidence
  	  @outbreak.viral_agents.build
  	  @outbreak.bacterial_agents.build
  	  @outbreak.toxic_agents.build
  	  @outbreak.protozoal_agents.build
  	  @outbreak.build_response.measures.build
  	end
  	
  	if @outbreak_type == "FOODBORNE" || @outbreak_type == "NON-FOODBORNE"
  		ob_type_query = "INVESTIGATIONS:CATEGORY:" << @outbreak_type
  		@sample_types = Property.find(:all, :conditions => {:field => ob_type_query})	
  	
  		render :update do |page|
			case @agent_type
				when "BACTERIUM"
					@bacterial_agent = BacterialAgent.new
					
					@bacterial_agent.category = "ASSOCIATED"
					@bacterial_agent.build_investigation
					@outbreak.bacterial_agents[@next_ba_index.to_i] = @bacterial_agent
					@bacteria = Bacterium.all
					
					@next_child_index = params[:next_ba_index]
					page.insert_html :bottom, "associated_sample_listing", :partial => "bacterial_agents/bacterial_agent_investigation_partial", :locals => { :bacteria => @bacteria, :bacterial_agent => @bacterial_agent }
				 
				when "VIRUS"
					@viral_agent = ViralAgent.new
					
					@viral_agent.category = "ASSOCIATED"
					@viral_agent.build_investigation
					@outbreak.viral_agents[@next_va_index.to_i] = @viral_agent
					@viruses = Virus.all
					
					@next_child_index = params[:next_va_index]
					page.insert_html :bottom, "associated_sample_listing", :partial => "viral_agents/viral_agent_investigation_partial", :locals => { :viruses => @viruses, :viral_agent => @viral_agent }
				 
				when "PROTOZOA"
					@protozoal_agent = ProtozoalAgent.new
					
					@protozoal_agent.category = "ASSOCIATED"
					@protozoal_agent.build_investigation
					@outbreak.protozoal_agents[@next_pa_index.to_i] = @protozoal_agent
					@protozoas = Protozoa.all
					
					@next_child_index = params[:next_pa_index]
					page.insert_html :bottom, "associated_sample_listing", :partial => "protozoal_agents/protozoal_agent_investigation_partial", :locals => { :protozoas => @protozoas, :protozoal_agent => @protozoal_agent }
				 
				when "TOXIN"
					@toxic_agent = ToxicAgent.new
					
					@toxic_agent.category = "ASSOCIATED"
					@toxic_agent.build_investigation
					@outbreak.toxic_agents[@next_ta_index.to_i] = @toxic_agent
					@toxins = Toxin.all
					
					@next_child_index = params[:next_ta_index]
					page.insert_html :bottom, "associated_sample_listing", :partial => "toxic_agents/toxic_agent_investigation_partial", :locals => { :toxins => @toxins, :toxic_agent => @toxic_agent }
		
			end
		end
  	
  	end
  	
  	
		
  end
	
  # GET /investigations
  # GET /investigations.xml
  def index
    @investigations = Investigation.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @investigations }
    end
  end

  # GET /investigations/1
  # GET /investigations/1.xml
  def show
    @investigation = Investigation.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @investigation }
    end
  end

  # GET /investigations/new
  # GET /investigations/new.xml
  def new
    @investigation = Investigation.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @investigation }
    end
  end

  # GET /investigations/1/edit
  def edit
    @investigation = Investigation.find(params[:id])
  end

  # POST /investigations
  # POST /investigations.xml
  def create
    @investigation = Investigation.new(params[:investigation])

    respond_to do |format|
      if @investigation.save
        flash[:notice] = 'Investigation was successfully created.'
        format.html { redirect_to(@investigation) }
        format.xml  { render :xml => @investigation, :status => :created, :location => @investigation }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @investigation.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /investigations/1
  # PUT /investigations/1.xml
  def update
    @investigation = Investigation.find(params[:id])

    respond_to do |format|
      if @investigation.update_attributes(params[:investigation])
        flash[:notice] = 'Investigation was successfully updated.'
        format.html { redirect_to(@investigation) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @investigation.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /investigations/1
  # DELETE /investigations/1.xml
  def destroy
    @investigation = Investigation.find(params[:id])
    @investigation.destroy

    respond_to do |format|
      format.html { redirect_to(investigations_url) }
      format.xml  { head :ok }
    end
  end
end
