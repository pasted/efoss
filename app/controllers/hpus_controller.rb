class HpusController < ApplicationController
  # GET /hpus
  # GET /hpus.xml
  def index
    @hpus = Hpu.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @hpus }
    end
  end

  # GET /hpus/1
  # GET /hpus/1.xml
  def show
    @hpu = Hpu.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @hpu }
    end
  end

  # GET /hpus/new
  # GET /hpus/new.xml
  def new
    @hpu = Hpu.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @hpu }
    end
  end

  # GET /hpus/1/edit
  def edit
    @hpu = Hpu.find(params[:id])
  end

  # POST /hpus
  # POST /hpus.xml
  def create
    @hpu = Hpu.new(params[:hpu])

    respond_to do |format|
      if @hpu.save
        flash[:notice] = 'Hpu was successfully created.'
        format.html { redirect_to(@hpu) }
        format.xml  { render :xml => @hpu, :status => :created, :location => @hpu }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @hpu.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /hpus/1
  # PUT /hpus/1.xml
  def update
    @hpu = Hpu.find(params[:id])

    respond_to do |format|
      if @hpu.update_attributes(params[:hpu])
        flash[:notice] = 'Hpu was successfully updated.'
        format.html { redirect_to(@hpu) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @hpu.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /hpus/1
  # DELETE /hpus/1.xml
  def destroy
    @hpu = Hpu.find(params[:id])
    @hpu.destroy

    respond_to do |format|
      format.html { redirect_to(hpus_url) }
      format.xml  { head :ok }
    end
  end
end
