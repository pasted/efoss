class VirusesController < ApplicationController
  # GET /viruses
  # GET /viruses.xml
  def index
    @viruses = Virus.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @viruses }
    end
  end

  # GET /viruses/1
  # GET /viruses/1.xml
  def show
    @virus = Virus.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @virus }
    end
  end

  # GET /viruses/new
  # GET /viruses/new.xml
  def new
    @virus = Virus.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @virus }
    end
  end

  # GET /viruses/1/edit
  def edit
    @virus = Virus.find(params[:id])
  end

  # POST /viruses
  # POST /viruses.xml
  def create
    @virus = Virus.new(params[:virus])

    respond_to do |format|
      if @virus.save
        flash[:notice] = 'Virus was successfully created.'
        format.html { redirect_to(@virus) }
        format.xml  { render :xml => @virus, :status => :created, :location => @virus }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @virus.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /viruses/1
  # PUT /viruses/1.xml
  def update
    @virus = Virus.find(params[:id])

    respond_to do |format|
      if @virus.update_attributes(params[:virus])
        flash[:notice] = 'Virus was successfully updated.'
        format.html { redirect_to(@virus) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @virus.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /viruses/1
  # DELETE /viruses/1.xml
  def destroy
    @virus = Virus.find(params[:id])
    @virus.destroy

    respond_to do |format|
      format.html { redirect_to(viruses_url) }
      format.xml  { head :ok }
    end
  end
end
