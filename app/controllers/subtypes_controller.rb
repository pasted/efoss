class SubtypesController < ApplicationController
	
	
  def update_category
  	  @transmission_mode = params[:ob_type]
  	  @categories = Category.find(:all, :conditions => {:transmission_mode => @transmission_mode})
  	  
  end
  
  def update_subcategory
  	  @category_id = params[:category]
  	  @subcategories = Subcategory.find(:all, :conditions => {:category_id => @category_id})
  	  
  end
	
  # GET /subtypes
  # GET /subtypes.xml
  def index
    @subtypes = Subtype.find(:all, :order => :id)
    
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @subtypes }
    end
  end

  # GET /subtypes/1
  # GET /subtypes/1.xml
  def show
    @subtype = Subtype.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @subtype }
    end
  end

  # GET /subtypes/new
  # GET /subtypes/new.xml
  def new
    @subtype = Subtype.new
    @transmission_modes = Property.find(:all, :order => "property_value ASC", :conditions => { :field => "OUTBREAKS:OUTBREAK_TYPE"})
    @categories = Category.find(:all, :order => "name ASC")
    @subcategories = Subcategory.find(:all, :order => "name ASC")
    
    @options_for_subcategories = Array.new
    @options_for_subtypes = Array.new

      	  @options_for_subcategories = Array.new
  	  Category.all.each do |this_category|
  	  	  final_array = Array.new
  	  	  final_array.push(this_category.name)
  	  	  @subcategories = Subcategory.find(:all, :conditions => {:category_id => this_category.id})
  	  	  subcategory_array = Array.new
  	  	  subcategory_array.push(['',''])
  	  	  @subcategories.each do |this_subcategory|
  	  	  	  subcategory_array.push([this_subcategory.name, this_subcategory.name])
  	  	  end
  	  	  final_array.push(subcategory_array)
  	  	  @options_for_subcategories.push(final_array)
  	  end
  	  
  	  @options_for_subtypes = Array.new
  	  Subcategory.all.each do |this_subcategory|
  	  	  final_array = Array.new
  	  	  final_array.push(this_subcategory.name)
  	  	  @subtypes = Subtype.find(:all, :conditions => {:subcategory_id => this_subcategory.id})
  	  	  subtype_array = Array.new
  	  	  subtype_array.push(['',''])
  	  	  @subtypes.each do |this_subtype|
  	  	  	  subtype_array.push([this_subtype.name, this_subtype.name])  
  	  	  end
  	  	  final_array.push(subtype_array)
  	  	  @options_for_subtypes.push(final_array)
  	  end
  	  
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @subtype }
    end
  end

  # GET /subtypes/1/edit
  def edit
    @subtype = Subtype.find(params[:id])
    @options_for_subcategories = Array.new
    @options_for_subtypes = Array.new

      	  @options_for_subcategories = Array.new
  	  Category.all.each do |this_category|
  	  	  final_array = Array.new
  	  	  final_array.push(this_category.name)
  	  	  @subcategories = Subcategory.find(:all, :conditions => {:category_id => this_category.id})
  	  	  subcategory_array = Array.new
  	  	  subcategory_array.push(['',''])
  	  	  @subcategories.each do |this_subcategory|
  	  	  	  subcategory_array.push([this_subcategory.name, this_subcategory.name])
  	  	  end
  	  	  final_array.push(subcategory_array)
  	  	  @options_for_subcategories.push(final_array)
  	  end
  	  
  	  @options_for_subtypes = Array.new
  	  Subcategory.all.each do |this_subcategory|
  	  	  final_array = Array.new
  	  	  final_array.push(this_subcategory.name)
  	  	  @subtypes = Subtype.find(:all, :conditions => {:subcategory_id => this_subcategory.id})
  	  	  subtype_array = Array.new
  	  	  subtype_array.push(['',''])
  	  	  @subtypes.each do |this_subtype|
  	  	  	  subtype_array.push([this_subtype.name, this_subtype.name])  
  	  	  end
  	  	  final_array.push(subtype_array)
  	  	  @options_for_subtypes.push(final_array)
  	  end
  	  
    @transmission_modes = Property.find(:all, :order => "property_value ASC", :conditions => { :field => "OUTBREAKS:OUTBREAK_TYPE"})
    @categories = Category.find(:all, :order => "name ASC")
    @subcategories = Subcategory.find(:all, :order => "name ASC")
  end

  # POST /subtypes
  # POST /subtypes.xml
  def create
    @subtype = Subtype.new(params[:subtype])

    respond_to do |format|
      if @subtype.save
        flash[:notice] = 'Subtype was successfully created.'
        format.html { redirect_to(@subtype) }
        format.xml  { render :xml => @subtype, :status => :created, :location => @subtype }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @subtype.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /subtypes/1
  # PUT /subtypes/1.xml
  def update
    @subtype = Subtype.find(params[:id])

    respond_to do |format|
      if @subtype.update_attributes(params[:subtype])
        flash[:notice] = 'Subtype was successfully updated.'
        format.html { redirect_to(@subtype) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @subtype.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /subtypes/1
  # DELETE /subtypes/1.xml
  def destroy
    @subtype = Subtype.find(params[:id])
    @subtype.destroy

    respond_to do |format|
      format.html { redirect_to(subtypes_url) }
      format.xml  { head :ok }
    end
  end
end
