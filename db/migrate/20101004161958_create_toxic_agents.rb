class CreateToxicAgents < ActiveRecord::Migration
  def self.up
    create_table :toxic_agents do |t|
      t.primary_key :id
      t.integer :outbreak_id
      t.integer :toxin_id
      t.string :category
      t.integer :concentration
      t.boolean :confirmed
      t.text :comment

      t.timestamps
    end
  end

  def self.down
    drop_table :toxic_agents
  end
end
