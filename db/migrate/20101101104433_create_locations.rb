class CreateLocations < ActiveRecord::Migration
  def self.up
    create_table :locations do |t|
      t.primary_key :id
      t.string :category
      t.string :placename
      t.string :address_1
      t.string :address_2
      t.string :address_3
      t.string :town
      t.string :region
      t.string :postcode
      t.float :lon
      t.float :lat
      t.point :lonlat
      t.integer :local_authority_id
      t.integer :hpu_id
      t.integer :region_id
      t.integer :country_id

      t.timestamps
    end
  end

  def self.down
    drop_table :locations
  end
end
