class CreateLocalAuthorities < ActiveRecord::Migration
  def self.up
    create_table :local_authorities do |t|
      t.primary_key :id
      t.integer :hpu_id
      t.integer :region_id
      t.string :name

      t.timestamps
    end
  end

  def self.down
    drop_table :local_authorities
  end
end
