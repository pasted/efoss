class CreateEvidenceCategories < ActiveRecord::Migration
  def self.up
    create_table :evidence_categories do |t|
      t.primary_key :id
      t.string :outbreak_type
      t.string :category
      t.string :name

      t.timestamps
    end
  end

  def self.down
    drop_table :evidence_categories
  end
end
