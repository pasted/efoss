class CreateViruses < ActiveRecord::Migration
  def self.up
    create_table :viruses do |t|
      t.primary_key :id
      t.string :name
      t.string :group
      t.string :family
      t.string :genus

      t.timestamps
    end
  end

  def self.down
    drop_table :viruses
  end
end
