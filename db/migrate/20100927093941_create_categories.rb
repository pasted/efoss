class CreateCategories < ActiveRecord::Migration
  def self.up
    create_table :categories do |t|
      t.primary_key :id
      t.string :name
      t.string :transmission_mode

      t.timestamps
    end
  end

  def self.down
    drop_table :categories
  end
end
