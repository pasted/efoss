class CreateExposures < ActiveRecord::Migration
  def self.up
    create_table :exposures do |t|
      t.primary_key :id
      t.string :name
      t.string :category
      t.string :comment
      t.integer :outbreak_id
      t.integer :detail_id
      t.integer :location_id

      t.timestamps
    end
  end

  def self.down
    drop_table :exposures
  end
end
