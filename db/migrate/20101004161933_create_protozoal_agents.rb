class CreateProtozoalAgents < ActiveRecord::Migration
  def self.up
    create_table :protozoal_agents do |t|
      t.primary_key :id
      t.integer :outbreak_id
      t.integer :protozoa_id
      t.string :category
      t.string :other_typing
      t.boolean :confirmed
      t.text :comment

      t.timestamps
    end
  end

  def self.down
    drop_table :protozoal_agents
  end
end
