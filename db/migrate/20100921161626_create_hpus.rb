class CreateHpus < ActiveRecord::Migration
  def self.up
    create_table :hpus do |t|
      t.primary_key :id
      t.integer :region_id
      t.string :name

      t.timestamps
    end
  end

  def self.down
    drop_table :hpus
  end
end
