class CreateIncidents < ActiveRecord::Migration
  def self.up
    create_table :incidents do |t|
      t.primary_key :id
      t.integer :outbreak_id
      t.integer :location_id
      t.integer :category_id
      t.integer :subcategory_id
      t.integer :subtype_id
      t.integer :cuisine_id
      t.string :detail

      t.timestamps
    end
  end

  def self.down
    drop_table :incidents
  end
end
