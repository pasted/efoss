class CreateInfections < ActiveRecord::Migration
  def self.up
    create_table :infections do |t|
      t.primary_key :id
      t.integer :outbreak_id
      t.integer :pathogen_id

      t.timestamps
    end
  end

  def self.down
    drop_table :infections
  end
end
