class CreateSubtypes < ActiveRecord::Migration
  def self.up
    create_table :subtypes do |t|
      t.primary_key :id
      t.string :name
      t.integer :subcategory_id

      t.timestamps
    end
  end

  def self.down
    drop_table :subtypes
  end
end
