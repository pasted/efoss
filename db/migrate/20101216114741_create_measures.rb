class CreateMeasures < ActiveRecord::Migration
  def self.up
    create_table :measures do |t|
      t.primary_key :id
      t.integer :response_id
      t.text :description

      t.timestamps
    end
  end

  def self.down
    drop_table :measures
  end
end
