class CreateResponses < ActiveRecord::Migration
  def self.up
    create_table :responses do |t|
      t.primary_key :id
      t.integer :outbreak_id
      t.string :premise_status
      t.string :prosecution_status
      t.string :notice_issued
      t.string :inspection_rating
      t.string :hygiene_rating
      t.text :comment

      t.timestamps
    end
  end

  def self.down
    drop_table :responses
  end
end
