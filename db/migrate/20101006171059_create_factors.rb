class CreateFactors < ActiveRecord::Migration
  def self.up
    create_table :factors do |t|
      t.primary_key :id
      t.string :name
      t.string :category
      t.string :subcategory
      t.string :outbreak_type

      t.timestamps
    end
  end

  def self.down
    drop_table :factors
  end
end
