class CreateInvestigations < ActiveRecord::Migration
  def self.up
    create_table :investigations do |t|
      t.primary_key :id
      t.integer :investigatable_id
      t.string :investigatable_type
      t.string :category
      t.string :level
      t.text :comment

      t.timestamps
    end
  end

  def self.down
    drop_table :investigations
  end
end
