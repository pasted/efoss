class CreateBacteria < ActiveRecord::Migration
  def self.up
    create_table :bacteria do |t|
      t.primary_key :id
      t.string :name
      t.string :genus
      t.string :species
      t.string :subspecies
      t.string :serovar

      t.timestamps
    end
  end

  def self.down
    drop_table :bacteria
  end
end
