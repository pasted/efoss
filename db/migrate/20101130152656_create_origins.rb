class CreateOrigins < ActiveRecord::Migration
  def self.up
    create_table :origins do |t|
      t.primary_key :id
      t.string :category

      t.timestamps
    end
  end

  def self.down
    drop_table :origins
  end
end
