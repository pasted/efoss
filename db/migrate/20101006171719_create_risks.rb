class CreateRisks < ActiveRecord::Migration
  def self.up
    create_table :risks do |t|
      t.primary_key :id
      t.integer :outbreak_id
      t.integer :factor_id
      t.text :detail

      t.timestamps
    end
  end

  def self.down
    drop_table :risks
  end
end
