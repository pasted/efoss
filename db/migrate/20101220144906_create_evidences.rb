class CreateEvidences < ActiveRecord::Migration
  def self.up
    create_table :evidences do |t|
      t.primary_key :id
      t.integer :evident_id
      t.string :evident_type
      t.integer :evidence_category_id
      t.text :comment

      t.timestamps
    end
  end

  def self.down
    drop_table :evidences
  end
end
