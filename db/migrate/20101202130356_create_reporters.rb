class CreateReporters < ActiveRecord::Migration
  def self.up
    create_table :reporters do |t|
      t.primary_key :id
      t.string :title
      t.string :fname
      t.string :lname
      t.string :telephone
      t.string :email
      t.string :position
    
      t.integer :outbreak_id
      t.integer :location_id

      t.timestamps
    end
  end

  def self.down
    drop_table :reporters
  end
end
