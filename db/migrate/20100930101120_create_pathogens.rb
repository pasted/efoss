class CreatePathogens < ActiveRecord::Migration
  def self.up
    create_table :pathogens do |t|
      t.primary_key :id
      t.string :pathogen_name
      t.string :category
      t.string :serotype
      t.string :phagetype
      t.string :other_typing
      t.string :toxin
      t.boolean :confirmed
      t.text :comment
      t.integer :bacterium_id
      t.integer :virus_id
      t.integer :protozoa_id

      t.timestamps
    end
  end

  def self.down
    drop_table :pathogens
  end
end
