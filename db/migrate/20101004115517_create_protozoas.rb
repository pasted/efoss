class CreateProtozoas < ActiveRecord::Migration
  def self.up
    create_table :protozoas do |t|
      t.primary_key :id
      t.string :name
      t.string :genus
      t.string :species
      t.string :subspecies

      t.timestamps
    end
  end

  def self.down
    drop_table :protozoas
  end
end
