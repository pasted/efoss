class CreateFoodSubcategories < ActiveRecord::Migration
  def self.up
    create_table :food_subcategories do |t|
      t.primary_key :id
      t.integer :food_category_id
      t.string :name

      t.timestamps
    end
  end

  def self.down
    drop_table :food_subcategories
  end
end
