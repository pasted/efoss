class CreateViralAgents < ActiveRecord::Migration
  def self.up
    create_table :viral_agents do |t|
      t.primary_key :id
      t.integer :outbreak_id
      t.integer :virus_id
      t.string :category
      t.string :other_typing
      t.boolean :confirmed
      t.text :comment

      t.timestamps
    end
  end

  def self.down
    drop_table :viral_agents
  end
end
