class CreateBacterialAgents < ActiveRecord::Migration
  def self.up
    create_table :bacterial_agents do |t|
      t.primary_key :id
      t.integer :outbreak_id
      t.integer :bacterium_id
      t.string :category
      t.string :serotype
      t.string :phagetype
      t.string :other_typing
      t.boolean :confirmed
      t.text :comment

      t.timestamps
    end
  end

  def self.down
    drop_table :bacterial_agents
  end
end
