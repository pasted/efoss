class CreateToxins < ActiveRecord::Migration
  def self.up
    create_table :toxins do |t|
      t.primary_key :id
      t.string :name
      t.string :category

      t.timestamps
    end
  end

  def self.down
    drop_table :toxins
  end
end
