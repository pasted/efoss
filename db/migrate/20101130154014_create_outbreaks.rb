class CreateOutbreaks < ActiveRecord::Migration
  def self.up
    create_table :outbreaks do |t|
      t.primary_key :id
      t.integer :user_id
      t.string :current_state
      t.integer :year
      t.date :onset_first
      t.date :onset_last
      t.string :user_reference
      t.string :gezi_reference
      t.string :transmission_mode
      t.integer :total_affected
      t.integer :at_risk
      t.integer :lab_confirmed
      t.integer :admitted
      t.integer :died
      t.string :aetiology
      t.string :outbreak_type
      t.text :comment
      t.boolean :point_source
      t.date :point_source_date
      t.text :point_source_detail
      t.integer :origin_id
      t.string :report_file_name
      t.string :report_content_type
      t.string :report_file_size
      t.datetime :report_updated_at

      t.timestamps
    end
  end

  def self.down
    drop_table :outbreaks
  end
end
