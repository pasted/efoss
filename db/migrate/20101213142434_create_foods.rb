class CreateFoods < ActiveRecord::Migration
  def self.up
    create_table :foods do |t|
      t.primary_key :id
      t.integer :outbreak_id
      t.integer :food_category_id
      t.integer :food_subcategory_id
      t.text :description
      t.boolean :contains_rse
      t.string :country

      t.timestamps
    end
  end

  def self.down
    drop_table :foods
  end
end
