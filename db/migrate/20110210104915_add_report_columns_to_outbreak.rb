class AddReportColumnsToOutbreak < ActiveRecord::Migration
  def self.up
  	  add_column :outbreaks, :report_file_name,    :string
  	  add_column :outbreaks, :report_content_type, :string
  	  add_column :outbreaks, :report_file_size,    :integer
  	  add_column :outbreaks, :report_updated_at,   :datetime

  end

  def self.down
  	  remove_column :outbreaks, :report_file_name
  	  remove_column :outbreaks, :report_content_type
  	  remove_column :outbreaks, :report_file_size
  	  remove_column :outbreaks, :report_updated_at

  end
end
