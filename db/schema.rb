# This file is auto-generated from the current state of the database. Instead of editing this file, 
# please use the migrations feature of Active Record to incrementally modify your database, and
# then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your database schema. If you need
# to create the application database on another system, you should be using db:schema:load, not running
# all the migrations from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20110314102816) do

  create_table "bacteria", :force => true do |t|
    t.string   "name"
    t.string   "genus"
    t.string   "species"
    t.string   "subspecies"
    t.string   "serovar"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "bacterial_agents", :force => true do |t|
    t.integer  "outbreak_id"
    t.integer  "bacterium_id"
    t.string   "category"
    t.string   "serotype"
    t.string   "phagetype"
    t.string   "other_typing"
    t.boolean  "confirmed"
    t.text     "comment"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "categories", :force => true do |t|
    t.string   "name"
    t.string   "outbreak_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cuisines", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "details", :force => true do |t|
    t.boolean  "microbiological"
    t.boolean  "cohort_study"
    t.boolean  "control_study"
    t.boolean  "descriptive"
    t.text     "comment"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "evidence_categories", :force => true do |t|
    t.string   "outbreak_type"
    t.string   "category"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "evidences", :force => true do |t|
    t.integer  "evident_id"
    t.string   "evident_type"
    t.integer  "evidence_category_id"
    t.text     "comment"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "exposures", :force => true do |t|
    t.string   "name"
    t.string   "category"
    t.string   "comment"
    t.integer  "outbreak_id"
    t.integer  "detail_id"
    t.integer  "location_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "factors", :force => true do |t|
    t.string   "name"
    t.string   "category"
    t.string   "subcategory"
    t.string   "outbreak_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "factors_outbreaks", :id => false, :force => true do |t|
    t.integer "factor_id"
    t.integer "outbreak_id"
  end

  create_table "food_categories", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "food_subcategories", :force => true do |t|
    t.integer  "food_category_id"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "foods", :force => true do |t|
    t.integer  "outbreak_id"
    t.integer  "food_category_id"
    t.integer  "food_subcategory_id"
    t.text     "description"
    t.boolean  "contains_rse"
    t.string   "country"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "geometry_columns", :id => false, :force => true do |t|
    t.string  "f_table_catalog",   :limit => 256, :null => false
    t.string  "f_table_schema",    :limit => 256, :null => false
    t.string  "f_table_name",      :limit => 256, :null => false
    t.string  "f_geometry_column", :limit => 256, :null => false
    t.integer "coord_dimension",                  :null => false
    t.integer "srid",                             :null => false
    t.string  "type",              :limit => 30,  :null => false
  end

  create_table "hpus", :force => true do |t|
    t.integer  "region_id"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "incidents", :force => true do |t|
    t.integer  "outbreak_id"
    t.integer  "location_id"
    t.integer  "category_id"
    t.integer  "subcategory_id"
    t.integer  "subtype_id"
    t.integer  "cuisine_id"
    t.string   "detail"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "investigations", :force => true do |t|
    t.integer  "investigatable_id"
    t.string   "investigatable_type"
    t.string   "category"
    t.string   "level"
    t.text     "comment"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "local_authorities", :force => true do |t|
    t.integer  "hpu_id"
    t.integer  "region_id"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "locations", :force => true do |t|
    t.string   "encrypted_category"
    t.string   "encrypted_placename"
    t.string   "encrypted_address_1"
    t.string   "encrypted_address_2"
    t.string   "encrypted_address_3"
    t.string   "encrypted_town"
    t.string   "encrypted_postcode"
    t.float    "lon"
    t.float    "lat"
    t.integer  "local_authority_id"
    t.integer  "hpu_id"
    t.integer  "region_id"
    t.integer  "country_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "measures", :force => true do |t|
    t.integer  "response_id"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "origins", :force => true do |t|
    t.string   "category"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "outbreaks", :force => true do |t|
    t.integer  "user_id"
    t.string   "current_state"
    t.integer  "year"
    t.date     "onset_first"
    t.date     "onset_last"
    t.string   "user_reference"
    t.string   "gezi_reference"
    t.string   "transmission_mode"
    t.integer  "total_affected"
    t.integer  "at_risk"
    t.integer  "lab_confirmed"
    t.integer  "admitted"
    t.integer  "died"
    t.string   "aetiology"
    t.string   "outbreak_type"
    t.text     "comment"
    t.boolean  "point_source"
    t.date     "point_source_date"
    t.text     "point_source_detail"
    t.integer  "origin_id"
    t.string   "report_file_name"
    t.string   "report_content_type"
    t.string   "report_file_size"
    t.datetime "report_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "postcodes", :id => false, :force => true do |t|
    t.string "postcode",    :limit => nil, :null => false
    t.string "area",        :limit => nil
    t.string "district",    :limit => nil
    t.string "sector",      :limit => nil
    t.string "country",     :limit => nil
    t.string "region",      :limit => nil
    t.string "ward",        :limit => nil
    t.string "local",       :limit => nil
    t.string "hpu",         :limit => nil
    t.string "team",        :limit => nil
    t.string "hpu_team",    :limit => nil
    t.string "hpu_tel",     :limit => nil
    t.string "lsoa",        :limit => nil
    t.string "x",           :limit => nil
    t.string "y",           :limit => nil
    t.string "lat",         :limit => nil
    t.string "lon",         :limit => nil
    t.string "urban_ind",   :limit => nil
    t.string "urban_ind_2", :limit => nil
    t.string "urban_ind_3", :limit => nil
    t.string "mapinfo_id",  :limit => nil
  end

  create_table "properties", :force => true do |t|
    t.string   "field"
    t.text     "property_value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "protozoal_agents", :force => true do |t|
    t.integer  "outbreak_id"
    t.integer  "protozoa_id"
    t.string   "category"
    t.string   "other_typing"
    t.boolean  "confirmed"
    t.text     "comment"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "protozoas", :force => true do |t|
    t.string   "name"
    t.string   "genus"
    t.string   "species"
    t.string   "subspecies"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "regions", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "reporters", :force => true do |t|
    t.string   "title"
    t.string   "fname"
    t.string   "lname"
    t.string   "telephone"
    t.string   "email"
    t.string   "position"
    t.text     "comment"
    t.integer  "outbreak_id"
    t.integer  "location_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "responses", :force => true do |t|
    t.integer  "outbreak_id"
    t.string   "premise_status"
    t.string   "prosecution_status"
    t.string   "notice_issued"
    t.string   "inspection_rating"
    t.string   "hygiene_rating"
    t.text     "comment"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "risks", :force => true do |t|
    t.integer  "outbreak_id"
    t.integer  "factor_id"
    t.text     "detail"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "roles_users", :id => false, :force => true do |t|
    t.integer "role_id"
    t.integer "user_id"
  end

  create_table "spatial_ref_sys", :id => false, :force => true do |t|
    t.integer "srid",                      :null => false
    t.string  "auth_name", :limit => 256
    t.integer "auth_srid"
    t.string  "srtext",    :limit => 2048
    t.string  "proj4text", :limit => 2048
  end

  create_table "subcategories", :force => true do |t|
    t.string   "name"
    t.integer  "category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "subtypes", :force => true do |t|
    t.string   "name"
    t.integer  "subcategory_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "toxic_agents", :force => true do |t|
    t.integer  "outbreak_id"
    t.integer  "toxin_id"
    t.string   "category"
    t.integer  "concentration"
    t.boolean  "confirmed"
    t.text     "comment"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "toxins", :force => true do |t|
    t.string   "name"
    t.string   "category"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                               :default => "", :null => false
    t.string   "encrypted_password",   :limit => 128, :default => "", :null => false
    t.string   "password_salt",                       :default => "", :null => false
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "reset_password_token"
    t.string   "remember_token"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                       :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.integer  "failed_attempts",                     :default => 0
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "fname"
    t.string   "lname"
    t.integer  "location_id"
  end

  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true
  add_index "users", ["unlock_token"], :name => "index_users_on_unlock_token", :unique => true

  create_table "viral_agents", :force => true do |t|
    t.integer  "outbreak_id"
    t.integer  "virus_id"
    t.string   "category"
    t.string   "other_typing"
    t.boolean  "confirmed"
    t.text     "comment"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "viruses", :force => true do |t|
    t.string   "name"
    t.string   "group"
    t.string   "family"
    t.string   "genus"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
